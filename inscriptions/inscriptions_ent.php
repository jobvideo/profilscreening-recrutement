<?php include('../include/header-inscriptions.php');?>
    <body id="body" class="page page-login page-inscription d-flex justify-content-center align-items-top">
        <main id="main" role="main" class="main-inscription d-flex flex-column align-items-center">
            <header id="header" class="header">
	            <h1 class="logo-login">Profilscreening©</h1>
	        <h2>Inscription à l'espace<br><span>entreprises</span></h2>
            </header>
            <?php
            include('../include/connexion.php');
            include('../include/fonctions.php');
            include("../include/parametres.php");
            include("../include/pagination.php");

            if (isset($_POST['nom'])) {
                //6LfpnngUAAAAAKYUSaEZc0qXMp0EzhQZSjTgQeci
                // Ma clé privée
                $secret   = "6LfpnngUAAAAAKYUSaEZc0qXMp0EzhQZSjTgQeci";
                // Paramètre renvoyé par le recaptcha
                $response = $_POST['g-recaptcha-response'];
                // On récupère l'IP de l'utilisateur
                $remoteip = $_SERVER['REMOTE_ADDR'];

                $api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
                        . $secret
                        . "&response=" . $response
                        . "&remoteip=" . $remoteip;

                $decode = json_decode(file_get_contents($api_url), true);
                    include "../include/operation_message.php";

                if ($decode['success'] == true) {
                    // C'est un humain
                    $nom                 = lecture($_POST['nom']);
                    $Prenom              = lecture($_POST['Prenom']);
                    $entreprise          = lecture($_POST['entreprise']);
                    $Siret               = lecture($_POST['Siret']);
                    $url                 = lecture($_POST['url']);
                    $email               = lecture($_POST['email']);
                    $tel                 = lecture($_POST['tel']);
                    $date                = date('Y-m-d');
                    $admin_user_password = generatePassword();
                    $sqlU                = "INSERT INTO admin_user
                                     SET    admin_user_nom            = '$entreprise_email'         ,
                                            admin_user_pouvoir        = 'entreprise'     ,
					  admin_user_description    = 'entreprise' ,
					  admin_user_date           = " . time() . "                ,
					  admin_user_actif          = 'Y'       ,
					  admin_user_password       = '" . md5($admin_user_password) . "'";

                    $resU          = $connexion->query($sqlU);
                    $admin_user_id = $connexion->lastInsertId();

                    $sql = "insert into  entreprise
	           SET      entreprise_raison_social            = '$entreprise'        ,
			    entreprise_num_siret             	= '$Siret'        ,
                            entreprise_email            	= '$email'  ,
                            entreprise_site            		= '$url' ,
                            entreprise_date_modif		= '$date' ,
                            entreprise_tel			= '$tel' ,
                            entreprise_nom                      = '$nom',
                            entreprise_prenom                    = 'entreprise_nom' ,
                            admin_user_id               =  '$admin_user_id '
						";

                    $res    = $connexion->query($sql);
                    $ent_id = $connexion->lastInsertId();
                    mail_pass($admin_user_password, $email, $entreprise . ' ' . $Prenom);
                    operation_message("Inscription Terminée", False, '', '../index.php');
                    exit;
                }
                else {
                    // C'est un robot ou le code de vérification est incorrecte
                    operation_message("Erreur captcha", False, '', 'inscriptions_ent.php');
                }



            }
            ?>
            <section id="featured-log" class="featured-log featured-inscription">
                <div class="formulaire log-content d-flex flex-column" id="content-insription">
                    <form class="container clearfix" action="" method="post" name="form1" id="form1" enctype="multipart/form-data">
                            <input name="role" type="hidden" value="Entreprises" />

                            <p class="input-block"><label for="text">Nom<span class="obligatory">*</span></label>
                                <input type="text" name="nom" id="nom" placeholder="Inserer votre nom ici"></p>

                            <p class="input-block"><label for="text">Prénom<span class="obligatory">*</span></label>
                                <input type="text" name="Prenom" id="Prenom" placeholder="Inserer votre prénom ici"></p>

                            <p class="input-block"><label for="text">Nom de l'entreprise<span class="obligatory">*</span></label>
                                <input type="text" name="entreprise" id="entreprise" placeholder="Inserer le Nom de votre entreprise"></p>


                            <p class="input-block"><label for="tel">Siret<span class="obligatory">*</span></label>
                                <input type="number_.format" id="Siret" name="Siret" placeholder="Votre numéro de siret" required></p>

                            <p class="input-block"><label for="url">Site internet</label>
                                <input type="url" id="url" name="url" placeholder="ex : http://www.monsite.fr"  ></p>

                            <p class="input-block"><label for="email">Email<span class="obligatory">*</span></label>
                                <input type="email" id="email" name="email" placeholder="ex : votreadresse@mondomaine.fr" required></p>

                            <p class="input-block"><label for="tel">Téléphone<span class="obligatory">*</span></label>
                                <input type="number_.format" id="tel" name="tel" placeholder="ex : 03364775236" required></p>

	                        <div class="checkbox-container d-flex align-items-center">
		                        <input name="Acceptation" id="Acceptation" type="checkbox" class="zone"><p>Accepte les conditions générales d'utilisation des données ?<span class="obligatory">*</span> Consultables <a href="https://profilscreening.fr/mentions-legales" title="Voir la politique d'utilisation des données de Profilscreening©" target="_blank">ici</a></p>
	                        </div>
                            <div class="g-recaptcha recaptcha-container" data-sitekey="6LfpnngUAAAAAOyFOmEpnyKOJe48uwKHtZX12aM3"></div>
                            <div class="validation-container">
	                            <button class="login-submit d-flex align-items-center"><input type="submit" name="Submit" value="S'inscrire" onclick="verif();"><i class="ti-arrow-right"></i></button>
                            </div>
                    </form>

                </div>
            </section>
 <?php include ('../include/footer-inscriptions.php');