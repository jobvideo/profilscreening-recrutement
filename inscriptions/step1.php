
<script type="text/javascript" language="javascript">


    function verif()
    {

        if (document.form1.candidat_sex.value === "") {
            alert("Veuillez introduire le sexe  ");
            document.form1.candidat_sex.focus();
            return;
        } //Fsi

        if (document.form1.candidat_nom.value === "") {
            alert("Veuillez introduire le nom  ");
            document.form1.candidat_nom.focus();
            return;
        } //Fsi
        if (document.form1.candidat_prenom.value === "") {
            alert("Veuillez introduire le prenom  ");
            document.form1.candidat_prenom.focus();
            return;
        } //Fsi
        if (document.form1.candidat_email.value === "") {
            alert("Veuillez introduire l'email  ");
            document.form1.candidat_email.focus();
            return;
        } //Fsi
        if (document.form1.candidat_telephone.value === "") {
            alert("Veuillez introduire le tél  ");
            document.form1.candidat_telephone.focus();
            return;
        } //Fsi

        if (document.form1.Acceptation.checked === false) {
            alert("Veuillez Accepter les conditions générales d'utilisation des données ");
            document.form1.Acceptation.focus();
            return;
        }

        document.form1.submit();
    } //Fin verif


</script>

<div id="step-1">
    <div class="formulaire" id="content-insription">
        <p class="input-block"><label for="text">Sexe<span class="obligatory">*</span></label>
			<select type="zone" name="candidat_sex" class="select-dropdown">
				<option value="Faites un choix">Faites un choix</option>
				<option value="Femme">Femme</option>
				<option value="Homme">Homme</option>
			</select>
        </p>

        <p class="input-block"><label for="text">Nom<span class="obligatory">*</span></label>
            <input type="text" name="candidat_nom" id="candidat_nom" value="" placeholder="Inserer votre nom ici" required></p>

        <p class="input-block"><label for="text">Prénom<span class="obligatory">*</span></label>
            <input type="text" name="candidat_prenom" id="candidat_prenom" placeholder="Inserer votre prénom ici" value="" required></p>


        <p class="input-block"><label for="email"> Email<span class="obligatory">*</span></label>
            <input type="email" id="candidat_email" name="candidat_email" value=""  placeholder="ex : votreadresse@mondomaine.fr" required></p>

        <p class="input-block"><label for="tel">Tél.<span class="obligatory">*(à renseigner si vous souhaitez un accompagnement)</span></label>
            <input type="number_.format" id="candidat_telephone" name="candidat_telephone"  value=""  placeholder="ex : 03364775236" required></p>
    </div>

</div>


