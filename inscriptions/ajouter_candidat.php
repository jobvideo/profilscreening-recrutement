<?php include('../include/header-inscriptions.php');?>
    <body id="body" class="page page-login page-inscription d-flex justify-content-center align-items-center">
        <main id="main" role="main" class="d-flex flex-column align-items-center main-inscription">
            <header id="header" class="header">
	            <h1 class="logo-login">Profilscreening©</h1>
	        <h2>Inscription à l'espace<br><span>Candidats</span></h2>
            </header>



<!--                         <li><a href="../">Authentification</a></li> -->


<!--             <link rel="stylesheet" type="text/css" media="all" href= "../css/bootstrap-old.css"> -->
            <?php
            include('../include/connexion.php');
            include('../include/fonctions.php');
            ?>

<!--

            <div id="cv-tabs" class="cv-tabs">

                <div class="cv-tabs-inner clearfix">
                </div>

                <div id="content-tab" class="content-tab" style="width:100%">

                    <div id="tabs-4" class="clearfix tab consult">
-->

                        <!-- !-->

                        <?php
//---> Rubrique valide ?
                        $rubrique_id = getRubriqueId($connexion, "operateurs");

//---> Tester la session et importer les variables : $select, $mod, $insert, $delete
//     relatives aux privil�ges de l'utilisateur et de la rubrique en cours

                        include "../include/candidats.php";     //---> Les fonctions du module candidat
//---> Utiliser le module pagination

                        if (isset($_POST['candidat_nom'])) {
                            // Ma clé privée
                            $secret   = "6LfpnngUAAAAAKYUSaEZc0qXMp0EzhQZSjTgQeci";
                            // Paramètre renvoyé par le recaptcha
                            $response = $_POST['g-recaptcha-response'];
                            // On récupère l'IP de l'utilisateur
                            $remoteip = $_SERVER['REMOTE_ADDR'];

                            $api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
                                    . $secret
                                    . "&response=" . $response
                                    . "&remoteip=" . $remoteip;

                            $decode = json_decode(file_get_contents($api_url), true);
                                include "../include/operation_message.php";  //---> inclure fonction pour afficher un message

                            if ($decode['success'] == true) {

                                $candidat_nom       = lecture($_POST['candidat_nom']);
                                $candidat_prenom    = $_POST['candidat_prenom'];
                                $candidat_sex       = $_POST['candidat_sex'];
                                $candidat_telephone = $_POST['candidat_telephone'];
                                $candidat_email     = $_POST['candidat_email'];
                                if (isset($_POST['accompagne']))
                                    $accompagne         = 'NON';
                                $accompagne         = 'OUI';

                                $date                = date('Y-m-d');
                                $admin_user_password = generatePassword();
                                $sqlU                = "INSERT INTO admin_user
                                     SET    admin_user_nom            = '$candidat_email'         ,
                                            admin_user_pouvoir        = 'candidat'     ,
					  admin_user_description    = 'candidat' ,
					  admin_user_date           = '" . $date . "'                ,
					  admin_user_actif          = 'Y'       ,
					  admin_user_password       = '" . md5($admin_user_password) . "'";

                                $resU          = $connexion->query($sqlU);
                                $admin_user_id = $connexion->lastInsertId();

                                $sql         = "insert into candidat
                                    SET  candidat_nom                = '$candidat_nom',
                                         candidat_prenom             = '$candidat_prenom'        ,
                                         candidat_sex                = '$candidat_sex'        ,
                                         accompagne                  = '$accompagne'        ,
                                         candidat_telephone          = '$candidat_telephone'        ,
                                         candidat_email              = '$candidat_email'        ,
                                         date_mise_ajour             = '$date'  ,
                                         admin_user_id               =  '$admin_user_id '
                                     ";
                                $res         = $connexion->query($sql);
                                $candidat_id = $connexion->lastInsertId();
                                mail_pass($admin_user_password, $candidat_email, $candidat_nom . ' ' . $candidat_prenom);
                                operation_message("Inscription Terminée", False, '', '../index.php');
                                exit;
                            }
                            else {
                                // C'est un robot ou le code de vérification est incorrecte
                                operation_message("Erreur captcha", False, '', 'ajouter_candidat.php');
                            }
                        } //Fsi
                        //---> Champs du formulaire
					?>
    <section id="featured-log" class="featured-log featured-inscription">
		<div id="log-content" class="log-content d-flex flex-column">
	        <form class="container d-flex flex-column clearfix" action="" method="post" name="form1" id="form1" enctype="multipart/form-data">
				<?php include("step1.php"); ?>
				<div class="checkbox-container d-flex align-items-center">
			        <input name="accompagne" id="accompagne" type="checkbox" class='zone'>
			        <p>Souhaite être accompagné ?</p>
		        </div>
		        <div class="checkbox-container d-flex align-items-center">
					<input name="Acceptation" id="Acceptation" type="checkbox" class="zone">
					<p>Accepte les conditions générales d'utilisation des données ?<span class="obligatory">*</span> Consultables <a href="https://profilscreening.fr/mentions-legales" title="Voir la politique d'utilisation des données de Profilscreening©" target="_blank">ici</a></p>
			    </div>
			    <div class="g-recaptcha recaptcha-container" data-sitekey="6LfpnngUAAAAAOyFOmEpnyKOJe48uwKHtZX12aM3"></div>
			    <div class="validation-container">
			        <button class="login-submit d-flex align-items-center"><input type="submit" name="Submit" value="S'inscrire" onclick="verif();"><i class="ti-arrow-right"></i></button>
			    </div>
	        </form>
		</div>
	</section>
<?php include ('../include/footer-inscriptions.php');
