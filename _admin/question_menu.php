<?php include('include/header-profil-operateur.php'); ?>

<div id="cv-tabs" class="cv-tabs">

    <div class="cv-tabs-inner clearfix">
        <a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

        <ul id="tabs" class="tabs clearfix">
            <li><a class="tab1 tab-profil" id="#tabs-1"id="firstonglet" href="./?<?php echo $action ?>=operateur_profil&<?php echo $link ?>#tabs-1" >
                    Mon Profil </a></li>
            <li><a class="tab2 tab-candidats"  id="#tabs-2" href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>#tabs-2" >
                    Candidats   </a></li>
            <li><a class="tab3 tab-synthese activate" id="#tabs-3"  href="./?<?php echo $action ?>=quest&<?php echo $link ?>#tabs-3"  >
                    Synthèse  </a></li>
            <li><a class="tab4 tab-competences" id="#tabs-4"  href="./?<?php echo $action ?>=competence_metier&&<?php echo $link ?>#tabs-4">Compétences</a></li>

            <!--
                                                    <li><a class="tab10"  id="#tabs-10" href="javascript:popup('changer_pw.php?admin_user_id=<?php echo $admin_user_id; ?>&<?php echo $link ?>#tabs-10',475,250)" class="menutext">
                                                                    Password		</a></li>
            -->
        </ul>

    </div>

    <div id="content-tab" class="content-tab">

        <div id="tabs-"<?php echo $tab_num ?> class="clearfix tab consult">

            <!-- !-->
            <style type="text/css">
                <!--
                @import url("style.css");
                -->
            </style>
            <?php
            //---> Rubrique valide ?
            $rubrique_id = getRubriqueId($connexion, "partie_synthese");

            //---> Tester la session et importer les variables : $select, $mod, $insert, $delete
            //     relatives aux privilèges de l'utilisateur et de la rubrique en cours
            include "../include/session_test.php";

            include "../include/question.php";     //---> Les fonctions du module question
            //---> Utiliser le module pagination

            if (isset($_GET['partie_synthese_id']))
                $partie_synthese_id = $_GET['partie_synthese_id'];
            else
                $partie_synthese_id = 0;
            if (isset($_GET['question_ap']))
                $question_ap = $_GET['question_ap'];
            else
                $question_ap = 0;
            //---> Procédure de suppression
            if (isset($_POST['supprimer']) && count($_POST['supprimer']) > 0 && $delete == 'Y') {
                question_supprimer($connexion, $_POST['supprimer']);
            } //Fsi


            if (isset($_POST['dsens']) && $mod == 'Y') {
                if ($_POST['dsens'] == "bas")
                    question_deplacer($_POST['d_id'], "bas");
                else
                    question_deplacer($_POST['d_id'], "haut");
            }

            /*             * *******************************************************************************************************
              Gestion de la pagination
             * ******************************************************************************************************** */
            //---> Créer un objet de pagination sans condition SQL sur la table
            $p = new CAdminPagination($connexion, "question", "partie_synthese_id = '$partie_synthese_id'", 8, "question_position");
            $p->writeJavaScript();    //---> Générer le code JavaScript correspondant
            ?>

            <script type="text/javascript" language="javascript">

                function verif()
                {
                    var msg = "Voulez reellement appliquer les changements demandes (modification + suppression) ?"
                    if (confirm(msg))
                        document.pagination_tab.submit();
                } //Fin appliquer

                function ajouter_Pop(question_id = 0)
                {
                    if (question_id == 0)
                        popup('question_add_Modal.php?<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&partie_synthese_id=<?php echo $partie_synthese_id; ?>&question_ap=<?php echo $question_ap ?>', 525, 320);
                    else
                        popup('question_add_Modal.php?<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&partie_synthese_id=<?php echo $partie_synthese_id; ?>&question_ap=<?php echo $question_ap ?>&question_id=' + question_id, 525, 320);


                } //Fin ajouter
                function ajouter(question_id = 0)
                {
                    if (question_id == 0)
                        document.location.href = './?p=question_add&<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&partie_synthese_id=<?php echo $partie_synthese_id; ?>&question_ap=<?php echo $question_ap ?>';
                    else
                        document.location.href = './?p=question_add&<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&partie_synthese_id=<?php echo $partie_synthese_id; ?>&question_ap=<?php echo $question_ap ?>&question_id=' + question_id;

                } //Fin ajouter
                function deplacer(id, sens)
                {
                    document.ordre.d_id.value = id;
                    document.ordre.dsens.value = sens;
                    document.ordre.submit();
                } //Fin deplacer


            </script>

            <h2>Liste des questions</h2>

            <?php
            $res = $p->makeButtons($action);    //---> Afficher les bouttons
            ?>
            <!-- Fin de l'enête de pagination -->	  </td>

            <form name="ordre" method="post" action="">
                <input type="hidden" name="dsens"   value="">
                <input type="hidden" name="d_id"    value="">
            </form>





            <?php
            if ($p->getTotal() != 0 && $select == 'Y') { //---> Autorisations suffisantes ?
                ?>
                <div class="tab-synthese">
                    <form name="pagination_tab" method="post" action="">
                        <!-- Début du tableau de contenu -->
                        <div class="tab-synthese-header clearfix">
                            <div class="tab-cell tab-10"><label>N&deg;</label></div>
                            <div class="tab-cell tab-40"><label>Nom</label></div>
                            <div class="tab-cell tab-40"><label>Reponse Questions</label></div>
                            <?php
                            if ($delete == 'Y') {
                                ?>
                                <div class="tab-cell tab-10 delete"><label>sup</label></div>
                                <?php
                            }//Fsi
                            ?>
                        </div>
                        <ul class="tabv-synthese-content clearfix">


                            <?php
                            $i = 0;
                            $nbre = @$res->rowCount();
                            $n = 0;
                            while ($row = @$res->fetch()) {
                                $i++;
                                $n++;
                                $disabled = ($mod != 'Y') ? "disabled" : "";
                                $color = ($i % 2 != 0) ? "#EFEFEF" : "#E9E9E9";
                                $session = $_GET["session"];
                                $question_id = $row['question_id'];
                                $question_nom = affichage($row['question_nom'], "---");
                                $question_visible = ($row['question_visible'] == 'Y') ? "CHECKED" : "";

                                //-->les catégories de chaque question
                                $sql2 = " SELECT DISTINCT reponse_question_id 	 FROM reponse_question WHERE  question_id = $question_id ";
                                $res2 = $connexion->query($sql2);
                                $nb = $res2->rowCount();
                                ?>
                                <input type="hidden" name="id[]" value="<?php echo $row['question_id'] ?>" /></td>
                                <li class="tab-synth">
                                    <div class="tab-cell tab-10">
                                        <?php echo $p->courent * $p->page + $i ?>
                                    </div>
                                    <div class="tab-cell tab-40">
                                        <input type="hidden" name="id[]" value="<?php echo $row['question_id'] ?>"/>
                                        <?php if ($mod == 'Y') { ?>
                                            <a href="javascript: ajouter(<?php echo $row['question_id'] ?>);"  class="menutext">
                                                <?php echo $question_nom ?>
                                            </a>
                                            <?php
                                        } else {
                                            echo $question_nom;
                                        }//Fsi
                                        ?>
                                    </div>
                                    <div class="tab-cell tab-40">
                                        <a href="./?<?php echo $action ?>=reponse_questions&<?php echo $link ?>&question_id=<?php echo $question_id ?>&question_ap=<?php echo $question_ap ?>" class="menutext"><?php echo $nb; ?></a>

                                    </div>
                                    <div class="tab-cell tab-10">
                                        <?php
                                        if ($delete == 'Y') {
                                            ?>
                                        <input type="checkbox" name="supprimer[]" id="supprimer<?php echo $i ?>" value="<?php echo $question_id; ?>"  onclick="javascript: restore_row(this.parentNode.parentNode, '<?php echo $color ?>');" /> 
                                    <?php
                                } //Fsi
                                ?>

                                </div>
                                </li>
                                <?php
                            } //Fwhile
                            ?>
                            <!-- Fin du tableau de contenu -->
                        </ul>
                    </form>	 

                    <div class="group-buttons">
                        <a href="../?<?php echo $action ?>=authentification" target="_blank">
                            <button>Aper&ccedil;u</button>
                        </a>
                        <?php
                    } //Fsi
                    ?>
                    <!-- Fin du tableau de contenu -->
                    <?php
                    if ($insert == 'Y') {                   //---> Autorisations suffisantes ?
                        ?>

                        <input type="button" name="Submit2" value="Ajouter" onClick="javascript: ajouter();">

                        <?php
                    } //Fsi
                    ?>

                    <?php
                    if ($p->getTotal() != 0 && $select == 'Y') { //---> Autorisations suffisantes ?
                        ?>
                        <input type="button" name="Submit" value="Appliquer"  onClick="javascript: verif();">
                        <?php
                    } //Fsi
                    ?>
                </div>



            </div>



        </div>
    </div>


</section>
