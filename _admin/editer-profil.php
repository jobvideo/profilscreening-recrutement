<?php
include('include/header-profil-operateur.php');

$id_operateur      = $_SESSION['operateur_id'];
$sql_operateur     = "select admin_user_id from operateur where 	operateur_id='$id_operateur'";
$res_operateur     = $connexion->query($sql_operateur);
$donnees_operateur = $res_operateur->fetch();


if (isset($_POST['operateur_raison_social'])) {


    $operateur_raison_social    = $_POST['operateur_raison_social'];
    $operateur_domaine_activite = $_POST['operateur_domaine_activite'];
    $operateur_num_siret        = $_POST['operateur_num_siret'];
    //$operateur_photo = $_POST['operateur_photo'] ;
    $operateur_video_url        = $_POST['operateur_video_url'];
    $operateur_adresse          = $_POST['operateur_adresse'];
    $operateur_code_postale     = $_POST['operateur_code_postale'];
    $operateur_vile             = $_POST['operateur_vile'];
    $operateur_site             = $_POST['operateur_site'];
    $operateur_tel              = $_POST['operateur_tel'];
    //$operateur_ref = $_POST['operateur_ref'] ;
    $operateur_description      = utf8_encode($_POST['operateur_description']);
    $operateur_email            = $_POST['operateur_email'];
    $operateur_date_modif       = date("-m-d");

    $sql = "update operateur set
					operateur_raison_social ='$operateur_raison_social',
	  				operateur_domaine_activite ='$operateur_domaine_activite',
					operateur_num_siret ='$operateur_num_siret',
 					operateur_video_url ='$operateur_video_url',
					operateur_date_modif = '$operateur_date_modif',
					operateur_adresse ='$operateur_adresse',
					operateur_vile ='$operateur_vile',
					operateur_site ='$operateur_site',
					operateur_tel ='$operateur_tel',
 					operateur_description ='$operateur_description',
					operateur_email ='$operateur_email'

	    ";
    $res = $connexion->query($sql);           //---> Exécuter la requête
    //
   if (isset($_FILES["operateur_photo"])) {

        if ($_FILES["operateur_photo"]["size"] > 0 &&
                $_FILES["operateur_photo"]["error"] == 0 &&
                $_FILES["operateur_photo"]["name"] != "") {
            //-->L'extension de l'image $ext
            $filename = $_FILES["operateur_photo"]["name"];
            $filename = explode(".", $filename);
            $ext      = $filename[count($filename) - 1];

            if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                if (isset($operateur_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                    $sql             = " SELECT operateur_photo FROM operateur WHERE operateur_id = $operateur_id ";
                    $res             = $connexion->query($sql);
                    $row             = $res->fetch();
                    $operateur_photo = $row['operateur_photo'];
                    @unlink("../common/Images/operateur/$operateur_photo");  //--> suppresssion de l'image pr&eacute;cedante
                }
                $filename = "operateur($operateur_id).$ext";

                upload_file("operateur_photo", "../common/Images/operateur/$filename");
                //operateur_photo_update($operateur_id, $filename, $ext,$connexion);
                //---> Je mets à jour le nom de l'image
                $sql = "UPDATE operateur
	           SET    operateur_photo   = '$filename'
			   WHERE  operateur_id      = $operateur_id";
                $res = $connexion->query($sql); //---> Exécuter la requête
            }
            else {

                operation_message("Format du Fichier incorrect !", true, '');
            }
        } //Fsi
    }
    ?>
    <script>

        document.location = './index.php?session=<?php echo $idses ?>';
    </script>
    <?php
}


$sql_operateur     = "select * from operateur where 	operateur_id='$id_operateur'";
$res_operateur     = $connexion->query($sql_operateur);
$donnees_operateur = $res_operateur->fetch();

$nom                        = $donnees_operateur['operateur_raison_social'];
$operateur_domaine_activite = $donnees_operateur['operateur_domaine_activite'];
$sirete                     = $donnees_operateur['operateur_num_siret'];
$operateur_photo            = $donnees_operateur['operateur_photo'];
$operateur_video_url        = $donnees_operateur['operateur_video_url'];
$operateur_adresse          = $donnees_operateur['operateur_adresse'];
$operateur_code_postale     = $donnees_operateur['operateur_code_postale'];
$operateur_vile             = $donnees_operateur['operateur_vile'];
$operateur_site             = $donnees_operateur['operateur_site'];
$operateur_tel              = $donnees_operateur['operateur_tel'];
$operateur_ref              = $donnees_operateur['operateur_ref'];
$operateur_description      = utf8_encode($donnees_operateur['operateur_description']);
$operateur_email            = $donnees_operateur['operateur_email'];



if ($operateur_photo == "")
    $operateur_photo = "vide.png";
?>


<div id="cv-tabs" class="cv-tabs">
    <div class="cv-tabs-inner clearfix">
        <a href="./?p=editer_profil&session=<?php echo $idses ?>#tabs-4" class="btn edit edit-on" title="Modifier son profil">Editer son profil</a>
        <ul id="tabs" class="tabs clearfix">
            <li><a class="tab1" id="#tabs-1"id="firstonglet" href="./?<?php echo $action ?>=operateur_profil&<?php echo $link ?>#tabs-1" >Mon Profil</a></li>
            <li><a class="tab2" id="#tabs-2" href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>#tabs-2" >Candidats</a></li>
            <li><a class="tab3" id="#tabs-3"  href="./?<?php echo $action ?>=quest&<?php echo $link ?>#tabs-3">Synthèse</a></li>
            <li><a class="tab4" id="#tabs-4"  href="./?<?php echo $action ?>=competence_metier&&<?php echo $link ?>#tabs-4">Compétences</a></li>

        </ul>
    </div>
    <div id="content-tab" class="content-tab">
        <div id="tabs-4" class="clearfix tab edit edit-profil-ope">
            <?php include('include/edit-profil-operateur.php'); ?>
        </div>
    </div>
</div>
</div>
</section>
