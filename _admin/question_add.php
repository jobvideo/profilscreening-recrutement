<?php include('include/header-profil-operateur.php'); ?>
 	<link rel="stylesheet" type="text/css" media="all" href= "../css/style_syntehese.css">

<div id="cv-tabs" class="cv-tabs">

    <div class="cv-tabs-inner clearfix">
        <a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

        <ul id="tabs" class="tabs clearfix">
            <li><a class="tab1 tab-profil" id="#tabs-1"id="firstonglet" href="./?<?php echo $action ?>=operateur_profil&<?php echo $link ?>#tabs-1" >
                    Mon Profil </a></li>
            <li><a class="tab2 tab-candidats"  id="#tabs-2" href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>#tabs-2" >
                    Candidats   </a></li>
            <li><a class="tab3 tab-synthese activate" id="#tabs-3"  href="./?<?php echo $action ?>=quest&<?php echo $link ?>#tabs-3"  >
                    Synthèse  </a></li>
            <li><a class="tab4 tab-competences" id="#tabs-4"  href="./?<?php echo $action ?>=competence_metier&&<?php echo $link ?>#tabs-4">Compétences</a></li>

            <!--
                                                    <li><a class="tab10"  id="#tabs-10" href="javascript:popup('changer_pw.php?admin_user_id=<?php echo $admin_user_id; ?>&<?php echo $link ?>#tabs-10',475,250)" class="menutext">
                                                                    Password		</a></li>
            -->
        </ul>

    </div>


    <?php
    $admin_user_id = isset($_GET['admin_user_id']) ? $_GET['admin_user_id'] : 0;
    $rubrique_id = getRubriqueId($connexion, "partie_synthese");


    include "../include/session_test.php"; //---> Tester la session et 
    //     Importer les variables $select, $mod, $insert, $delete
    include "../include/question.php";  //---> Les fonctions du module question 

    if (isset($_GET['partie_synthese_id']))
        $partie_synthese_id = $_GET['partie_synthese_id'];
    else
        $partie_synthese_id = 0;
    if (isset($_GET['question_ap']))
        $question_ap = $_GET['question_ap'];
    else
        $question_ap = 0;
    ?>
    <script>
        function fermer()
        {
            document.location.href = './?p=question&<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&partie_synthese_id=<?php echo $partie_synthese_id; ?>&question_ap=<?php echo $question_ap ?>';

        } //Fin ajouter
    </script> 
    <?php
    //---> Le formaulire de la page en cours a �t� envoy�
    if (isset($_POST['question_nom'])) {
        include "../include/operation_message.php";  //---> inclure fonction pour afficher un message
        $question_nom = lecture($_POST['question_nom']);
        if ($question_ap == "Aptitude") {
            $question_p = lecture($_POST['question_p']);
            $question_q = lecture($_POST['question_q']);
        } else {
            $question_p = 0;
            $question_q = 0;
        }
        if ($question_ap == "Personnalite") {
            $oppose = lecture($_POST['oppose']);
        } else {
            $oppose = "";
        }
        if (isset($_GET['question_id'])) { //---> il s'agit d'une modification	
            $question_id = $_GET['question_id'];
            $sql = "UPDATE question
	           SET    question_nom             = '$question_nom'         ,
			   question_p             = '$question_p'         ,
			    oppose             = '$oppose'         ,
			   question_q             = '$question_q'         
					  WHERE  question_id         =  $question_id            ";
            $res = $connexion->query($sql);           //---> Ex�cuter la requ�te
            operation_message("Modification Terminée", TRUE);  //---> Msg + Fermer la fen�tre	  
        } else { //---> il s'agit d'une insertion
            $sql = "SELECT MAX(question_position)  as MAX 
				FROM   question";
            $res = $connexion->query($sql);
            $row = $res->fetch();
            $pos = $row['MAX'];
            $pos++;
             $sql = "INSERT INTO question
	           SET       question_nom             = '$question_nom'        ,
			     question_visible           = 'Y'              ,
			     oppose             = '$oppose'         ,
			    question_p             = '$question_p'         ,
			    question_q             = '$question_q' ,
			    question_position = '$pos'		,
			    partie_synthese_id             = '$partie_synthese_id'    ";
            //---> Ex�cuter la requ�te et faire $question_id = @connexion->lastInsertId();
            $res = $connexion->query($sql);
            $question_id = $connexion->lastInsertId();
            operation_message("Insertion Terminée", FALSE);   //---> Msg + Racharger la page d'insertion	 
        } //Fsi
         
        ?>
        <script>
            fermer();
        </script>
        <?php
        exit();
    } //Fsi
//---> Champs du formulaire
    if (isset($_GET['question_id'])) { //---> il s'agit d'une modification
        $op = "Modifier";
        $sql = "SELECT *
	         FROM   question
			 WHERE  question_id = " . $_GET['question_id'];
        $res = $connexion->query($sql); //---> Ex�cuter la requ�te
        $row = $res->fetch();
        if ($row == FALSE)
            die("Impossible de trouver l'actualit�");
        $question_nom = decode_text($row['question_nom']);
        $question_id = $row['question_id'];
        $question_q = $row['question_q'];
        $question_p = $row['question_p'];
        $oppose = $row['oppose'];
    } else { //---> il s'agit d'une insertion
        $op = "Ajouter";
        $question_nom = "";
        $question_id = NULL;
        $question_q = "";
        $question_p = "";
        $oppose = "";
    } //Fsi
    if ($question_ap == "Personnalite") {
        include"rep_personnalite.php";
    } else
    if ($question_ap == "Aptitude") {
        include"quest_aptitude.php";
    } else {
        include"quest_autre.php";
    }
    ?>
</div>