<?php include('include/header-profil-operateur.php');?>
	<div id="cv-tabs" class="cv-tabs">
		<div class="cv-tabs-inner clearfix">
			<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

			<ul id="tabs" class="tabs clearfix">
				<li><a class="tab1 tab-profil" id="#tabs-1"id="firstonglet" href="./?<?php  echo $action?>=operateur_profil&<?php  echo $link?>#tabs-1" >Mon Profil</a></li>
				<li><a class="tab2 tab-candidat"  id="#tabs-2" href="./?<?php  echo $action?>=liste_candidats&<?php  echo $link?>#tabs-2">Candidats</a></li>
				<li><a class="tab3 tab-synthese activate" id="#tabs-3"  href="./?<?php  echo $action?>=quest&<?php  echo $link?>#tabs-3">Synthèse</a></li>
				<li><a class="tab4 tab-comptetences" id="#tabs-4"  href="./?<?php  echo $action?>=competence_metier&&<?php  echo $link?>#tabs-4">Compétences</a></li>

			</ul>

		</div>

		<div id="content-tab" class="content-tab">

			<div id="tabs-"<?php echo $tab_num?> class="clearfix tab-ope-synth consult">

				<?php
				   //---> Rubrique valide ?
				   $rubrique_id = getRubriqueId($connexion,"partie_synthese");

				   //---> Tester la session et importer les variables : $select, $mod, $insert, $delete
				   //     relatives aux privilèges de l'utilisateur et de la rubrique en cours
				   include "../include/session_test.php";

				   include "../include/partie_synthese.php";     //---> Les fonctions du module partie_synthese
				          //---> Utiliser le module pagination



				   //---> Procédure de suppression
				   if (isset($_POST['supprimer']) && count($_POST['supprimer']) > 0 && $delete=='Y')
				   {
					 partie_synthese_supprimer($_POST['supprimer']);
				   } //Fsi

				   if (isset($_POST['id']) && $mod=='Y')
				   {
				     //---> Procédure de modification "visible"
				     if (isset($_POST['visible']))
					   partie_synthese_visible($_POST['visible'],$_POST['id']);
					 else
					   partie_synthese_visible(NULL,$_POST['id']);  // Tous à faux


				   } //Fsi
				    if (isset($_POST['dsens']) && $mod=='Y')
						{
						 if ($_POST['dsens']=="bas")
						   partie_synthese_deplacer($_POST['d_id'],"bas");
						 else
						   partie_synthese_deplacer($_POST['d_id'],"haut");
						}

				  /*********************************************************************************************************
				                                            Gestion de la pagination
				  **********************************************************************************************************/
				  //---> Créer un objet de pagination sans condition SQL sur la table
				  $p = new CAdminPagination($connexion,"partie_synthese","", 8, "partie_synthese_position");
				  $p->writeJavaScript();    //---> Générer le code JavaScript correspondant
				?>

				<script type="text/javascript" language="javascript">
				<!--
				  function verif()
				  {
				    var msg = "Voulez réellement appliquer les changements demandés (modification + suppression) ?"
					if (confirm(msg))
					  document.pagination_tab.submit();
				  } //Fin appliquer

				  function ajouter()
				  {
				    popup('partie_synthese_add.php?<?php  echo $link?>&admin_user_id=<?php  echo $admin_user_id?>',525,320);
				  } //Fin ajouter
				   function deplacer(id, sens)
				  {
				    document.ordre.d_id.value    = id;
				    document.ordre.dsens.value = sens;
					document.ordre.submit();
				  } //Fin deplacer

				-->
				</script>


				<h2>Liste des synthèses de mes candidats</h2>

				<?php
				  $res = $p->makeButtons($action);    //---> Afficher les bouttons
				?>

				<form name="ordre" method="post" action="">
				   <input type="hidden" name="dsens"   value="">
		           <input type="hidden" name="d_id"    value="">
				</form>

			    <?php
				  if($p->getTotal()!=0 && $select=='Y') //---> Autorisations suffisantes ?
		          {
				?>
					<form name="pagination_tab" method="post" action="">
					<!-- Début du tableau de contenu -->

						<div class="tab-synthese">
							<ul class="tab-synthese-header clearfix">
								<li class="tab-cell tab-10"><label>N°</label></li>
								<li class="tab-cell tab-50"><label>Designation de la synthèse</label></li>
								<li class="tab-cell tab-20"><label>Nombre de questions</label></li>
								<li class="tab-cell tab-10"><label>Visibilité</label></li>
								<li class="tab-cell tab-5 delete">
									<label>
										<?php
											if($delete=='Y')
											{
						          			 echo 'Supprimer';
											}?>
					          		</label>
					          	</li>
							</ul>

							<ul class="tab-synthese-content clearfix">
				                <?php
							          $i = 0;
									   $nbre=@$res->rowCount();
								       $n=0;
							          while($row=@$res->fetch())
							          {
									    $i++;
										$n++;
									    $disabled            = ($mod!='Y')? "disabled" : "";
										$color               = ($i%2!=0)? "#EFEFEF" : "#E9E9E9";
										$session             = $_GET["session"];
										 $partie_synthese_id     = $row['partie_synthese_id'];
									    $partie_synthese_nom     = affichage($row['partie_synthese_nom'],"---");
										 $partie_synthese_visible   = ($row['partie_synthese_visible']=='Y')? "CHECKED" : "";

										//-->les catégories de chaque partie_synthese
									$sql2 = " SELECT DISTINCT question_type_id 	 FROM question_type WHERE  partie_synthese_id = '$partie_synthese_id' ";
									$res2 = $connexion->query($sql2);
									$nb = $res2->rowCount();
								?>

								<li class="tab-synth">
					            	<div class="tab-cell tab-10">
									  <?php  echo $p->courent*$p->page+$i?>
									  <input type="hidden" name="id[]" value="<?php  echo $row['partie_synthese_id']?>" />
							    	</div>

							        <div class="tab-cell tab-50"
							            <?php
										if($mod=='Y')
								        {
								      ?>
						                <a href="javascript:popup('partie_synthese_add.php?<?php  echo $link?>&admin_user_id=<?php  echo $admin_user_id?>&partie_synthese_id=<?php  echo $row['partie_synthese_id']?>',525,720,true);">
						                <?php  echo $partie_synthese_nom?>
						                </a>
						                <?php
										} else
										{
										  echo $partie_synthese_nom;
										} //Fsi
						              ?>
									</div>

									<div class="tab-cell tab-20">
										<a href="./?<?php  echo $action?>=question_type&<?php  echo $link?>&partie_synthese_id=<?php  echo $partie_synthese_id?>"><?php echo $nb; ?></a>
									</div>

									<div class="tab-cell tab-10">
										<input type="Checkbox" name="visible[]" value="<?php  echo $row['partie_synthese_id']?>" style="color:#666666;" <?php  echo $partie_synthese_visible?> <?php  echo $disabled?> />
									</div>

									<div class="tab-cell tab-5 delete">
						                <?php if($delete=='Y'){
							          		echo '<input type="reset" name="supprimer[]" id="supprimer' . $i . '" value="' . $row['partie_synthese_id'] . '" onclick="javascript: restore_row(this.parentNode.parentNode, ' . $color. ')" />';
									    } //Fsi
									  	?>
								  	</div>
								</li>

							<?php
							  } //FTQ
							?>

						</ul>

						<?php

						  } //Fsi
						?>

				    </form>


						<?php
							if($insert=='Y')//---> Autorisations suffisantes ?

							{

						?>
						<div class="btn-action">
							<input type="button" name="Submit2" value="Ajouter" onClick="javascript: ajouter();">
						</div>
						<?php

						} //Fsi

						?>



						<?php
						if($p->getTotal()!=0 && $select=='Y') //---> Autorisations suffisantes ?

						{

						?>
						<div class="btn-action">
							<input class="submit" type="button" name="Submit" value="Enregistrer"  onClick="javascript: verif();">
						</div>
						<?php

						} //Fsi

						?>

				</div><!-- .tab-synthese -->

			</div><!-- .tab-ope-synth -->

		</div><!-- .content-tab -->

	</div><!-- .cv-tabs -->
</section>

