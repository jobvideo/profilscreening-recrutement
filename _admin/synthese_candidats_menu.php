<?php include('include/header-profil-operateur.php');?>

		<div id="cv-tabs" class="cv-tabs">

			<div class="cv-tabs-inner clearfix">
				<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

				<ul id="tabs" class="tabs clearfix">
					<li><a class="tab1" id="#tabs-1"id="firstonglet" href="./?<?php  echo $action?>=operateur_profil&<?php  echo $link?>#tabs-1" >
							Mon Profil </a></li>
					<li><a class="tab2"  id="#tabs-2" href="./?<?php  echo $action?>=liste_candidats&<?php  echo $link?>#tabs-2" >
							Candidats   </a></li>
					<li><a class="tab3 activate" id="#tabs-3"  href="./?<?php  echo $action?>=quest&<?php  echo $link?>#tabs-3"  >
							Synthèse  </a></li>
					<li><a class="tab4" id="#tabs-4"  href="./?<?php  echo $action?>=competence_metier&&<?php  echo $link?>#tabs-4">Compétences</a></li>

<!--
					<li><a class="tab10"  id="#tabs-10" href="javascript:popup('changer_pw.php?admin_user_id=<?php  echo $admin_user_id;?>&<?php  echo $link?>#tabs-10',475,250)" class="menutext">
							Password		</a></li>
-->
				</ul>

			</div>

			<div id="content-tab" class="content-tab">

				<div id="tabs-"<?php echo $tab_num?> class="clearfix tab consult">

 <!-- --!-->
 <style type="text/css">
<!--
@import url("style.css");
-->
</style>
<?php
   //---> Rubrique valide ?
   $rubrique_id = getRubriqueId($connexion,"partie_synthese");

   //---> Tester la session et importer les variables : $select, $mod, $insert, $delete
   //     relatives aux privilèges de l'utilisateur et de la rubrique en cours
   include "../include/session_test.php";

   include "../include/partie_synthese.php";     //---> Les fonctions du module partie_synthese
          //---> Utiliser le module pagination
   if (isset($_GET['candidat_id'])) {
    $candidat_id= $_GET['candidat_id'];
	} else
	{ $candidat_id="";
	}  //--> end if
	if (isset($_GET['operateur_id'])) {
    $operateur_id= $_GET['operateur_id'];
	} else
	{ $operateur_id="";
	}  //--> end if


   //---> Procédure de suppression
   if (isset($_POST['supprimer']) && count($_POST['supprimer']) > 0 && $delete=='Y')
   {
	 partie_synthese_supprimer($_POST['supprimer']);
   } //Fsi

   if (isset($_POST['id']) && $mod=='Y')
   {
     //---> Procédure de modification "visible"
     if (isset($_POST['visible']))
	   partie_synthese_visible($_POST['visible'],$_POST['id']);
	 else
	   partie_synthese_visible(NULL,$_POST['id']);  // Tous à faux


   } //Fsi
    if (isset($_POST['dsens']) && $mod=='Y')
		{
		 if ($_POST['dsens']=="bas")
		   partie_synthese_deplacer($_POST['d_id'],"bas");
		 else
		   partie_synthese_deplacer($_POST['d_id'],"haut");
		}

  /*********************************************************************************************************
                                            Gestion de la pagination
  **********************************************************************************************************/
  //---> Créer un objet de pagination sans condition SQL sur la table
  $p = new CAdminPagination($connexion,"partie_synthese","partie_synthese_id<>4", 8, "partie_synthese_position");
  $p->writeJavaScript();    //---> Générer le code JavaScript correspondant
?>



<table width="99%" border="0" align="right" cellpadding="0" cellspacing="0" >
<tr valign="top">
  <td colspan="2" height="20"></td>
</tr>
  <tr valign="top">
  <td width="15" height="25"></td>
  <td>
    <h2>Liste des synthèses</h2>
  </td>
</tr>
<tr>
  <td colspan="2" height="2" height="20"bgcolor="#FF0000"></td>
</tr>

 <tr valign="top">
  <td colspan="2" height="20"></td>
</tr>
<tr valign="top">
  <td height="25" colspan="2">
    <!-- Début du tableau avec un système de pagination -->
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
	  <td colspan="2">
        <!-- Début de l'enête de pagination -->
		<?php
		  $res = $p->makeButtons($action);    //---> Afficher les bouttons
		?>
		<!-- Fin de l'enête de pagination -->	  </td>
	</tr>
	<tr>
	  <td height="5"></td>
    </tr>
	<tr>
	  <td align="left">
	  <form name="ordre" method="post" action="">
		   <input type="hidden" name="dsens"   value="">
           <input type="hidden" name="d_id"    value="">
		</form>

	    <?php
		  if($p->getTotal()!=0 && $select=='Y') //---> Autorisations suffisantes ?
          {
		?>
	    <form name="pagination_tab" method="post" action="">
		<!-- Début du tableau de contenu -->
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr>
            <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF" class="menutext">
                <tr bgcolor="#B6B6B6" class="tcat">
                  <td width="25" align="center" > N&deg; </td>
                  <td width="295" bgcolor="#B6B6B6">  Nom

                  </td>
               <td  width="80"align="center"  >Questions</td>



                </tr>
                <?php
	          $i = 0;
			   $nbre=@$res->rowCount();
		       $n=0;
	          while($row=@$res->fetch())
	          {
			    $i++;
				$n++;
			    $disabled            = ($mod!='Y')? "disabled" : "";
				$color               = ($i%2!=0)? "#EFEFEF" : "#E9E9E9";
				$session             = $_GET["session"];
				 $partie_synthese_id     = $row['partie_synthese_id'];
				 $question_ap     = $row['question_ap'];
			    $partie_synthese_nom     = affichage($row['partie_synthese_nom'],"---");
				 $partie_synthese_visible   = ($row['partie_synthese_visible']=='Y')? "CHECKED" : "";

				//-->les catégories de chaque partie_synthese
			$sql2 = " SELECT DISTINCT question_id 	 FROM question WHERE  partie_synthese_id = '$partie_synthese_id' ";
			$res2 = $connexion->query($sql2);
			$nb = $res2->rowCount();

			?>
              <tr bgcolor="#E0DFE3" onMouseOver="this.style.background='#FCFCFC'" onMouseOut="this.style.background='#E0DFE3'">
			  <td height="85" align="center"  class="menutext" ><?php  echo $p->courent*$p->page+$i?>
                <input type="hidden" name="id[]" value="<?php  echo $row['partie_synthese_id']?>" /></td>
            <td align="left"><?php


				  echo $partie_synthese_nom;

              ?>            </td>
			  <td width="80" align="center" valign="middle" >
					<a href="./?<?php  echo $action?>=question_candidat_op&<?php  echo $link?>&question_ap=<?php  echo $question_ap?>&partie_synthese_id=<?php  echo $partie_synthese_id?>&candidat_id=<?php  echo $candidat_id?>&operateur_id=<?php  echo $operateur_id?>" class="menutext"><?php echo $nb; ?></a>
					</td>



              </tr>
              <?php
			  } //FTQ
			?>
            </table></td>
          </tr>
        </table>

		<a href="../?<?php  echo $action?>=candidat" class="menutext" target="_blank">
          Aper&ccedil;u        </a>
		<?php
		  } //Fsi
		?>
		<!-- Fin du tableau de contenu -->
	    </form>	  </td>
	</tr>
	<tr>
	  <td height="5"></td>
    </tr>

    </table>
    <!-- Fin du tableau -->  </td>
</tr>
<tr>
  <td height="15" colspan="2"></td>
</tr>
</table>
  <!-- --!-->

				</div>
			</div>

		</div>
	</div>
</section>

<form   action="" method="post" name="change"  id='change' enctype="multipart/form-data">

			<div class="container">
			   <!-- Trigger the modal with a button -->


			  <!-- Modal -->
			  <div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">

				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">
						 <p><img src="../images/logo.gif" width="100"></p><br>

					  </h4>
					</div>
					<div class="modal-body">

					  <p> Nouveau mot de passe
					  <input type="password"   placeholder="Nouveau mot de passe" name="operateur_password" id="operateur_password"
								value=""/>

					  </p>
					</div>

					<div class="modal-footer">
<button type="submit" class="btn btn-default" data-dismiss="modal" name="save" value="save"
					onclick="document.location='./?p=editer_profil&session=<?php echo $idses ?>&password='+document.change.operateur_password.value;">Enregistrer</button>
										  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>

				</div>
			  </div>

			</div>

</form>