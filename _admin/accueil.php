<?php include('include/header-profil-operateur.php');?>

<section id="cv-tabs" class="cv-tabs cv-operateurs">

	<div class="cv-tabs-inner clearfix">
		<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

		<ul id="tabs" class="tabs clearfix">
			<li><a class="tab1 activate" id="#tabs-1"id="firstonglet" href="./?<?php  echo $action?>=operateur_profil&<?php  echo $link?>#tabs-1" >Mon Profil</a></li>
			<li><a class="tab2"  id="#tabs-2" href="./?<?php  echo $action?>=liste_candidats&<?php  echo $link?>#tabs-2" >Candidats</a></li>
			<li><a class="tab3" id="#tabs-3"  href="./?<?php  echo $action?>=quest&<?php  echo $link?>#tabs-3"  >Synthèse</a></li>
			<li><a class="tab4" id="#tabs-4"  href="./?<?php  echo $action?>=competence_metier&&<?php  echo $link?>#tabs-4">Compétences</a></li>

		</ul>
	</div>

	<div id="content-tab" class="content-tab">

		<div id="tabs-"<?php echo $tab_num?> class="clearfix tab consult">

				<!-- ACCÉS BASE PRINCIPALE -->
<?php

    $id_operateur = $_SESSION['operateur_id'] ;

 $sql_operateur = "select * from operateur where 	operateur_id='$id_operateur'";
	$res_operateur = $connexion->query($sql_operateur);
	$donnees_operateur  = $res_operateur->fetch();

	  $operateur_raison_social = $donnees_operateur['operateur_raison_social'] ;
	  $operateur_domaine_activite = $donnees_operateur['operateur_domaine_activite'] ;
	  $sirete = $donnees_operateur['operateur_num_siret'] ;
	  $operateur_date_modif = $donnees_operateur['operateur_date_modif'] ;
	  $operateur_photo = $donnees_operateur['operateur_photo'] ;
	  $operateur_adresse = $donnees_operateur['operateur_adresse'] ;
	  $operateur_code_postale = $donnees_operateur['operateur_code_postale'] ;
	  $operateur_vile   = $donnees_operateur['operateur_vile'] ;
	  	  $operateur_tel = $donnees_operateur['operateur_tel'] ;
	  $operateur_ref = $donnees_operateur['operateur_ref'] ;



?>
				<article class="profil-content-inner clearfix">
					<figure id="portrait" class="portrait portrait-operateur">
						<img src="../common/Images/operateur/<?php  echo $operateur_photo?>"    alt="logo-photo-operateur"/>
					</figure>
					<div class="cv-text">
						<h2><?php echo utf8_decode( $operateur_raison_social); ?></h2>
						 <h2 class="skills">Domaine de compétences de l'opérateur<?php echo $operateur_domaine_activite; ?></h2>

 						<h3><?php echo $operateur_adresse; ?> - <?php echo $operateur_vile; ?>,<?php echo $operateur_code_postale; ?></h3>
						<h3 class="tel">Tél :  <?php echo $operateur_tel; ?></h3><span class="seperate">|</span><h3 class="siret">Siret : <?php echo $sirete; ?></h3>
						<ul id="ref" class="ref">
							 							<li>Dernière mise à jour : <?php echo $operateur_date_modif; ?></li>

							<li>Réf. <?php echo $operateur_ref; ?></li>
						</ul>
					</div>
				</article>
		</div>
	</div>
</section>
