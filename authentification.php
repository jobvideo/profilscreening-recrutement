<?php
session_start();
setcookie("sid", 'Y');
include_once ("./include/connexion.php");
include_once ("./include/fonctions.php");
include_once ("./include/parametres.php");

$status = "ok";  // peut prendre : no, yes, inactif
$lang   = "fr";

if ((isset($_POST['login_pass'])) && (isset($_POST['user_login']))) {
    $pw   = lecture($_POST['login_pass']);
    $user = lecture($_POST['user_login']);
    $sql  = "SELECT *
	         FROM  admin_user
			 WHERE admin_user_nom      = '$user' AND
			       admin_user_password = '" . md5($pw) . "'";
    $res  = $connexion->query($sql);
    $row  = @$res->fetch();
    if ($row == FALSE) { //---> Login invalide
        $status = "no";
    }
    else { //---> Login correct
        if ($row['admin_user_actif'] == 'N') { //---> Utilisateurs désactivés
            unset($_COOKIE["sid"]);
            $status = "inactif";
        }
        else { //--> Ok !
            $date               = time();
            $iduser             = $row['admin_user_id'];
            $role               = $row['admin_user_role'];
            $_SESSION['iduser'] = $iduser;
            $_SESSION['nom']    = $user;
            $_SESSION['role']   = $role;

            if ($role == 'operateur') {
                $sql                      = "select operateur_id from operateur where admin_user_id = '$iduser'";
                $res                      = $connexion->query($sql);
                $row                      = $res->fetch();
                $operateur_id             = $row['operateur_id'];
                $_SESSION['operateur_id'] = $operateur_id;
            }
            else
            if ($role == 'entreprise') {
                $sql                       = "select entreprise_id from entreprise where admin_user_id = '$iduser'";
                $res                       = $connexion->query($sql);
                $row                       = $res->fetch();
                $_SESSION['id_entreprise'] = $row['entreprise_id'];
            }
            if ($role == 'candidat') {
                $sql                     = "select candidat_id from candidat where admin_user_id = '$iduser'";
                $res                     = $connexion->query($sql);
                $row                     = $res->fetch();
                $_SESSION['candidat_id'] = $row['candidat_id'];
            }

            $sql        = "INSERT INTO session_admin
		           SET user_id      = '$iduser' ,
				       dat_ouv_ses  = '$date',
                                    dat_ferm_ses = '$date'   ";
            $res        = $connexion->query($sql);
            $session_id = $connexion->lastInsertId();
            $idses      = md5($session_id);
            $desactive  = 'yes';
            ?>
            <script language='JavaScript' type="text/javascript">

                document.location.href = './_<?php echo $role; ?>/index.php?session=<?php echo $idses; ?>&lang=<?php echo $lang ?>';

            </script>
            <?php
            exit();
        } //Fsi
    } //Fsi
} //Fsi

include('include/header.php');
?>

<body id="body" class="page page-login d-flex justify-content-center align-items-center">
    <div class="overlay" id="overlay"></div>
    <main id="main" role="main" class="d-flex flex-column align-items-center">
         <header id="header" class="header">
	        <h1 class="logo-login">Profilscreening©</h1>
	        <h2>Connexion à l'Espace<br><span>Outil d'aide au recrutement</span></h2>
        </header>
        <section id="featured-log" class="featured-log">
            <div id="log-content" class="log-content d-flex flex-column">
                <form name="loginform" id="loginform" action="#" method="post">

                    <p class="login-user_loginname">
                        <label for="user_login">Identifiant</label>
                        <input type="text" name="user_login" id="user_login" class="input" value=""/>
                    </p>
                    <p class="login-password">
                        <label for="login_pass">Mot de passe</label>
                        <input type="password" name="login_pass" id="login_pass" class="input" value=""/>
                    </p>
                    <button class="login-submit d-flex align-items-center">
                        <input aria-label="Je me connecte à mon espace candidat ou entreprise" type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="Je me connecte" /><i class="ti-lock"></i>
                    </button>
                </form>
            </div>
        </section>
<?php include('include/footer-authentification.php');?>