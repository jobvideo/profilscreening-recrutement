<?php include('include/header-inscriptions.php');?>

<body id="body" class="page page-login d-flex justify-content-center align-items-center">
    <div class="overlay" id="overlay"></div>
    <main id="main" role="main" class="d-flex flex-column align-items-center">
         <header id="header" class="header">
	        <h1 class="logo-login">Profilscreening©</h1>
	        <h2>Oups...<br><span> J'ai oublié mon mot de passe !</span></h2>
        </header>
        <section id="featured-log" class="featured-log">
            <div id="log-content" class="log-content d-flex flex-column">
                <form name="loginform" id="loginform" action="#" method="post">
					<p class="forget-pass-resume">Merci d'indiquer votre identifiant et de cliquer sur le bouton "Nouveau mot de passe" pour en recevoir un nouveau.</p>
                    <p class="login-user_loginname">
                        <label for="user_login">Identifiant</label>
                        <input type="text" name="user_login" id="user_login" class="input" value=""/>
                    </p>
                    <div class="g-recaptcha recaptcha-container recaptcha-pass" data-sitekey="6LfpnngUAAAAAOyFOmEpnyKOJe48uwKHtZX12aM3"></div>
                    <button class="login-submit d-flex align-items-center btn-new-pass">
                        <input aria-label="Je me connecte à mon espace candidat ou entreprise" type="submit" name="wp-submit" id="wp-submit" value="Nouveau mot de passe" /><i class="ti-lock"></i>
                    </button>
                </form>
            </div>
        </section>
<?php include('include/footer-inscriptions.php');?>