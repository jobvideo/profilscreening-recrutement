<?php

$lang = "fr";

function entreprise_deplacer($id, $sens) {
    executer("LOCK TABLES entreprise WRITE", __FILE__, __LINE__);
    if ($sens == "bas") {
        $sql = "SELECT MIN(entreprise_position)  as MAX 
			FROM   entreprise
			WHERE  entreprise_position > $id";
    } elseif ($sens == "haut") {
        $sql = "SELECT MAX(entreprise_position) as MAX
			FROM   entreprise
			WHERE  entreprise_position < $id";
    }
    $res = $connexion->query($sql);
    $row = $res->fetch();

    $id2 = $row['MAX'];
    if ($id2 != "") {
        $sql = "UPDATE entreprise
	        SET    entreprise_position         = 0
			WHERE  entreprise_position          = $id2";
        $res = $connexion->prepare($sql);
        $res->execute();
        $sql = "UPDATE entreprise
	        SET    entreprise_position          = $id2
			WHERE  entreprise_position          = $id";
        $res = $connexion->prepare($sql);
        $res->execute();
        $sql = "UPDATE entreprise
	        SET    entreprise_position          = $id
			WHERE  entreprise_position          = 0";
        $res = $connexion->prepare($sql);
        $res->execute();
    } //Fsi
    executer("UNLOCK TABLES", __FILE__, __LINE__);
}

//Fin clients_deplacer_bas

/* * **************************************************************************

  /****************************************************************************
  Supprime une liste d'actualit�s
 * ************************************************************************** */

function entreprise_supprimer($tab) {
    global $lang;

    if (count($tab) == 0)         // Param�tre vide ?
        return;

    $str = implode(", ", $tab);

    //---> Supprimer les images
    foreach ($tab as $entreprise_id) {
        //--> selection du nom de limage
        $sql = " SELECT entreprise_photo, entreprise_fichier FROM entreprise WHERE entreprise_id = $entreprise_id ";
        $res = $connexion->query($sql);
        $row = $res->fetch();

        $entreprise_photo = $row['entreprise_photo'];
        $entreprise_fichier = $row['entreprise_fichier'];

        @unlink("../common/Images/entreprise/$entreprise_photo");
        @unlink("../$lang/downloads/$entreprise_fichier");
    }

    //---> Supprimer les images de la une 


   


      $sql = "DELETE FROM entreprise
      WHERE entreprise_id IN ($str)";
      $res =  $connexion->prepare($sql);$res->execute();

  } //Fin entreprise_supprimer
      /**************************************************************************************************
      Modifier la propri�t� visible d'un ensemble de entreprise
     * ************************************************************************************************ */
    function entreprise_visible($tab, $tab_id) {
        $id = implode(", ", $tab_id);
        $sql = "UPDATE entreprise
          SET entreprise_visible = 'N'
          WHERE entreprise_id IN ($id)";
        $res = $connexion->prepare($sql);
        $res->execute();
        if (count($tab) > 0) {
            $str = implode(", ", $tab);
            $sql = "UPDATE entreprise
            SET   entreprise_visible = 'Y'
            WHERE entreprise_id IN ($str)";
            $res = $connexion->prepare($sql);
            $res->execute();
        } //Fsi			
    }

//Fin entreprise_visible
    /*     * ************************************************************************************************
      Modifier la propri�t� une d'un ensemble de entreprise
     * ************************************************************************************************ */

    function entreprise_une($tab, $tab_id) {
        $id = implode(", ", $tab_id);
        $sql = "UPDATE entreprise
          SET   entreprise_une = 'N'
          WHERE entreprise_id IN ($id)";
        $res = $connexion->prepare($sql);
        $res->execute();
        if (count($tab) > 0) {
            $str = implode(", ", $tab);
            $sql = "UPDATE entreprise
            SET   entreprise_une = 'Y'
            WHERE entreprise_id IN ($str)";
            $res = $connexion->prepare($sql);
            $res->execute();
        } //Fsi			
    }

//Fin entreprise_une

    function entreprise_new($tab, $tab_id) {
        $id = implode(", ", $tab_id);
        $sql = "UPDATE entreprise
          SET   entreprise_new = 'N'
          WHERE entreprise_id IN ($id)";
        $res = $connexion->prepare($sql);
        $res->execute();
        if (count($tab) > 0) {
            $str = implode(", ", $tab);
            $sql = "UPDATE entreprise
            SET   entreprise_new = 'Y'
            WHERE entreprise_id IN ($str)";
            $res = $connexion->prepare($sql);
            $res->execute();
        } //Fsi			
    }

//Fin entreprise_new
    /*     * **************************************************************************
      Mettre � jour le fichier de la photo
      et
      Supprimer le fichier pr�c�dent

     * ************************************************************************** */

    function entreprise_photo_update($entreprise_id, $filename, $ext) {
        global $lang;
        $sql = " SELECT entreprise_photo FROM entreprise WHERE entreprise_id = '$entreprise_id'";
        $res = $connexion->query($sql);
        $row = $res->fetch();
        $entreprise_photo = stripcslashes($row['entreprise_photo']);
        if ($entreprise_photo != 'img_vide.gif') {
            @unlink("../common/Images/entreprise/$entreprise_photo");
        }

        $fn = "entreprise($entreprise_id).$ext";
        copy($filename, "../common/Images/entreprise/$fn");
        $sql = "UPDATE entreprise
	      SET    entreprise_photo = '$fn'
		  WHERE  entreprise_id    = " . $_GET['entreprise_id'];
        //echo '<script> entreprise_photo = '.$entreprise_photo.'</script>';
        $res = $connexion->prepare($sql);
        $res->execute();
    }

//Fin entreprise_photo_update
    /*     * **************************************************************************
      Mettre � jour le fichier
      et
      Supprimer le fichier pr�c�dent
     * ************************************************************************* */

//***************************************************************************/

    function entreprise_fichier_update($entreprise_id, $filename, $ext) {
        global $lang;
        $sql = " SELECT entreprise_fichier FROM entreprise WHERE entreprise_id = '$entreprise_id'";
        $res = $connexion->query($sql);
        $row = $res->fetch();
        $entreprise_fichier = stripcslashes($row['entreprise_fichier']);
        @unlink("../common/Fichiers/$entreprise_fichier");
        $fn = "entreprise_fichier($entreprise_id).$ext";
        // copy($filename, "../common/Fichiers/$fn");
        $sql = "UPDATE entreprise
	      SET    entreprise_fichier = ''
		  WHERE  entreprise_id    = " . $_GET['entreprise_id'];
        $res = $connexion->prepare($sql);
        $res->execute();
    }

//Fin entreprise_photo_update
    /*     * **************************************************************************
      Mettre � jour le fichier de la photo de la une
      et
      Supprimer le fichier pr�c�dent
     * ************************************************************************** */

    /*     * **************************************************************************
      D�finir l'objet de pagination
     * ************************************************************************** */

    function entreprise_pagination_object() {
        $p = new CPagination("entreprise", "entreprise_visible = 'Y'", 5, "entreprise_raison_social", "ASC");
        return $p;
    }

//Fin entreprise_pagination_object
?>