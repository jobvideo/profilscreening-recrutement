		<footer class="footer-inscriptions">
			<a href="/nouveau-mot-de-passe.php" class="forget-password" title="Regénérer un mot de passe" aria-label="Vous avez oublié votre mot de passe vous pouvez en demander un autre Profilscreening©">
				Mot de passe oublié<i class="ti-lock"></i>
			</a>
			<a href="https://profilscreening.fr/" class="come-back" title="Retour à l'accueil du site général" aria-label="Revenir au site explicatif de l'outil d'aide au recrutement Profilscreening©">
				<i class="ti-arrow-left"></i>Retour au site Profilscreening©
			</a>
		</footer>
	</main>
	<?php include('footer-scripts.php');?>
	</body>
</html>