<div class="overlay" id="overlay"></div>
	<main id="main" role="main">
		<header id="header" class="header clearfix" aria-label="navigation principale">
			<nav id="nav-header" class="nav-header wrapper clearfix" >
				<div id="nav-inner" class="navInner">
					<a href="#" id="nav-Responsive" class="navResponsive" title="ouvrir la navigation"><span class="menuBurger">ouvrir la navigation</span></a>
					<a id="logo" class="logo" href="https://profilscreening.fr/" title="retour à l'accueil">Profilscreening©</a>
				</div>
				<div id="nav-top" class="menu-entreprises-container navTop">
					<ul id="menu-entreprises" class="menu-header clearfix">
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123">
							<a title="Se déconnecter et retourner à l'accueil du site général" href="https://profilscreening.fr">Accueil</a>
						</li>
						<li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124">
							<a title="Se déconnecter" href="index.php?session=<?php echo $idses?>&ferms=1">Déconnexion</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>