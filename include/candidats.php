<?php
$lang="fr";
function candidat_deplacer($id,$sens,$id_cat)
{
  executer("LOCK TABLES candidat WRITE",__FILE__,__LINE__);
  if ($sens=="bas") 
  {
	$sql = "SELECT MIN(candidat_position) as MAX 
			FROM   candidat
			WHERE  candidat_position > $id and candidat_operateur_id='$id_cat'";
 }
  elseif($sens=="haut")
 {
	  $sql = "SELECT MAX(candidat_position) as MAX 
			FROM   candidat
			WHERE  candidat_position < $id  and candidat_operateur_id='$id_cat'";
  }
  $res = $connexion->query($sql);
  $row  = $res->fetch();
   $id2 = $row['MAX'];
  if ($id2!="")
  {
	$sql = "UPDATE candidat
	        SET    candidat_position         = 0
			WHERE  candidat_position          = $id2  and candidat_operateur_id='$id_cat'";
	$res = $connexion->prepare($sql);$res->execute();
	$sql = "UPDATE candidat
	        SET    candidat_position          = $id2
			WHERE  candidat_position          = $id  and candidat_operateur_id='$id_cat'";
	$res = $connexion->prepare($sql);$res->execute();
	$sql = "UPDATE candidat
	        SET    candidat_position          = $id
			WHERE  candidat_position          = 0  and candidat_operateur_id='$id_cat'"  ;
	$res = $connexion->prepare($sql);$res->execute();
  } //Fsi
  executer("UNLOCK TABLES",__FILE__,__LINE__);
} //Fin clients_deplacer_bas

/****************************************************************************

/****************************************************************************
                      Supprime une liste des candidats
****************************************************************************/
function candidat_supprimer($tab) {
global $lang;

  if (count($tab)==0)         // Param�tre vide ?
    return;
	
  $str = implode(", ", $tab);
   
  
  //---> Supprimer les images
  foreach($tab as $candidat_id)
    {
    //--> selection du nom de limage
   echo  $sql = " SELECT candidat_photo FROM candidat WHERE candidat_id = $candidat_id ";
	$res = $connexion->query($sql);
  $row  = $res->fetch();
 	$candidat_photo = $row['candidat_photo'];
	 
	 
    @unlink("../common/Images/candidat/$candidat_photo");
	 
  }		  
   //---> Supprimer les images de la une 
    
  
   
  $sql = "DELETE FROM candidat
          WHERE candidat_id IN ($str)";
  $res =  $connexion->prepare($sql);$res->execute();
} //Fin candidat_supprimer
/**************************************************************************************************
                       Modifier la propri�t� visible d'un ensemble de candidat
**************************************************************************************************/
function candidat_visible($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE candidat
          SET candidat_visible = 'N'
          WHERE candidat_id IN ($id)";
  $res =  $connexion->prepare($sql);$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE candidat
            SET   candidat_visible = 'Y'
            WHERE candidat_id IN ($str)";
    $res =  $connexion->prepare($sql);$res->execute();     
  } //Fsi			
} //Fin candidat_visible
/**************************************************************************************************
                       Modifier la propri�t� une d'un ensemble de candidat
**************************************************************************************************/
function candidat_une($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE candidat
          SET   candidat_une = 'N'
          WHERE candidat_id IN ($id)";
  $res =  $connexion->prepare($sql);$res->execute(); 
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE candidat
            SET   candidat_une = 'Y'
            WHERE candidat_id IN ($str)";
    $res =  $connexion->prepare($sql);$res->execute();     
  } //Fsi			
} //Fin candidat_une

function candidat_new($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE candidat
          SET   candidat_new = 'N'
          WHERE candidat_id IN ($id)";
  $res =  $connexion->prepare($sql);$res->execute(); 
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE candidat
            SET   candidat_new = 'Y'
            WHERE candidat_id IN ($str)";
    $res =  $connexion->prepare($sql);$res->execute();     
  } //Fsi			
} //Fin candidat_new
/****************************************************************************
                   Mettre � jour le fichier de la photo
				                  et
					Supprimer le fichier pr�c�dent

****************************************************************************/
function candidat_photo_update($candidat_id, $filename, $ext,$connexion)
{
global $lang;
  $sql = " SELECT candidat_photo FROM candidat WHERE candidat_id = '$candidat_id'";
 
  $res = $connexion->query($sql);
  $row  = $res->fetch();   
   $candidat_photo = stripcslashes(  $row['candidat_photo']);
 if($candidat_photo != 'img_vide.gif')
	{@unlink("../common/Images/candidat/$candidat_photo");}
	
  $fn  = "candidat($candidat_id).$ext";
   
  copy($filename, "../common/Images/candidat/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_photo = '$fn'
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
		  //echo '<script> candidat_photo = '.$candidat_photo.'</script>';
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_photo_update
/****************************************************************************
                   Mettre � jour le fichier de la photo de la une 
				                  et
					Supprimer le fichier pr�c�dent
***************************************************************************/
 //***************************************************************************
//***************************************************************************
/****************************************************************************
                  
//***************************************************************************/

function candidat_synthese_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_synthese,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $candidat_synthese = stripcslashes($row['candidat_synthese']);
  $candidat_reference =  stripcslashes($row['candidat_reference']);
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_synthese");
  $fn  = "candidat_synthese($candidat_id).$ext";
 copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_synthese = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_image_update

function candidat_lettre_motivation_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_lettre_motivation,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $candidat_lettre_motivation = stripcslashes($row['candidat_lettre_motivation']);
  $candidat_reference =  stripcslashes($row['candidat_reference']);
  
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_lettre_motivation");
  $fn  = "candidat_lettre_motivation($candidat_id).$ext";
   copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_lettre_motivation = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
  
} //Fin candidat_image_update

function candidat_video_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_video,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $candidat_video = stripcslashes($row['candidat_video']);
  $candidat_reference =  stripcslashes($row['candidat_reference']);
  
  
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_video");
  $fn  = "candidat_video($candidat_id).$ext";
  copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_video = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_image_update

function candidat_cv_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_cv,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
   $res = $connexion->query($sql);
	$row  = $res->fetch();
	 
echo "eee";exit;
  
  $candidat_cv = stripcslashes($row['candidat_cv']);
  $candidat_reference =  stripcslashes($row['candidat_reference']);
   
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_cv");
  $fn  = "candidat_cv($candidat_id).$ext";
 copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_cv = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_image_update

function candidat_attestation_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_attestation,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
   $res = $connexion->query($sql);
	$row  = $res->fetch();
	 

  $candidat_attestation = stripcslashes($row['candidat_attestation']);
  $candidat_reference =  stripcslashes($row['candidat_reference']);
   
    
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_attestation");
  $fn  = "candidat_attestation($candidat_id).$ext";
 copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_attestation = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_image_update

function candidat_diplome2_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_diplome2,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
   $res = $connexion->query($sql);
	$row  = $res->fetch();
	 

  $candidat_diplome2 = stripcslashes($row['candidat_diplome2']);
  $candidat_reference =  stripcslashes($row['candidat_reference']);
   
   
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_diplome2");
  $fn  = "candidat_diplome2($candidat_id).$ext";
 copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_diplome2 = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_image_update

function candidat_certificat_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_certificat,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
   $res = $connexion->query($sql);
	$row  = $res->fetch();
 
  $candidat_certificat = stripcslashes($row['candidat_certificat']);
  $candidat_reference =  stripcslashes($row['candidat_reference']);
    
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_certificat");
  $fn  = "candidat_certificat($candidat_id).$ext";
 copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_certificat = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_image_update

function candidat_fichier_update($candidat_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT candidat_fichier,candidat_reference FROM candidat WHERE candidat_id = '$candidat_id'";
 $res = $connexion->query($sql);
	$row  = $res->fetch();
   $candidat_fichier = stripcslashes($row['candidat_fichier']);
  $candidat_reference = stripcslashes($row['candidat_reference']);
  $dossier = '../common/Fichiers/dossier_'.$candidat_reference;

  @unlink($dossier."/$candidat_fichier");
  $fn  = "candidat_fichier($candidat_id).$ext";
 copy($filename, $dossier."/$fn");
  $sql = "UPDATE candidat
	      SET    candidat_fichier = ''
		  WHERE  candidat_id    = " . $_GET['candidat_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin candidat_image_update
/****************************************************************************
                   Mettre � jour le fichier de la photo de la une 
				                  et
					Supprimer le fichier pr�c�dent
****************************************************************************/

/****************************************************************************
   			        D�finir l'objet de pagination
****************************************************************************/
function candidat_pagination_object()
{
 $p   = new CPagination("candidat","candidat_visible = 'Y'",5,"candidat_nom","ASC");
 return $p;
} //Fin candidat_pagination_object
?>