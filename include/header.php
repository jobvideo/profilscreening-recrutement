<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="fr-FR"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="fr-FR"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<!--<![endif]-->
<html lang="fr-FR">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Recrutement | Profilscreening.fr</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="shortcut icon" href="/images/profilscreening/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/images/profilscreening/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" media="all" href="/css/styles.css" />
        <link rel="stylesheet" type="text/css" media="all" href= "/css/style_admin.css">
        <link rel="pingback" href="http://jobvideo.fr/xmlrpc.php" />
        <script language="javascript" type="text/javascript" src="../js/scripts.js"></script>
              <!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
              <!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
        <!--[if IE]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen"><![endif]-->
        <!--[if lt IE 9]><style>.content{height: auto; margin: 0;}	.content div {position: relative;}</style><![endif]-->


        <?php
        header('Content-Type: text/html; charset=utf-8'); // écrase l'entête utf-8 envoyé par php
        ini_set('default_charset', 'utf-8');
        ?>
    </head>