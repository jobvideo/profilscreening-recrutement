<?php
$lang="fr";
/****************************************************************************
                      Supprime une liste d'actualit�s
****************************************************************************/
function operateur_supprimer($tab) {
global $lang;
  if (count($tab)==0)         // Param�tre vide ?
    return;
	
  $str = implode(", ", $tab);
   
  //---> Supprimer les images
  foreach($tab as $operateur_id)
    {
    //--> selection du nom de limage
    $sql = " SELECT operateur_photo FROM operateur WHERE operateur_id = $operateur_id ";
	$res = $connexion->query($sql);
    $row  = $res->fetch();
	$operateur_photo = $row['operateur_photo'];
	
    @unlink("../common/Images/operateur/$operateur_photo");
  }		  
  
   //---> Supprimer la liste des produits de de ces operateurs
  include "operateur_candidats.php";
  $sql = "SELECT  DISTINCT candidat_id
          FROM   candidat
		  WHERE  candidat_operateur_id IN ($str)";
  $res = $connexion->query($sql);
   $id  = array(); 
  while ($row =   $res->fetch())
  {
    $id[] = $row['candidat_id'];
  } //FTQ
  candidat_supprimer($id);
  
  //---> Suppresion effective de la base de donn�es
  $sql = "DELETE FROM operateur
          WHERE operateur_id IN ($str)";
  $res =  $connexion->prepare($sql);$res->execute();
} //Fin operateur_supprimer
/**************************************************************************************************
                       Modifier la propri�t� visible d'un ensemble de operateur
**************************************************************************************************/
function operateur_visible($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE operateur
          SET operateur_visible = 'N'
          WHERE operateur_id IN ($id)";
  $res =  $connexion->prepare($sql);$res->execute();
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE operateur
            SET   operateur_visible = 'Y'
            WHERE operateur_id IN ($str)";
    $res =  $connexion->prepare($sql);$res->execute();     
  } //Fsi			
} //Fin operateur_visible
/**************************************************************************************************
                       Modifier la propri�t� une d'un ensemble de operateur
**************************************************************************************************/
function operateur_une($tab, $tab_id)
{
  $id  = implode(", ", $tab_id);
  $sql = "UPDATE operateur
          SET   operateur_une = 'N'
          WHERE operateur_id IN ($id)";
  $res =  $connexion->prepare($sql);$res->execute(); 
  if (count($tab)>0)
  {
    $str = implode(", ", $tab);
    $sql = "UPDATE operateur
            SET   operateur_une = 'Y'
            WHERE operateur_id IN ($str)";
    $res =  $connexion->prepare($sql);$res->execute();     
  } //Fsi			
} //Fin operateur_une

////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
function operateur_deplacer($id,$sens)
{
  executer("LOCK TABLES operateur WRITE",__FILE__,__LINE__);
  if ($sens=="bas") 
  {
	$sql = "SELECT MIN(operateur_position) as MAX 
			FROM   operateur
			WHERE  operateur_position > $id";
 }
  elseif($sens=="haut")
 {
	$sql = "SELECT MAX(operateur_position) as MAX 
			FROM   operateur
			WHERE  operateur_position < $id";
  }
  $res = $connexion->query($sql);
  $row  = $res->fetch();
   $id2 = $row['MAX'] ;
  if ($id2!="")
  {
	$sql = "UPDATE operateur
	        SET    operateur_position         = 0
			WHERE  operateur_position          = $id2";
	$res = $connexion->prepare($sql);$res->execute();
	$sql = "UPDATE operateur
	        SET    operateur_position          = $id2
			WHERE  operateur_position          = $id";
	$res = $connexion->prepare($sql);$res->execute();
	$sql = "UPDATE operateur
	        SET    operateur_position          = $id
			WHERE  operateur_position          = 0"  ;
	$res = $connexion->prepare($sql);$res->execute();
  } //Fsi
  executer("UNLOCK TABLES",__FILE__,__LINE__);
} //Fin clients_deplacer_bas

/****************************************************************************
                   Mettre � jour le fichier de la photo
				                  et
					Supprimer le fichier pr�c�dent
****************************************************************************/
function operateur_photo_update($operateur_id, $filename, $ext)
{
global $lang;
  $sql = " SELECT operateur_photo FROM operateur WHERE operateur_id = '$operateur_id'";
  $res = $connexion->query($sql);
  $row  = $res->fetch();
  $operateur_photo = stripcslashes($row['operateur_photo']);
  if($operateur_photo != 'img_vide.gif')
	{@unlink("../common/Images/operateur/$operateur_photo");}
   $fn  = "operateur($operateur_id).$ext";
  copy($filename, "../common/Images/operateur/$fn");
  $sql = "UPDATE operateur
	      SET    operateur_photo = ''
		  WHERE  operateur_id    = " . $_GET['operateur_id'];
  $res = $connexion->prepare($sql);$res->execute();
} //Fin operateur_photo_update
//****************************************************************************/



/****************************************************************************
   			        D�finir l'objet de pagination
****************************************************************************/
function operateur_pagination_object()
{
 $p   = new CPagination("operateur","operateur_visible = 'Y'",5,"operateur_position","ASC");
 return $p;
} //Fin operateur_pagination_object
?>