<?php include('include/header-profil-operateur.php'); ?>
<link rel="stylesheet" type="text/css" media="all" href= "../css/style_syntehese.css">

<link rel="stylesheet" type="text/css" media="all" href= "../css/style_syntehese.css">

<div id="cv-tabs" class="cv-tabs">

    <div class="cv-tabs-inner clearfix">
        <a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

        <ul id="tabs" class="tabs clearfix">
            <li><a class="tab1 tab-profil" id="#tabs-1"id="firstonglet" href="./?<?php echo $action ?>=operateur_profil&<?php echo $link ?>#tabs-1" >
                    Mon Profil </a></li>
            <li><a class="tab2 tab-candidats"  id="#tabs-2" href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>#tabs-2" >
                    Candidats   </a></li>
            <li><a class="tab3 tab-synthese activate" id="#tabs-3"  href="./?<?php echo $action ?>=quest&<?php echo $link ?>#tabs-3"  >
                    Synthèse  </a></li>
            <li><a class="tab4 tab-competences  activate" id="#tabs-4"  href="./?<?php echo $action ?>=competence_metier&&<?php echo $link ?>#tabs-4">Compétences</a></li>

            <!--
                                                    <li><a class="tab10"  id="#tabs-10" href="javascript:popup('changer_pw.php?admin_user_id=<?php echo $admin_user_id; ?>&<?php echo $link ?>#tabs-10',475,250)" class="menutext">
                                                                    Password		</a></li>
            -->
        </ul>

    </div>
    <script type="text/javascript" language="javascript">

        function verif()
        {

            if (document.form1.question_nom.value == "") {
                alert("Veuillez introduire le titre de la compétence");
                document.form1.question_nom.focus();
                return;
            } //Fsi
            if (document.form1.metier.value == "") {
                alert("Veuillez choisir un métier");
                document.form1.metier.focus();
                return;
            } //Fsi


            document.form1.submit();
        } //Fin verif

        function fermer()
        {
            document.location.href = './?p=competence_metier&<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&partie_synthese_id=4';

        } //Fin ajouter

    </script>
    
    <?php
    //---> Rubrique et id utilsateur valides ?
    $admin_user_id = isset($_GET['admin_user_id']) ? $_GET['admin_user_id'] : 0;
    $rubrique_id = getRubriqueId($connexion, "partie_synthese");

    include "../include/parametres.php";  //---> Tester la session et 

    include "../include/session_test.php"; //---> Tester la session et 
    //     Importer les variables $select, $mod, $insert, $delete
    include "../include/question.php";  //---> Les fonctions du module question 


    $partie_synthese_id = 4;
    if (isset($_GET['question_ap']))
        $question_ap = $_GET['question_ap'];
    else
        $question_ap = 0;

    //---> Le formaulire de la page en cours a été envoyé
    if (isset($_POST['question_nom'])) {
        include "../include/operation_message.php";  //---> inclure fonction pour afficher un message
        $question_nom = lecture($_POST['question_nom']);

        $type = lecture($_POST['type']);
        $metier = lecture($_POST['metier']);

        if (isset($_GET['question_id'])) { //---> il s'agit d'une modification	
            $question_id = $_GET['question_id'];
            $sql = "UPDATE question
	           SET    question_nom             = '$question_nom'         ,
			    type             = '$type'         ,			     
			    metier             = '$metier'         
				WHERE  question_id         =  $question_id            ";
            $res = $connexion->query($sql);           //---> Exécuter la requête
            operation_message("Modification Terminée", TRUE);  //---> Msg + Fermer la fenêtre	  
        } else { //---> il s'agit d'une insertion
            $sql = "SELECT MAX(question_position)  as MAX 
				FROM   question";
            $res = $connexion->query($sql);
            $row = $res->fetch();
            $pos = $row['MAX'];
            $pos++;
            $sql = "INSERT INTO question
	           SET      
					   question_nom             = '$question_nom'        ,
					   question_visible           = 'Y'              ,
					    type             = '$type'         ,
						metier             = '$metier' ,
					   question_position = '$pos'		,
					   partie_synthese_id             = '$partie_synthese_id'    ";
            //---> Exécuter la requête et faire $question_id = @connexion->lastInsertId();
            $res = $connexion->query($sql);
            $question_id = $connexion->lastInsertId();
            operation_message("Insertion Terminée", false);   //---> Msg + Racharger la page d'insertion	 
        } //Fsi
        ?>
        <script>
            fermer();
        </script>
        <?php
    } //Fsi
    //---> Champs du formulaire
    if (isset($_GET['question_id'])) { //---> il s'agit d'une modification
        $op = "Modifier";
        $sql = "SELECT *
	         FROM   question
			 WHERE  question_id = " . $_GET['question_id'];
        $res = $connexion->query($sql); //---> Exécuter la requête
        $row = $res->fetch();
        if ($row == FALSE)
            die("Impossible de trouver l'actualité");
        $question_nom = decode_text($row['question_nom']);
        $question_id = $row['question_id'];
        $metier = $row['metier'];
        $type = $row['type'];
    } else { //---> il s'agit d'une insertion
        $op = "Ajouter";
        $question_nom = "";
        $question_id = NULL;
        $metier = "";
        $type = "";
    } //Fsi
    ?>



    <form name="form1" id="form1"  method="post" action="#">

        <fieldset>
            <legend> Compétences  : </legend><br>

            <br>	 

            <label for="utilise"> Titre : 

                <input name="question_nom" type="text"  style="width:325px;"  value="<?php echo $question_nom; ?>">  

            </label>
            <label for="utilise"> Type : 

                <select type="zone" name="type" class="form-control"style="width:325px;">
                    <option  value="base " <?php echo ($type == 'base' ) ? 'selected' : '' ?>  > Compétence de base </option>
                    <option  value="specifique" <?php echo ($type == 'specifique' ) ? 'selected' : '' ?>  >compétence spécifiques</option>
                </select>

            </label>

            <label for="utilise"> Métier
                <select class="select dispo" name="metier" id="metier">
                    <optgroup>
                        <option value="">Sélectionnez un métier </option>
                        <?php
                        $sql = "select * from metier";
                        $res_rech = $connexion->query($sql);

                        while ($row_rech = $res_rech->fetch()) {
                            $metier_nom = $row_rech['metier_nom'];
                            $metier_id = $row_rech['metier_id'];
                            ?>
                            <option value="<?php echo $metier_id; ?>" <?php echo ($metier_id == $metier) ?"selected": ""?>> <?php echo $metier_nom; ?> </option>

                            <?php
                        }
                        ?>			</optgroup>
                </select>

            </label>
        </fieldset>	 

        <p> 
            <input type="button" name="Submit" value="<?php echo $op ?>" onClick="javascript: verif();" style="width:95px; cursor:pointer ">               

            <input type="button" name="Submit2" value="Fermer" onClick="javascript: fermer();" style="width:95px; cursor:pointer ">             


        </p> 
</div>