<?php include('include/header-profil-operateur.php'); ?>
<link rel="stylesheet" type="text/css" media="all" href= "../css/bootstrap-old.css">

<div id="cv-tabs" class="cv-tabs">

    <div class="cv-tabs-inner clearfix">
        <a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

        <ul id="tabs" class="tabs clearfix">
            <li><a class="tab1" id="#tabs-1"id="firstonglet" href="./?<?php echo $action ?>=operateur_profil&<?php echo $link ?>#tabs-1" >Mon Profil</a></li>
            <li><a class="tab2 activate" id="#tabs-2" href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>#tabs-2" >Candidats</a></li>
             

        </ul>

    </div>

    <div id="content-tab" class="content-tab">

        <div id="tabs-"<?php echo $tab_num ?> class="clearfix tab consult">

            <!-- !-->

            <?php
//---> Rubrique valide ?
            $rubrique_id = getRubriqueId($connexion, "operateurs");

//---> Tester la session et importer les variables : $select, $mod, $insert, $delete
//     relatives aux privil�ges de l'utilisateur et de la rubrique en cours
            include "../include/session_test.php";

            include "../include/operateur_candidats.php";     //---> Les fonctions du module candidat
//---> Utiliser le module pagination
            $operateur_id = $_SESSION['operateur_id'];


            if (isset($_GET['operateur_id'])) {
                $operateur_id = $_GET['operateur_id'];
            }
            else {
                $operateur_id = "";
            }  //--> end if
            if (isset($_GET['candidat_id'])) {
                $candidat_id = $_GET['candidat_id'];
            }
            else {
                $candidat_id = 0;
            }  //--> end if 
            include "../include/operation_message.php";  //---> inclure fonction pour afficher un message
            //---> Le formulaire de suppression de candidat_diplome2 a été envoyée
            //---> Le formulaire de suppression de candidat_diplome2 a été envoyée
            if (isset($_POST['file']) && isset($_GET['candidat_id'])) {
                if ($_POST['file'] == 'image') { //---> Il s'agit de la suppression d'image
                    $ext = 'JPEG';
                    candidat_photo_update($_GET['candidat_id'], "../common/Images/img_vide.gif", $ext, $connexion);
                } //Fsi
                if ($_POST['file'] == 'candidat_synthese') { //---> Il s'agit de la suppression d'image
                    $ext = '';
                    candidat_synthese_update($_GET['candidat_id'], "", $ext);
                } //Fsi
                if ($_POST['file'] == 'candidat_lettre_motivation') { //---> Il s'agit de la suppression d'image
                    $ext = '';
                    candidat_lettre_motivation_update($_GET['candidat_id'], "", $ext);
                } //Fsi


                if ($_POST['file'] == 'candidat_video') { //---> Il s'agit de la suppression d'image
                    $ext = '';
                    candidat_video_update($_GET['candidat_id'], "", $ext);
                } //Fsi

                if ($_POST['file'] == 'candidat_cv') { //---> Il s'agit de la suppression d'image
                    $ext = '';
                    candidat_cv_update($_GET['candidat_id'], "", $ext);
                } //Fsi

                if ($_POST['file'] == 'candidat_attestation') { //---> Il s'agit de la suppression d'image
                    $ext = '';
                    candidat_attestation_update($_GET['candidat_id'], "", $ext);
                } //Fsi


                if ($_POST['file'] == 'candidat_diplome2') { //---> Il s'agit de la suppression d'image
                    $ext = '';
                    candidat_diplome2_update($_GET['candidat_id'], "", $ext);
                } //Fsi
                if ($_POST['file'] == 'candidat_certificat') { //---> Il s'agit de la suppression d'image
                    $ext = '';
                    candidat_certificat_update($_GET['candidat_id'], "", $ext);
                } //Fsi
            } //Fsi
            //---> Le formaulire de la page en cours a été envoyé
            if (isset($_POST['candidat_nom'])) {
                $candidat_ref               = "Ref" . $candidat_id;
                $candidat_nom               = lecture($_POST['candidat_nom']);
                $candidat_prenom            = $_POST['candidat_prenom'];
                $candidat_date_naissance    = $_POST['candidat_date_naissance'];
                $candidat_sex               = $_POST['candidat_sex'];
                $candidat_telephone         = $_POST['candidat_telephone'];
                $candidat_email             = $_POST['candidat_email'];
                $candidat_domicilite        = addslashes($_POST['candidat_domicilite']);
                $candidat_codepostale       = $_POST['candidat_codepostale'];
                $candidat_permis            = $_POST['candidat_permis'];
                if (isset($_POST['candidat_permisT']))
                    $candidat_permisT           = implode($_POST['candidat_permisT']);
                else
                    $candidat_permisT           = "";
                $candidat_mobilite          = $_POST['candidat_mobilite'];
                $candidat_handicap          = $_POST['candidat_handicap'];
                $candidat_date_dispo        = $_POST['candidat_date_dispo'];
                $candidat_profession        = $_POST['candidat_profession'];
                $candidat_emploi_rechercher = addslashes($_POST['candidat_emploi_rechercher']);
                //$candidat_login			= $_POST['candidat_login'];
                //$candidat_password			= $_POST['candidat_password'];
                $candidat_diplome           = $_POST['candidat_diplome'];
                $accompagne                 = $_POST['accompagne'];
                $lequel                     = ( isset($_POST['lequel'])) ? $_POST['lequel'] : "";
                $S_definit_comme            = $_POST['S_definit_comme'];
                $candidat_certification     = $_POST['candidat_certification'];
                $candidat_lettre_motivation = "";
                $candidat_synthese          = "";
                $candidat_video             = "";
                $candidat_cv                = "";
                $candidat_attestation       = "";
                $candidat_diplome2          = "";
                $candidat_certificat        = "";



///////////////
                $date = date('Y-m-d');

                $dossier = '../common/Fichiers/dossier_' . $candidat_ref;


                if (!is_dir($dossier)) {
                    mkdir($dossier);
                }
                if (isset($_GET['candidat_id'])) { //---> il s'agit d'une modification
                    $sql = "UPDATE candidat
	           SET  candidat_nom             = '$candidat_nom',
			candidat_prenom                 = '$candidat_prenom'        ,
			candidat_date_naissance                 = '$candidat_date_naissance'        ,
			candidat_sex                 = '$candidat_sex'        ,
                        candidat_genre                 = '$candidat_sex'        ,  
			accompagne                 = '$accompagne'        ,
			lequel                 = '$lequel'        ,
			S_definit_comme                 = '$S_definit_comme'        ,
			candidat_telephone             = '$candidat_telephone'        ,
			candidat_email                 = '$candidat_email'        ,
			candidat_domicilite                 = '$candidat_domicilite'        ,
                        date_mise_ajour = '$date',
			candidat_codepostale                 = '$candidat_codepostale'        ,
			candidat_permis             = '$candidat_permis'        ,
                         candidat_permisT             = '$candidat_permisT'        ,   
			candidat_mobilite                 = '$candidat_mobilite'        ,
 			candidat_handicap                 = '$candidat_handicap'        ,			 
			candidat_date_dispo                 = '$candidat_date_dispo'        ,
			candidat_profession                 = '$candidat_profession'        ,
			candidat_emploi_rechercher                 = '$candidat_emploi_rechercher'        ,
                        candidat_diplome                 = '$candidat_diplome'        ,
 
			candidat_certification                 = '$candidat_certification'
			          ";
                    $res = $connexion->query($sql);           //---> Exécuter la requête
                    //operation_message("Modification Terminée", TRUE);  //---> Msg + Fermer la fenêtre
                    $op  = "Modification";
                }
                else {
                    $admin_user_password = generatePassword();
                      $sqlU                = "INSERT INTO admin_user
                                     SET    admin_user_nom            = '$candidat_email'         ,
                                            admin_user_pouvoir        = 'candidat'     , 
					  admin_user_description    = 'candidat' ,
					  admin_user_date           = '" . $date . "'                ,					  
					  admin_user_actif          = 'Y'       ,					  
					  admin_user_password       = '" . md5($admin_user_password) . "'";

                    $resU          = $connexion->query($sqlU);
                    $admin_user_id = $connexion->lastInsertId();

                      $sql         = "insert into candidat
	           SET  candidat_nom             = '$candidat_nom',
			candidat_prenom                 = '$candidat_prenom'        ,
			candidat_date_naissance                 = '$candidat_date_naissance'        ,
                        candidat_operateur_id = '$operateur_id',
                        candidat_genre                 = '$candidat_sex'        ,
			candidat_sex                 = '$candidat_sex'        ,
			accompagne                 = '$accompagne'        ,
                        admin_user_id               =  '$admin_user_id ' ,
			lequel                 = '$lequel'        ,
			S_definit_comme                 = '$S_definit_comme'        ,
			candidat_telephone             = '$candidat_telephone'        ,
			candidat_email                 = '$candidat_email'        ,
			candidat_domicilite                 = '$candidat_domicilite'        ,
                        date_mise_ajour = '$date',
			candidat_codepostale                 = '$candidat_codepostale'        ,
			candidat_permis             = '$candidat_permis'        ,
                        candidat_permisT             = '$candidat_permisT'        ,   
			candidat_mobilite                 = '$candidat_mobilite'        ,
 			candidat_handicap                 = '$candidat_handicap'        ,			 
			candidat_date_dispo                 = '$candidat_date_dispo'        ,
			candidat_profession                 = '$candidat_profession'        ,
			candidat_emploi_rechercher                 = '$candidat_emploi_rechercher'        ,
 
                        candidat_diplome                 = '$candidat_diplome'        ,
			candidat_certification                 = '$candidat_certification'
			      ";
                    $res         = $connexion->query($sql);
                    $candidat_id = $connexion->lastInsertId();
                    $op          = "Inscription";
                   mail_pass($admin_user_password, $candidat_email, $candidat_nom . ' ' . $candidat_prenom);
                 }
                //---> Télécharger le Fichier
                $candidat_ref = "Ref" . $candidat_id;
                $dossier      = '../common/Fichiers/dossier_' . $candidat_ref;
                if (!is_dir($dossier)) {
                    mkdir($dossier);
                }
                $j = 0;

                $sql_ent = "delete from  experience WHERE candidat_id='" . $candidat_id . "'";
                $res_ent = $connexion->query($sql_ent);

                while (isset($_POST['entreprise_name_' . $j])) {

                    $entreprise_name        = $_POST['entreprise_name_' . $j];
                    $experience_titre       = $_POST['experience_poste_' . $j];
                    $experience_date_debut  = $_POST['experience_date_debut_' . $j];
                    $experience_date_fin    = $_POST['experience_date_fin_' . $j];
                    if (isset($_POST['experience_poste_actuel_' . $j]))
                        $experience_en_cour     = 1;
                    else
                        $experience_en_cour     = 0;
                    $experience_description = ""; //$_POST['experience_description_'.$j];
                    $experience_lieu        = $_POST['experience_ville_' . $j];
                    if ($experience_date_fin != "")
                        $sql_ent                = "INSERT INTO experience
				SET     entreprise        = '$entreprise_name'      ,
					   experience_titre        = '$experience_titre'        ,
					   experience_date_debut   = '$experience_date_debut'        ,
					   experience_date_fin    = '$experience_date_fin'        ,
					   experience_en_cour     = '$experience_en_cour'        ,
					   experience_description  = '$experience_description'  ,
                                           experience_position   = '0',    
					   experience_lieu                 = '$experience_lieu'  ,
					  candidat_id='" . $candidat_id . "'						";
                    else
                        $sql_ent                = "INSERT INTO experience
				SET     entreprise        = '$entreprise_name'      ,
					   experience_titre        = '$experience_titre'        ,
					   experience_date_debut   = '$experience_date_debut'        ,
					   
					   experience_en_cour     = '$experience_en_cour'        ,
					   experience_description  = '$experience_description'  ,
                                           experience_position   = '0',    
					   experience_lieu                 = '$experience_lieu'  ,
					  candidat_id='" . $candidat_id . "'						";

                    $res_ent = $connexion->query($sql_ent);
                    $j++;
                }

                if (isset($_FILES["candidat_photo"])) {

                    if ($_FILES["candidat_photo"]["size"] > 0 &&
                            $_FILES["candidat_photo"]["error"] == 0 &&
                            $_FILES["candidat_photo"]["name"] != "") {
                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_photo"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];

                        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                                $sql            = " SELECT candidat_photo FROM candidat WHERE candidat_id = $candidat_id ";
                                $res            = $connexion->query($sql);
                                $row            = $res->fetch();
                                $candidat_photo = $row['candidat_photo'];
                                @unlink("../common/Images/candidat/$candidat_photo");  //--> suppresssion de l'image pr&eacute;cedante
                            }
                            $filename = "candidat($candidat_id).$ext";

                            upload_file("candidat_photo", "../common/Images/candidat/$filename");
                            //candidat_photo_update($candidat_id, $filename, $ext,$connexion);
                            //---> Je mets à jour le nom de l'image
                            $sql = "UPDATE candidat
	           SET    candidat_photo   = '$filename'
			   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            ?>	   <script> alert('Format du Fichier incorrect !');</script> <?php
                        }
                    } //Fsi
                }

                ////////
                // $dossier = '../common/Fichiers/dossier_'.$candidat_ref;
                if (isset($_FILES["candidat_synthese"]))
                //---> Télécharger le candidat_diplome2 de l'candidat_synthese
                    if ($_FILES["candidat_synthese"]["size"] > 0 &&
                            $_FILES["candidat_synthese"]["error"] == 0 &&
                            $_FILES["candidat_synthese"]["name"] != "") {

                        if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                            $sql               = " SELECT candidat_synthese FROM candidat WHERE candidat_id = $candidat_id ";
                            $res               = $connexion->query($sql);
                            $row               = $res->fetch();
                            $candidat_synthese = $row['candidat_synthese'];
                            @unlink($dossier . "/$candidat_synthese");  //--> suppresssion de l'image pr&eacute;cedante
                        }

                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_synthese"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];
                        if ($ext == 'pdf' || $ext == 'PDF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            $filename = "candidat_synthese($candidat_id).$ext";
                            upload_file("candidat_synthese", $dossier . "/$filename");

                            //---> Je mets à jour le nom de l'image2
                            $sql = "UPDATE candidat
				   SET    candidat_synthese   = '$filename'
				   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            ?>	   <script> alert('Format du Fichier incorrect !');</script> <?php
                        }
                    } //Fsi






                    
//---> Télécharger le candidat_diplome2 de l'candidat_video
                if (isset($_FILES["candidat_video"]))
                    if ($_FILES["candidat_video"]["size"] > 0 &&
                            $_FILES["candidat_video"]["error"] == 0 &&
                            $_FILES["candidat_video"]["name"] != "") {

                        if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                            $sql            = " SELECT candidat_video FROM candidat WHERE candidat_id = $candidat_id ";
                            $res            = $connexion->query($sql);
                            $row            = $res->fetch();
                            $candidat_video = $row['candidat_video'];
                            @unlink($dossier . "/$candidat_video");  //--> suppresssion de l'image pr&eacute;cedante
                        }

                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_video"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];
                        if ($ext == 'pdf' || $ext == 'PDF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            $filename = "candidat_video($candidat_id).$ext";
                            upload_file("candidat_video", $dossier . "/$filename");

                            //---> Je mets à jour le nom de l'image2
                            $sql = "UPDATE candidat
				   SET    candidat_video   = '$filename'
				   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            ?>	   <script> alert('Format du Fichier incorrect !');</script> <?php
                        }
                    } //Fsi








                    
//---> Télécharger le candidat_diplome2 de l'candidat_cv

                if (isset($_FILES["candidat_cv"]))
                    if ($_FILES["candidat_cv"]["size"] > 0 &&
                            $_FILES["candidat_cv"]["error"] == 0 &&
                            $_FILES["candidat_cv"]["name"] != "") {

                        if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                            $sql         = " SELECT candidat_cv FROM candidat WHERE candidat_id = $candidat_id ";
                            $res         = $connexion->query($sql);
                            $row         = $res->fetch();
                            $candidat_cv = $row['candidat_cv'];
                            @unlink($dossier . "/$candidat_cv");  //--> suppresssion de l'image pr&eacute;cedante
                        }

                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_cv"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];
                        if ($ext == 'pdf' || $ext == 'PDF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            $filename = "candidat_cv($candidat_id).$ext";
                            upload_file("candidat_cv", $dossier . "/$filename");

                            //---> Je mets à jour le nom de l'image2
                            $sql = "UPDATE candidat
				   SET    candidat_cv   = '$filename'
				   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            ?>	   <script> alert('Format du Fichier incorrect !');</script> <?php
                        }
                    } //Fsi



                    
//---> Télécharger le candidat_diplome2 de l'candidat_lettre_motivation
                if (isset($_FILES["candidat_lettre_motivation"]))
                    if ($_FILES["candidat_lettre_motivation"]["size"] > 0 &&
                            $_FILES["candidat_lettre_motivation"]["error"] == 0 &&
                            $_FILES["candidat_lettre_motivation"]["name"] != "") {

                        if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                            $sql                        = " SELECT candidat_lettre_motivation FROM candidat WHERE candidat_id = $candidat_id ";
                            $res                        = $connexion->query($sql);
                            $row                        = $res->fetch();
                            $candidat_lettre_motivation = $row['candidat_lettre_motivation'];
                            @unlink($dossier . "/$candidat_lettre_motivation");  //--> suppresssion de l'image pr&eacute;cedante
                        }

                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_lettre_motivation"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];
                        if ($ext == 'pdf' || $ext == 'PDF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            $filename = "candidat_lettre_motivation($candidat_id).$ext";
                            upload_file("candidat_lettre_motivation", $dossier . "/$filename");

                            //---> Je mets à jour le nom de l'image2
                            $sql = "UPDATE candidat
	           SET    candidat_lettre_motivation   = '$filename'
			   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            ?>	   <script> alert('Format du Fichier incorrect !');</script> <?php
                        }
                    } //Fsi








                    
//---> Télécharger le candidat_diplome2 de l'candidat_attestation
                if (isset($_FILES["candidat_attestation"]))
                    if ($_FILES["candidat_attestation"]["size"] > 0 &&
                            $_FILES["candidat_attestation"]["error"] == 0 &&
                            $_FILES["candidat_attestation"]["name"] != "") {

                        if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                            $sql                  = " SELECT candidat_attestation FROM candidat WHERE candidat_id = $candidat_id ";
                            $res                  = $connexion->query($sql);
                            $row                  = $res->fetch();
                            $candidat_attestation = $row['candidat_attestation'];
                            @unlink($dossier . "/$candidat_attestation");  //--> suppresssion de l'image pr&eacute;cedante
                        }

                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_attestation"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];
                        if ($ext == 'pdf' || $ext == 'PDF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            $filename = "candidat_attestation($candidat_id).$ext";
                            upload_file("candidat_attestation", $dossier . "/$filename");

                            //---> Je mets à jour le nom de l'image2
                            $sql = "UPDATE candidat
				   SET    candidat_attestation   = '$filename'
				   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            ?>	   <script> alert('Format du Fichier incorrect !');</script> <?php
                        }
                    } //Fsi



                    
//---> Télécharger le candidat_diplome2 de l'candidat_diplome2
                if (isset($_FILES["candidat_diplome2"]))
                    if ($_FILES["candidat_diplome2"]["size"] > 0 &&
                            $_FILES["candidat_diplome2"]["error"] == 0 &&
                            $_FILES["candidat_diplome2"]["name"] != "") {

                        if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                            $sql               = " SELECT candidat_diplome2 FROM candidat WHERE candidat_id = $candidat_id ";
                            $res               = $connexion->query($sql);
                            $row               = $res->fetch();
                            $candidat_diplome2 = $row['candidat_diplome2'];
                            @unlink($dossier . "/$candidat_diplome2");  //--> suppresssion de l'image pr&eacute;cedante
                        }

                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_diplome2"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];
                        if ($ext == 'pdf' || $ext == 'PDF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            $filename = "candidat_diplome2($candidat_id).$ext";
                            upload_file("candidat_diplome2", $dossier . "/$filename");

                            //---> Je mets à jour le nom de l'image2
                            $sql = "UPDATE candidat
				   SET    candidat_diplome2   = '$filename'
				   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            ?>	   <script> alert('Format du Fichier incorrect !');</script> <?php
                        }
                    } //Fsi







                    
//---> Télécharger le candidat_diplome2 de l'candidat_certificat

                if (isset($_FILES["candidat_certificat"]))
                    if ($_FILES["candidat_certificat"]["size"] > 0 &&
                            $_FILES["candidat_certificat"]["error"] == 0 &&
                            $_FILES["candidat_certificat"]["name"] != "") {

                        if (isset($candidat_id)) {  //--> IL S'AGIT DE LA MODIFICATION DE L'IMAGE
                            $sql                 = " SELECT candidat_certificat FROM candidat WHERE candidat_id = $candidat_id ";
                            $res                 = $connexion->query($sql);
                            $row                 = $res->fetch();
                            $candidat_certificat = $row['candidat_certificat'];
                            @unlink($dossier . "/$candidat_certificat");  //--> suppresssion de l'image pr&eacute;cedante
                        }

                        //-->L'extension de l'image $ext
                        $filename = $_FILES["candidat_certificat"]["name"];
                        $filename = explode(".", $filename);
                        $ext      = $filename[count($filename) - 1];
                        if ($ext == 'pdf' || $ext == 'PDF' || $ext == 'doc' || $ext == 'docx' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF') {
                            $filename = "candidat_certificat($candidat_id).$ext";
                            upload_file("candidat_certificat", $dossier . "/$filename");

                            //---> Je mets à jour le nom de l'image2
                            $sql = "UPDATE candidat
	           SET    candidat_certificat   = '$filename'
			   WHERE  candidat_id      = $candidat_id";
                            $res = $connexion->query($sql); //---> Exécuter la requête
                        }
                        else {
                            operation_message("Format du Fichier incorrect !", true, '');
                        }
                    } //Fsi
                operation_message($op . " Terminée !", False, '', './?p=liste_candidats&' . $link . '#tabs-3');
                exit;
            } //Fsi
            //---> Champs du formulaire
            if (isset($_GET['candidat_id'])) { //---> il s'agit d'une modification
                $op                         = "Modifier";
                $sql                        = "SELECT *
	         FROM   candidat
			 WHERE  candidat_id = " . $candidat_id;
                $res                        = $connexion->query($sql); //---> Exécuter la requête
                $row                        = $res->fetch();
                $candidat_ref               = "Ref" . $candidat_id;
                if ($row == FALSE)
                    die("Impossible de trouver le candidat");
                $candidat_nom               = decode_text($row['candidat_nom']);
                $candidat_prenom            = $row['candidat_prenom'];
                $candidat_date_naissance    = $row['candidat_date_naissance'];
                $candidat_sex               = $row['candidat_sex'];
                $candidat_telephone         = $row['candidat_telephone'];
                $candidat_email             = $row['candidat_email'];
                $candidat_domicilite        = addslashes($row['candidat_domicilite']);
                $candidat_codepostale       = $row['candidat_codepostale'];
                $candidat_permis            = $row['candidat_permis'];
                $candidat_permisT           = $row['candidat_permisT'];
                $candidat_mobilite          = $row['candidat_mobilite'];
                $candidat_handicap          = $row['candidat_handicap'];
                $candidat_date_dispo        = $row['candidat_date_dispo'];
                $candidat_profession        = $row['candidat_profession'];
                $candidat_emploi_rechercher = $row['candidat_emploi_rechercher'];
                $candidat_diplome           = $row['candidat_diplome'];
                //$candidat_login = $row['candidat_login'];
                //$candidat_password = $row['candidat_password'];
                $accompagne                 = $row['accompagne'];
                $lequel                     = $row['lequel'];
                $S_definit_comme            = $row['S_definit_comme'];

                $candidat_certification = $row['candidat_certification'];
                $dossier                = '../common/Fichiers/dossier_' . $candidat_ref;
                if ($row['candidat_photo'] != 'img_vide.gif') {
                    $candidat_photo = "../common/Images/candidat/" . stripslashes($row['candidat_photo']);
                }
                else {
                    $candidat_photo = "../common/Images/img_vide.gif";
                }


                $candidat_lettre_motivation = $dossier . "/" . stripslashes($row['candidat_lettre_motivation']);
                $candidat_synthese          = $dossier . "/" . stripslashes($row['candidat_synthese']);
                $candidat_video             = stripslashes($row['candidat_video']);
                $candidat_cv                = $dossier . "/" . stripslashes($row['candidat_cv']);
                $candidat_attestation       = $dossier . "/" . stripslashes($row['candidat_attestation']);
                $candidat_diplome2          = $dossier . "/" . stripslashes($row['candidat_diplome2']);
                $candidat_certificat        = $dossier . "/" . stripslashes($row['candidat_certificat']);
            }
            else { //---> il s'agit d'une insertion
                $candidat_ref               = "Ref" . $candidat_id;
                $op                         = "Ajouter";
                $candidat_nom               = "";
                $candidat_prenom            = "";
                $candidat_date_naissance    = "";
                $candidat_sex               = "";
                $candidat_telephone         = "";
                $candidat_email             = "";
                $candidat_domicilite        = "";
                $candidat_codepostale       = "";
                $candidat_permis            = "";
                $candidat_permisT           = "";
                $candidat_mobilite          = "";
                $candidat_handicap          = "";
                $candidat_date_dispo        = "";
                $candidat_profession        = "";
                $candidat_emploi_rechercher = "";
                $candidat_diplome           = "";
                //$candidat_login = "";
                // $candidat_password = "";
                $accompagne                 = "NON";
                $lequel                     = "";
                $S_definit_comme            = "";

                $candidat_certification = "";
                $candidat_photo         = "../common/Images/img_vide.gif";
                $dossier                = '../common/Fichiers/dossier_' . $candidat_ref;


                $candidat_lettre_motivation = "";
                $candidat_synthese          = "";
                $candidat_video             = "";
                $candidat_cv                = "";
                $candidat_attestation       = "";
                $candidat_diplome2          = "";
                $candidat_certificat        = "";
                $candidat_id                = NULL;
            } //Fsi
            /*             * *******************************************************************************************************
              Gestion de la pagination
             * ******************************************************************************************************** */
//---> Cr�er un objet de pagination sans condition SQL sur la table
            $p   = new CAdminPagination($connexion, "candidat", "candidat_operateur_id='$operateur_id'", 10, "candidat_id");
            $p->writeJavaScript();    //---> Générer le code JavaScript correspondant
            $res = $p->makeButtons($action);    //---> Afficher les bouttons
            ?>
            <div class="bloc-titre">
                <h2>Liste de mes candidats</h2>
                <!--
                        LINK LISTE OPÉRATEURS
                         <span class="bleu"> | <a href="./?<?php echo $action ?>=operateurs&<?php echo $link ?>" >  operateur  </a> </span> -->
            </div>
            <section class="cv-content cv-ope clearfix">

                <script src="../js/jquery.js"></script>
                <link media="all" type="text/css" rel="stylesheet" href="../css/styleCand.css">
                <link media="all" type="text/css" rel="stylesheet" href="../css/bootstrap.css">

                <form  class=" " action="" method="post" name="form1" id="form1" enctype="multipart/form-data">

                    <div class=" ">
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                                    <li class="active"><a href="#step-1">
                                            <h4 class="list-group-item-heading"> </h4>
                                            <p class="list-group-item-text">Informations </p>
                                        </a></li>
                                    <li class="disabled"><a href="#step-2">
                                            <h4 class="list-group-item-heading"> </h4>
                                            <p class="list-group-item-text">Documents</p>
                                        </a></li>
                                    <li class="disabled"><a href="#step-3">
                                            <h4 class="list-group-item-heading"> </h4>
                                            <p class="list-group-item-text">Expériences</p>
                                        </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
<?php include("step1.php"); ?>
<?php include("step2.php"); ?>
<?php include("step3.php"); ?>
<?php //include("step4.php");      ?>

                    <p class=" ">
                        <input name="Acceptation" id="Acceptation" type="checkbox" class='zone' style="width:50px; float left;" >        Acceptation des conditions générales d'utilisation des données? |<span class="obligatory">*</span> 

                    </p>


                    <input type="button" name="Submit" value="<?php echo $op ?>" onclick="verif();"  style="width:95px; cursor:pointer ">

                </form>

                <script type="text/javascript">
                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-34731274-1']);
                    _gaq.push(['_trackPageview']);
                    _gaq.push(['_trackEvent', 'sharing', 'viewed full-screen', 'snippet W7gNz', 0, true]);
                    (function () {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>
                <script type="text/javascript">
                    (function ($) {
                        $('#theme_chooser').change(function () {
                            whichCSS = $(this).val();
                            document.getElementById('snippet-preview').contentWindow.changeCSS(whichCSS);
                        });
                    })(jQuery);


                    // Activate Next Step

                    $(document).ready(function () {

                        var navListItems = $('ul.setup-panel li a'),
                                allWells = $('.setup-content');

                        allWells.hide();

                        navListItems.click(function (e)
                        {
                            e.preventDefault();
                            var $target = $($(this).attr('href')),
                                    $item = $(this).closest('li');

                            if (!$item.hasClass('disabled')) {
                                navListItems.closest('li').removeClass('active');
                                $item.addClass('active');
                                allWells.hide();
                                $target.show();
                            }
                        });

                        $('ul.setup-panel li.active a').trigger('click');

                        // DEMO ONLY //
                        $('#activate-step-2').on('click', function (e) {
                            $('ul.setup-panel li:eq(1)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-2"]').trigger('click');
                            $(this).remove();
                        })

                        $('#activate-step-3').on('click', function (e) {
                            $('ul.setup-panel li:eq(2)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-3"]').trigger('click');
                            $(this).remove();
                        })

                        $('#activate-step-4').on('click', function (e) {
                            $('ul.setup-panel li:eq(3)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-4"]').trigger('click');
                            $(this).remove();
                        })

                        $('#activate-step-3').on('click', function (e) {
                            $('ul.setup-panel li:eq(2)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-3"]').trigger('click');
                            $(this).remove();
                        })
                    });




                </script>

            </section>

            <!-- !-->
        </div>
    </div>

</div>
</div>
</section>
