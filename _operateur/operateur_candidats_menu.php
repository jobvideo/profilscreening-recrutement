<?php include('include/header-profil-operateur.php');?>

		<div id="cv-tabs" class="cv-tabs">
<div class="cv-tabs-inner clearfix">
			<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

			<ul id="tabs" class="tabs clearfix">
				<li><a class="tab1 tab-profil" id="#tabs-1"id="firstonglet" href="./?<?php  echo $action?>=operateur_profil&<?php  echo $link?>#tabs-1" >Mon Profil</a></li>
				<li><a class="tab2 tab-candidat activate"  id="#tabs-2" href="./?<?php  echo $action?>=liste_candidats&<?php  echo $link?>#tabs-2">Candidats</a></li>
				 
 
			</ul>

		</div>


			<div id="content-tab" class="content-tab">

				<div id="tabs-"<?php echo $tab_num?> class="clearfix tab consult">

 <!-- !-->

<?php
//---> Rubrique valide ?
$rubrique_id = getRubriqueId($connexion, "operateurs");

//---> Tester la session et importer les variables : $select, $mod, $insert, $delete
//     relatives aux privil�ges de l'utilisateur et de la rubrique en cours
include "../include/session_test.php";

include "../include/operateur_candidats.php";     //---> Les fonctions du module candidat
//---> Utiliser le module pagination
$operateur_id = $_SESSION['operateur_id'];

	//---> Proc�dure de suppression
	if (isset($_GET['supprimer'])  && $delete == 'Y') {
		candidat_supprimer($connexion,$_GET['supprimer']);
	} //Fsi

	if (isset($_GET['visibilite'])  && $delete == 'Y') {
		 candidat_visible($connexion,$_GET['visibilite']);
	} //Fsi

/*
if (isset($_POST['id']) && $mod == 'Y') {
    //---> Proc�dure de modification "visible"
    if (isset($_POST['visible']))
        candidat_visible($_POST['visible'], $_POST['id']);
    else
        candidat_visible(NULL, $_POST['id']);  // Tous � faux
*/


//---> Proc�dure de modification "une"
//} //Fsi

/* * *******************************************************************************************************
  Gestion de la pagination
 * ******************************************************************************************************** */
//---> Cr�er un objet de pagination sans condition SQL sur la table
$p = new CAdminPagination($connexion, "candidat,metier", "candidat_operateur_id='$operateur_id' and ((candidat.metier_id = metier.metier_id)or (candidat_emploi_rechercher = metier_nom)) and accompagne = 'OUI'", 500, "candidat_id");
$p->writeJavaScript();    //---> Générer le code JavaScript correspondant
  $res = $p->makeButtons($action);    //---> Afficher les bouttons

?>
<script type="text/javascript" language="javascript">

    function verif()
    {
        var msg = "Voulez reellement appliquer les changements demandes (modification + suppression) ?";
        if (confirm(msg))
            document.pagination_tab.submit();
    } //Fin appliquer

    function ajouter()
    {
		document.location='./?<?php  echo $action?>=operateur_candidats_add&<?php echo $link ?>&operateur_id=<?php echo $operateur_id ?>&admin_user_id=<?php echo $admin_user_id ?>';
        //popup('operateur_candidats_add.php?<?php echo $link ?>&operateur_id=<?php echo $operateur_id ?>&admin_user_id=<?php echo $admin_user_id ?>', 625, 750, true);
    } //Fin ajouter

</script>

<div class="bloc-titre">
<h2>Liste de mes candidats</h2>
<!--
	LINK LISTE OPÉRATEURS
	 <span class="bleu"> | <a href="./?<?php echo $action ?>=operateurs&<?php echo $link ?>" >  operateur  </a> </span> -->
</div>
<section class="cv-content cv-ope clearfix">
	<form name="pagination_tab" method="post" action="">

	 <?php
        $i = 0;
        $n = 0;
        $nbre = $res->rowCount();

        while ($row = $res->fetch()) {
            $i++;
            $n++;
            $disabled = ($mod != 'Y') ? "disabled" : "";
            $color = ($i % 2 != 0) ? "#EFEFEF" : "#E9E9E9";
            $session = $_GET["session"];
            $candidat_id = $row['candidat_id'];
            $candidat_nom = affichage($row['candidat_nom'], "---");
  $metier_nom = affichage($row['metier_nom'], "---");
            if ($row['candidat_photo'] != 'img_vide.gif') {
                $candidat_photo = "../common/Images/candidat/" . stripslashes($row['candidat_photo']);
            } else {
                $candidat_photo = "../common/Images/candidat/img_vide.gif";
            }

            $candidat_visible = ($row['candidat_visible'] == 'Y') ? "CHECKED" : "";
            //-->les accesssoires de chaque candidat
            $sql2 = " SELECT DISTINCT experience_id FROM experience WHERE candidat_id = $candidat_id ";
            $res2 = $connexion->query($sql2);
			if($res2)
            $nb_exp = $res2->rowCount();
			else $nb_exp = 0;

            $sql3 = " SELECT DISTINCT question_id FROM question,candidat WHERE partie_synthese_id = 4 and metier=candidat.metier_id and  candidat_id = $candidat_id";
            $res3 = $connexion->query($sql3);
			if($res3)
            $nb_comp = $res3->rowCount();
			else $nb_comp = 0;
            $sql4 = " SELECT DISTINCT partie_synthese_id 	 FROM question_type   ";
            $res4 = $connexion->query($sql4);
			if($res4)
            $nb_synt = $res4->rowCount();
			else $nb_synt = 0;
		?>
		<article class="cv-content-inner cv-content-inner-ope">
			<div class="cv-file clearfix">
				<div class="cv-contener cv-contener-ope clearfix">
					<figure id="portrait" class="portrait">
						<img src="<?php echo $candidat_photo ; ?>" alt="portrait-baptiste"/>
					</figure>
					<div class="cv-text">
						<div class="cv-col">
							<h2><strong><?php echo $candidat_nom ; ?>(<?php echo $metier_nom ; ?>)</strong></h2>
						</div>
						<div class="cv-col">
							<ul class="op-candidat-exp">
								<li>Expériences<strong><a href="./?<?php echo $action ?>=experience_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>" ><?php echo $nb_exp; ?></a></strong></li>
								<li>Compétences<strong><a href="./?<?php echo $action ?>=competence_candidat&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>" ><?php echo $nb_comp; ?></a></strong></li>
								<li>Synthèse<strong><a href="./?<?php echo $action ?>=synthese_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>" > <?php echo $nb_synt; ?></a></strong></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="btnOpe clearfix">
					<div class="btnOpeContent btnOpeView">
					<a href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>&visibilite=<?php echo $candidat_id ?>" >
<!--
						<input type="checkbox" id="visibilite" value="visibilite" name="visibilite">
						<label class="view-on" for="visibilite"><span></span><p>
-->
						visibilite</a><!-- </p></label> -->
					</div>
					<div class="btnOpeContent btnOpeDelete">
					<a href="./?<?php echo $action ?>=liste_candidats&<?php echo $link ?>&candidat_id=<?php echo $candidat_id ?>&operateur_id=<?php echo $operateur_id ?>&supprimer=<?php echo $candidat_id ?>" >
<!--
						<input type="checkbox" id="supprimer" value="supprimer" name="supprimer">
						<label class="delete-candidat" for="supprimer"><span></span><p>
-->
						supprimer</a><!-- </p></label> -->
					</div>
					<div class="btnOpeContent btnOpeModif">
						<a href="javascript:document.location='./?<?php  echo $action?>=operateur_candidats_add&<?php echo $link ?>&admin_user_id=<?php echo $admin_user_id ?>&candidat_id=<?php echo $row['candidat_id'] ?>&operateur_id=<?php echo $operateur_id ?>';">Modifier les données candidat</a>
					</div>
					<div class="btnOpeContent btnOpeVoir">
						<a class="button voir-profil" title="Cliquer pour voir le profil" href="./?p=voir&cnd=<?php echo $candidat_id;  ?>&admin_user_id=<?php echo $admin_user_id ?>&operateur_id=<?php echo $operateur_id ?>&session=<?php echo $idses?>" target="_blank">voir le profil</a>
					</div>
				</div>
		</div>

		</article>
		<?php
		}
		?>
		<input type="button" name="wp-submit" id="wp-submit" class="wp-submit button-primary" value="Ajouter un candidat" onClick="javascript:  ajouter();">
	</form>
</section>

 <!-- !-->
				</div>
			</div>

		</div>
	</div>
</section>
