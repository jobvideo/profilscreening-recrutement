 <?php
    
	 $sql = " SELECT * FROM question WHERE  partie_synthese_id = $partie_synthese_id ";
	$res = $connexion->query($sql);
   while($row=@$res->fetch())
	 {
	    $question_id     = $row['question_id'];
		$question_nom     = affichage($row['question_nom'],"---");
		$Moyen_apptitude_Q = 0;
		$Moyen_apptitude_P = 0; 
		$nb=0;
			 $sql2 = " SELECT * FROM reponse_question WHERE  question_id = $question_id ";
			$res2 = $connexion->query($sql2);

		  if(isset($_POST['quest_'.$question_id.''])){
		  while($row2=@$res2->fetch())
		{
		   $nb++;
		// pour chaque quest il y a plusieurs réponse , et une valeur pour chacune
	        $reponse_question_id     = $row2['reponse_question_id'];
			if(isset( $_POST['rep_'.$reponse_question_id.'']))
			{
				$Quotidienne     = $_POST['Quotidienne_'.$reponse_question_id.''];
				$Professionnelle = $_POST['Professionnelle_'.$reponse_question_id.''];
				
				$Moyen_apptitude_Q = $Moyen_apptitude_Q + $Quotidienne ;
				$Moyen_apptitude_P = $Moyen_apptitude_P + $Professionnelle ;
				
				$sql_existe = "select * from reponse_candidat where question_id  = '$question_id'  and
								reponse_id  = '$reponse_question_id' and candidat_id = '$candidat_id'";
				$res_existe = $connexion->query($sql_existe);
				$nbre=@$res_existe->rowCount();
			
		  if($nbre == 0)
			 $sql_rep = "INSERT INTO reponse_candidat
	           SET      
					    question_id             = '$question_id'        ,
					    Valeur_Quotidienne      = '$Quotidienne'        ,
						Valeur_Professionnelle  = '$Professionnelle'        , 
					    reponse_id              = '$reponse_question_id' ,
                        candidat_id             = '$candidat_id'					   ";
		 else 
		     $sql_rep = "update reponse_candidat
	           SET      					   
					     Valeur_Quotidienne = '$Quotidienne'        ,
						 Valeur_Professionnelle = '$Professionnelle'     
						  
						 where question_id             = '$question_id'    and 		
						 reponse_id           = '$reponse_question_id'  and 
						 candidat_id = '$candidat_id' ";
					 
				$res_rep = $connexion->query($sql_rep);					    
				 
		  }
		 }//reponse_candidat
		 if($nb>0){
		  $Moyen_apptitude_Q       = round($Moyen_apptitude_Q/$nb ,2);        
		 $Moyen_apptitude_P       = round($Moyen_apptitude_P/$nb,2);
						 
		 $sql_rep = "update reponse_candidat
	           SET      					   
					      Moyen_apptitude_Q       = '$Moyen_apptitude_Q'        ,
						 Moyen_apptitude_P       = '$Moyen_apptitude_P'
						 
						 where question_id             = '$question_id'    and 		
 						 candidat_id = '$candidat_id' ";
					 
				$res_rep = $connexion->query($sql_rep);					    
			
		 }
	}
					
	
	}

  /*********************************************************************************************************
                                            Gestion de la pagination
  **********************************************************************************************************/
  //---> Créer un objet de pagination sans condition SQL sur la table
  $p = new CAdminPagination($connexion,"question","partie_synthese_id = '$partie_synthese_id'", 118, "question_position");
  $p->writeJavaScript();    //---> Générer le code JavaScript correspondant
?>

 
 
	    <form name="pagination_tab" id="pagination_tab"  method="post" action="#">

				<fieldset>
				 <legend> Questionnaire d’aptitudes : </legend><br>
				   <p>Ce questionnaire est une auto-évaluation. Il a pour objectif de vous faire réfléchir aux aptitudes que vous possédez. Une aptitude est la capacité à réaliser une action. Au sein d’un travail, plusieurs peuvent ainsi être nécessaires. 
<br>
Ce questionnaire peut être rempli en 30 minutes environ. Il n’est cependant pas limité dans le temps. 
<br>
Vous allez donc devoir, dans un premier temps, évaluer la présence ou non de 69 aptitudes. Celles-ci ont été répertoriées de manière à noter l’ensemble des aptitudes présentes dans tous les métiers. De ce fait, certaines vont vous paraître éloignées de votre projet professionnel. Il vous faudra tout de même répondre. 
<br>
Vous devrez utiliser une échelle de valeur variant de 1 à 4, sachant que :
				<br><br>
					<dl class="faq">
						  <dt>  = je ne possède pas cette aptitude ; </dt>
						  <dt>  = je possède peu cette aptitude ; </dt>
						  <dt>  = je possède cette aptitude et l’utilise parfois; </dt>
						  <dt>  = je possède cette aptitude et l’utilise souvent .  </dt>
					</dl><br>
				Evaluez chaque aptitude mise en pratique lors de vos expériences personnelles et professionnelles. Notez la fréquence de son utilisation. 
				<br>
				Décrivez quelques situations où certaines de vos aptitudes ont été particulièrement utilisées dans le domaine personnel ou professionnel. Précisez s’il s’agit d’une mission de travail ou d’un loisir. Précisez cette démarche. 
				<br>
				Pour l’ensemble de ce questionnaire, soyez le plus précis possible. Ces données compléteront votre profil, il importe qu’elles vous correspondent au mieux. Il n’y a pas de bonnes ou mauvaises réponses. Répondez le plus honnêtement possible pour ne pas fausser votre profil<br>
			<br>	 

			<p>
			<!-- Début de l'enête de pagination -->
					<?php
					  $res = $p->makeButtons($action);    //---> Afficher les bouttons
					?>
					<!-- Fin de l'enête de pagination -->
			</p>
			 
			  <table border="1" width="90%">
			  <tr>
				  <td></td>  
				   <td width="10">Q</td>
				  <td width="10">P</td>
				  <td  > </td>			  
				  <td width="30">Quotidienne</td>
				  <td width="30">Professionnelle</td>
				   
			  </tr>
			 <?php
			  
							 $nbre=@$res->rowCount();
						   
						  
			 while($row=@$res->fetch())
						  {
							 
							$session             = $_GET["session"];
							 $question_id     = $row['question_id'];
							$question_nom     = affichage($row['question_nom'],"---");
							 $question_visible   = ($row['question_visible']=='Y')? "CHECKED" : "";
			   
							//-->les catégories de chaque question
							
							
			
						$sql2 = " SELECT * FROM reponse_question WHERE  question_id = $question_id ";
						$res2 = $connexion->query($sql2);
						?>
						  <?php
						  $Moyen_apptitude_Q = '';
						  $Moyen_apptitude_P = '';
						  $nb=0; 
							  while($row2=@$res2->fetch())
							  {
								  $nb++;
								$sql_existe = "select * from reponse_candidat where question_id  = '$question_id'  
								and reponse_id  = '".$row2['reponse_question_id']."' and candidat_id = '$candidat_id'";
									$res_existe = $connexion->query($sql_existe);
									$nbre=@$res_existe->rowCount();
									if($nbre == 0){$q=""; $p="";}
									else{
										$row_existe=@$res_existe->fetch();
										$q= $row_existe['Valeur_Quotidienne'];
										$p= $row_existe['Valeur_Professionnelle'];
										 
										if($nb==1){
										$Valeur_Quotidienne     = $row_existe['Moyen_apptitude_Q'];
										$Valeur_Professionnelle     = $row_existe['Moyen_apptitude_P'];
										}else{
										$Valeur_Quotidienne     = "";
										$Valeur_Professionnelle     = "";
										}
										 
									}
							  ?>
							  <tr>
									<td><b><?php  echo utf8_encode($question_nom); ?></b></td>
									<td width="10"><?php echo $Valeur_Quotidienne; ?> </td>
									<td width="10"><?php echo $Valeur_Professionnelle; ?></td>	
									 <td  > <?php   echo  affichage(utf8_encode($row2['reponse_question_nom']),"---"); ?></td>
									<input type="hidden" name="quest_<?php echo $question_id; ?>">	
									<input type="hidden" name="rep_<?php echo $row2['reponse_question_id']; ?>">									
									<td width="30"><input type="text" name="Quotidienne_<?php echo $row2['reponse_question_id']; ?>" 
									value="<?php echo $q; ?>"></td>
									<td width="30"> <input type="text" name="Professionnelle_<?php echo $row2['reponse_question_id']; ?>" 
									value="<?php echo $p; ?>"></td>
 							 </tr>
								<?php
								$question_nom="";
							   $Valeur_Quotidienne="";
							   $Valeur_Professionnelle="";
							   }
							   ?>   
					   
				    
					<?php
						   }
						   ?> 
				</table> 
			 
			</fieldset>	 

	 <p> 
 
	 <input type="submit" name= "Envoyer" value="Enregistrer" onclick="document.pagination_tab.submit();" />
	 <input type="reset" value="Annuler" onclick="document.pagination_tab.reset();" />
	 </p>

	</form>
  