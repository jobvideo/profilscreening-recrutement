<!-- Magnific Popup core CSS file -->
<!-- Magnific Popup core CSS file -->
<link rel="stylesheet" href="../popup/bootstrap.min.css">
<script src="../popup/jquery.min.js"></script>
<script src="../popup/bootstrap.min.js"></script>

<section class="cv-edit clearfix">
    <h3>Éffectuer l'édition de votre profil :</h3>

    <form   action="" method="post" name="user_modif"  id='user_modif' enctype="multipart/form-data">
        <div class="col-tab">

            <p class="titleInterest">Raison Social</p>
            <section class="filter clearfix">
                <input type="text" class="reference" for="name"  placeholder="Nom de l'operateur" name="operateur_raison_social"
                       id="operateur_raison_social"  value="<?php echo $nom; ?>"/>

            </section>
            <p class="titleInterest">Votre principal domaine de compétence</p>
            <section class="filter clearfix">
                <input type="text" class="reference" for="competence"  placeholder="Votre compétence principale" 
                       name="operateur_domaine_activite" id="operateur_domaine_activite"  value="<?php echo $operateur_domaine_activite; ?>"/>

            </section>

            <section class="filter clearfix">
                <p class="titleInterest">Votre numéro de téléphone</p>
                <input type="text" class="reference" for="competence"  placeholder="Votre téléphone" name="operateur_tel" id="operateur_tel" 
                       value="<?php echo $operateur_tel; ?>"/>

            </section>
            <section class="filter clearfix">form
                <p class="titleInterest">Votre numéro de siret</p>
                <input type="text" class="reference" for="competence"  placeholder="Votre siret" name="operateur_num_siret" id="operateur_num_siret" 
                       value="<?php echo $sirete; ?>"/>

            </section>
            <section class="filter clearfix">
                <p class="titleInterest">Votre email</p>
                <input type="text" class="reference" for="mail"  placeholder="Votre  adresse mail " name="operateur_email" id="operateur_email" 
                       value="<?php echo $operateur_email; ?>"/>

            </section>
            <section class="filter clearfix">
                <p class="titleInterest">Votre site web </p>
                <input type="text" class="reference" for="competence"  placeholder="Votre  site web " name="operateur_site" id="operateur_site" 
                       value="<?php echo $operateur_site; ?>"/>

            </section>
        </div>
        <div class="col-tab">
            <section class="filter clearfix">
                <p class="titleInterest">Votre logo</p>

                <?php
                if ($operateur_photo != "") {
                    ?>
                    <img src="../common/Images/operateur/<?php echo $operateur_photo ?>" width="80"    style="padding-left:5px; float: right;" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" />
                    <?php
                }
                else {
                    ?>
                    <img src="../common/Images/vide.gif" width="80"    style="padding-left:5px; float: right;" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" />
                    <?php
                }
                ?>

                <input type="file" class="reference_image" for="competence"  placeholder="Télécharger votre logo" name="operateur_photo" id="operateur_photo" />

            </section>
            <section class="filter clearfix">
                <p class="titleInterest">Votre vidéo</p>
                <input type="text" class="reference" for="competence"  placeholder="Url de votre vidéo" name="operateur_video_url" id="operateur_video_url"  
                       value="<?php echo $operateur_video_url; ?>"/>

            </section>
            <section class="filter clearfix skill">
                <p class="titleInterest">Votre Description</p>
                <textarea class="reference_textarea" for="competence"  placeholder="operateur description" name="operateur_description" 
                          id="operateur_description" rows="52"><?php echo $operateur_description; ?></textarea>
            </section>
            <section class="filter clearfix skill">
                <p class="titleInterest">Votre Adresse</p>
                <input type="text" class="reference" for="competence"  placeholder="operateur_adresse" name="operateur_adresse" id="operateur_adresse"  
                       value="<?php echo $operateur_adresse; ?>" />

                <input type="text" class="reference_code_postale" for="competence"  placeholder="operateur_code_postale" name="operateur_code_postale" 
                       id="operateur_code_postale"  value="<?php echo $operateur_code_postale; ?>" />

                <input type="text" class="reference_code_vile" for="competence"  placeholder="operateur_vile" name="operateur_vile" id="operateur_vile" 
                       value="<?php echo $operateur_vile; ?>" />

            </section>

           			 

        </div>

        <input type="submit" class="submit valid valid-profil" for="enregistrer" value="enregistrer"/>

    </form>


</section>

