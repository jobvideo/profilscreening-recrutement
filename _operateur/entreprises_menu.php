<?php include('include/header-profil-operateur.php');?>

		<div id="cv-tabs" class="cv-tabs">

			<div class="cv-tabs-inner clearfix">
				<a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>

				<ul id="tabs" class="tabs clearfix">
					<li><a class="tab1" id="#tabs-1"id="firstonglet" href="./?<?php  echo $action?>=operateur_profil&<?php  echo $link?>#tabs-1" >
							Mon Profil </a></li>
					<li><a class="tab2"  id="#tabs-2" href="./?<?php  echo $action?>=liste_candidats&<?php  echo $link?>#tabs-2" >
							Candidats   </a></li>
					<li><a class="tab3 activate" id="#tabs-3"  href="./?<?php  echo $action?>=quest&<?php  echo $link?>#tabs-3"  >
							Synthèse  </a></li>
					<li><a class="tab4" id="#tabs-4"  href="./?<?php  echo $action?>=competence_metier&&<?php  echo $link?>#tabs-4">Compétences</a></li>

<!--
					<li><a class="tab10"  id="#tabs-10" href="javascript:popup('changer_pw.php?admin_user_id=<?php  echo $admin_user_id;?>&<?php  echo $link?>#tabs-10',475,250)" class="menutext">
							Password		</a></li>
-->
				</ul>

			</div>

			<div id="content-tab" class="content-tab">

				<div id="tabs-"<?php echo $tab_num?> class="clearfix tab consult">

<!-- --!-->

<?php

   //---> Rubrique valide ?
   $rubrique_id = getRubriqueId($connexion,"entreprises");

   //---> Tester la session et importer les variables : $select, $mod, $insert, $delete
   //     relatives aux privilèges de l'utilisateur et de la rubrique en cours
   include "../include/session_test.php";

   include "../include/entreprises.php";     //---> Les fonctions du module entreprise
          //---> Utiliser le module pagination


   //---> Procédure de suppression
   if (isset($_POST['supprimer']) && count($_POST['supprimer']) > 0 && $delete=='Y')
   {
	 entreprise_supprimer($_POST['supprimer']);
   } //Fsi

   if (isset($_POST['id']) && $mod=='Y')
   {
     //---> Procédure de modification "visible"
     if (isset($_POST['visible']))
	   entreprise_visible($_POST['visible'],$_POST['id']);
	 else
	   entreprise_visible(NULL,$_POST['id']);  // Tous à faux

     //---> Procédure de modification "une"
     if (isset($_POST['une']))
	   entreprise_une($_POST['une'],$_POST['id']);
	 else
	   entreprise_une(NULL,$_POST['id']);      // Tous à faux

	        //---> Procédure de modification "une"

   } //Fsi
   if (isset($_POST['dsens']) && $mod=='Y')
		{
		 if ($_POST['dsens']=="bas")
		   entreprise_deplacer($_POST['d_id'],"bas");
		 else
		   entreprise_deplacer($_POST['d_id'],"haut");
		}

  /*********************************************************************************************************
                                            Gestion de la pagination
  **********************************************************************************************************/
  //---> Créer un objet de pagination sans condition SQL sur la table
  $p = new CAdminPagination($connexion,"entreprise","", 6, "entreprise_position");
  $p->writeJavaScript();    //---> Générer le code JavaScript correspondant
?>
<script type="text/javascript" language="javascript">
<!--
  function verif()
  {
    var msg = "Voulez reellement appliquer les changements demandes (modification + suppression) ?"
	if (confirm(msg))
	  document.pagination_tab.submit();
  } //Fin appliquer

  function ajouter()
  {
    popup('entreprises_add.php?<?php  echo $link?>&admin_user_id=<?php  echo $admin_user_id?>',525,700,true);
  } //Fin ajouter

    function deplacer(id, sens)
  {
    document.ordre.d_id.value    = id;
    document.ordre.dsens.value = sens;
	document.ordre.submit();
  } //Fin deplacer

-->
</script>

<table width="99%" border="0" align="right" cellpadding="0" cellspacing="0">
<form name="ordre" method="post" action="">
		   <input type="hidden" name="dsens"   value="">
           <input type="hidden" name="d_id"    value="">
		</form>
	<tr valign="top">
  <td colspan="2" height="20"></td>
</tr>
  <tr valign="top">
  <td width="15" height="25"></td>
  <td>
    <span class="titre">Liste des Entreprise</span>
  </td>
</tr>
<tr>
  <td colspan="2" height="2" bgcolor="#FF0000"></td>
</tr>
<tr valign="top">
  <td colspan="2" height="20"></td>
</tr>
<tr valign="top">
  <td height="25" colspan="3">
    <!-- Début du tableau avec un système de pagination -->
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
	  <td colspan="2">
        <!-- Début de l'enête de pagination -->
		<?php
		  $res = $p->makeButtons($action);    //---> Afficher les bouttons
		?>
		<!-- Fin de l'enête de pagination -->	  </td>
	</tr>
	<tr>
	  <td height="5"></td>
    </tr>
	<tr>
	  <td align="left">
	    <?php
		  if($p->getTotal()!=0 && $select=='Y') //---> Autorisations suffisantes ?
          {
		?>
	    <form name="pagination_tab" method="post" action="">
		<!-- Début du tableau de contenu -->
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr>
            <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF" >
                <tr bgcolor="#B6B6B6">
                  <td width="67" align="center" > N&deg; </td>
                  <td width="621" bgcolor="#B6B6B6">  Les titres des entreprises

                   </td>

                  <th width="100" align="center" scope="col">  Position

                    </th>
                  <td  width="101"align="center"  > Photo  </td>


                  <td width="93" align="center" >  Visible

                    </td>
                  <?php
	             if($delete=='Y')
	             {
	          ?>
                  <td width="89" bgcolor="#B6B6B6" scope="col" >Supp</td>
                  <?php
			     }//Fsi
			  ?>
                </tr>
                <?php
	          $i = 0;$n=0;
			  			   $nbre=@$res->rowCount();

	          while($row=@$res->fetch())
	          {
			    $i++;$n++;
			    $disabled            = ($mod!='Y')? "disabled" : "";
				$color               = ($i%2!=0)? "#EFEFEF" : "#E9E9E9";
				$session             = $_GET["session"];
				 $entreprise_id     = $row['entreprise_id'];
			    $entreprise_raison_social     = affichage($row['entreprise_raison_social'],"---");
				if(($row['entreprise_photo']!= 'img_vide.gif')&&(($row['entreprise_photo']!= '')))
				{
				$entreprise_photo     = "../common/Images/entreprise/".stripslashes($row['entreprise_photo']);
				}else{
				$entreprise_photo    = "../common/Images/entreprise/img_vide.gif";
				}

				$entreprise_une       = ($row['entreprise_une']=='Y')? "CHECKED" : "";
				$entreprise_visible   = ($row['entreprise_visible']=='Y')? "CHECKED" : "";
				//-->les accesssoires de chaque entreprise




			?>
              <tr bgcolor="#E0DFE3" onMouseOver="this.style.background='#FCFCFC'" onMouseOut="this.style.background='#E0DFE3'">
			  <td height="70" align="center"  class="texte" ><?php  echo $p->courent*$p->page+$i?>
                <input type="hidden" name="id[]" value="<?php  echo $row['entreprise_id']?>" /></td>
            <td align="left"><?php
				if($mod=='Y')
		        {
		      ?>
                <a href="javascript:popup('entreprises_add.php?<?php  echo $link?>&admin_user_id=<?php  echo $admin_user_id?>&entreprise_id=<?php  echo $row['entreprise_id']?>',525,700,true);" class="menutext">
                <?php  echo $entreprise_raison_social?>
                </a>
                <?php
				} else
				{
				  echo $entreprise_raison_social;
				} //Fsi
              ?>            </td>

                <td align="center"><?php
			  if ($n!=1)
			  {
				if($mod=='Y')
				{
					?>
                    <a href="javascript: deplacer(<?php  echo $row['entreprise_position']?>,'haut')" title="Vers le haut"> <img src="images/fl_haut.gif" alt="Vers le haut" border="0"> </a><br>
                    <?php
				}
				else
				{
					?>
                    <img src="images/fl_haut.gif" alt="Vers le haut" border="0"><br>
                    <?php
				}//Fsi
			   }
			   if ($n!=$nbre)
			   {
			   	if($mod=='Y')
				{
					?>
                    <a href="javascript: deplacer(<?php  echo $row['entreprise_position']?>,'bas')" title="Vers le bas"> <img src="images/fl_bas.gif" alt="Vers le bas" border="0"> </a><br>
                    <?php
				}
				else
				{
					?>
                    <img src="images/fl_bas.gif" alt="Vers le bas" border="0"><br>
                    <?php
				}//Fsi
   			  }
			  ?></td>
                <td width="101" align="center" valign="middle" >
				<img src="<?php  echo $entreprise_photo?>" alt="Photo" width="101" height="70" border="0" align="absmiddle" />				</td>

                <td align="center" ><input type="Checkbox" name="visible[]" value="<?php  echo $row['entreprise_id']?>" style="color:#666666;" <?php  echo $entreprise_visible?> <?php  echo $disabled?> />            </td>
                <?php
			    if($delete=='Y')
	            {
	          ?>
            <td align="center" ><input type="Checkbox" name="supprimer[]" id="supprimer<?php  echo $i?>" value="<?php  echo $row['entreprise_id']?>"  onclick="javascript: restore_row(this.parentNode.parentNode, '<?php  echo $color?>');" />            </td>
                <?php
			    } //Fsi
			  ?>
              </tr>
              <?php
			  } //FTQ
			?>
            </table></td>
          </tr>
        </table>
		<a href="../?<?php  echo $action?>=entreprise" class="menutext" target="_blank">
          Aper&ccedil;u        </a>
		<?php
		  } //Fsi
		?>
		<!-- Fin du tableau de contenu -->
	    </form>	  </td>
	</tr>
	<tr>
	  <td height="5"></td>
    </tr>
	<tr>
	  <td align="right">
	    <!-- Début de la barre des bouttons -->
		<table align="right" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td height="22">
		  <?php
            if($insert=='Y')                   //---> Autorisations suffisantes ?
            {
          ?>

		      <input type="button" name="Submit2" value="Ajouter" onClick="javascript: ajouter();" style="width:95px; cursor:pointer ">

		  <?php
		    } //Fsi
		  ?>		  </td>
	      <?php
            if($p->getTotal()!=0 && $select=='Y') //---> Autorisations suffisantes ?
            {
		  ?>
		  <td width="15"></td>
		  <td height="22">

		      <input type="button" name="Submit" value="Appliquer"  onClick="javascript: verif();" style="width:95px; cursor:pointer ">

	      <?php
		    } //Fsi
		  ?>		  </td>
		</tr>
		</table>
	    <!-- Fin de la barrre des bouttons -->	  </td>
	</tr>
    </table>
    <!-- Fin du tableau -->  </td>
</tr>
<tr>
  <td height="15" colspan="3"></td>
</tr>
</table>

<!-- --!-->


				</div>
			</div>

		</div>
	</div>
</section>
