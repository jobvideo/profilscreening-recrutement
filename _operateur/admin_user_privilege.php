<?php 
   include "../include/fonctions.php" ;
   include "../include/connexion.php";
   
   //---> Rubrique et id utilsateur valides ?
   $admin_user_id = isset($_GET['admin_user_id'])? $_GET['admin_user_id'] : 0;
   $rubrique_id   = getRubriqueId($connexion,"admin_user");
   
   include "../include/parametres.php";  //---> Tester la session et 
   include "../include/session_test.php"; //     Importer les variables $select, $mod, $insert, $delete
   include "../include/admin_user.php";  //---> Les fonctions du module admin_user

   //---> Test de la validit� des param�tres
   if (!isset($_GET['admin_user_id2']))
     die("Param�tre admin_user_id2");
     $sql = "SELECT admin_user_nom
           FROM   admin_user
		   WHERE  admin_user_id   = " . $_GET['admin_user_id2'];
		   
	  $res = $connexion->query($sql);	 
	  $res->execute();
	  
	  if ($res->rowCount()!=1)
     die("Param�tre invalide");
     $row  = $res->fetch();

   $admin_user_nom = affichage($ow['admin_user_nom']);

   if (isset($_POST['admin_user_privilege']))
   {
	 //---> Mettre � jour les privil�ges
	 $privilege_select = (isset($_POST['privilege_select'])) ? $_POST['privilege_select'] : NULL ;
	 $privilege_update = (isset($_POST['privilege_update'])) ? $_POST['privilege_update'] : NULL ;	 
	 $privilege_insert = (isset($_POST['privilege_insert'])) ? $_POST['privilege_insert'] : NULL ;
	 $privilege_delete = (isset($_POST['privilege_delete'])) ? $_POST['privilege_delete'] : NULL ;
	 privilege_modifier($_GET['admin_user_id2'],$privilege_select,$privilege_update,$privilege_insert,$privilege_delete);     

     include  "../include/operation_message.php";  //---> inclure fonction pour afficher un message	 
     operation_message("Modification Termin�e", TRUE); //---> Msg + Fermer la fen�tre 	 
     exit();
   } //Fsi
   
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Modifier les privil�ges</title>
<link href= "../include/style_admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="../include/scripts.js"></script>
<script type="text/javascript" language="javascript">
<!--
  function verif()
  {
	document.form1.submit();
  } //Fin verif
-->
</script>
</head>
<body bgcolor="#AAB7BD">
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td align="center" valign="middle" height="350">
    <!--******************************************************************************************
	                                 D�but de l'encadrement blanc
    *******************************************************************************************-->	  
	<table width="350" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	  <td width="7" height="7"><img src="./images/cgh.gif" width="7" height="7"></td>
 	  <td bgcolor="#FFFFFF" width="99%"></td>
	  <td width="7"><img src="./images/cdh.gif" width="7" height="7"></td>
	  <td width="1"></td>
	</tr>
	<tr>
	  <td bgcolor="#FFFFFF" width="7"></td>
	  <td bgcolor="#FFFFFF">
		<table align="center" width="100%" cellpadding="0" cellspacing="0">
		<tr>
		  <td valign="top">
		  <!--*************************************************************************************
		                                       D�but du contenu
		  **************************************************************************************-->
		    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
              <td colspan="2" height="10"></td>
            </tr>
            <tr valign="top" align="left">
              <td width="15" height="25"></td>
              <td>
                <span class="titre">Administrateurs</span>
              </td>
            </tr>
            <tr>
              <td colspan="2" height="2" bgcolor="#FF0000"></td>
            </tr>
            <tr valign="top" align="left">
              <td height="25"></td>
              <td >
                <img src="./images/flnoir.gif">
	            Changement de privil�ges
              </td>
            </tr>
            <tr valign="top">
              <td height="15"></td>
              <td></td>
            </tr>
            <tr valign="top">
              <td width="15" height="25"></td>
              <td >
              <form action="" method="post" name="form1">
			  <input type="hidden" name="admin_user_privilege">
  			    <!--******************************************************************************
				                              D�but du formulaire
				*******************************************************************************-->
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td valign="top">
                    <fieldset>
		              <legend>
					    <img src="./images/b_edit.png" width="16" height="16" border="0" align="absmiddle">
						Privil�ges :
					  </legend>
		              <br> 
                      <?php afficher_privilege($_GET['admin_user_id2'], "form1"); ?>
					  <br>
		              <table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
					  <tr align="left">
					    <td class="obligatoire">
						* Afin de garantir la s�curit� de votre site, n'offrez que les 


 privil&egrave;ges  n�cessaires.
						</td>
				      </tr>
					  </table>
					  <br>
		              </fieldset>
				  </td>
				  </tr>
				</table>
			    <!--******************************************************************************
				                               Fin du formulaire				
				*******************************************************************************-->				
			  </form>
              </td>
            </tr>
            <tr>
              <td height="25"></td>
              <td align="center">
			    <table border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
				  <td>
			        <div style="cursor:hand" onClick="javascript: verif();">
                      <table width="75"  border="0" cellpadding="0" cellspacing="0" class="menutext">
                        <tr>
                          <td width="6"><img src="./images/boutton-gauche.gif" border="0"></td>
                          <td width="401" align="center" style="background-image:url(./images/boutton-fond.gif)">Modifier</td>
                          <td width="11"><img src="./images/boutton-droite.gif" border="0"></td>
                        </tr>
                      </table>
			        </div>
				  </td>
				  <td width="25">&nbsp;</td>
				  <td>
			        <div style="cursor:hand" onClick="javascript: fermer_popup();">
                      <table width="75"  border="0" cellpadding="0" cellspacing="0" class="menutext">
                        <tr>
                          <td width="6"><img src="./images/boutton-gauche.gif" border="0"></td>
                          <td width="401" align="center" style="background-image:url(./images/boutton-fond.gif)">Fermer</td>
                          <td width="11"><img src="./images/boutton-droite.gif" border="0"></td>
                        </tr>
                      </table>
			        </div>
				  </td>
				</tr>
			    </table>
			  </td>
            </tr>															
            </table>
		  <!--*************************************************************************************
		                                       Fin du contenu
		  **************************************************************************************-->
		  </td>
		</tr>
		</table>
	  </td>
	  <td bgcolor="#FFFFFF" width="7"></td>		
	  <td bgcolor="#7A7B7B" width="1"></td>	  
	</tr>
	<tr>
	  <td height="7" width="7"><img src="./images/cbg.gif" width="7" height="7"></td>
	  <td style="background-image:url(./images/b.gif)"></td>
	  <td width="7"><img src="./images/cbd.gif" width="7" height="7"></td>
	  <td></td>
	</tr>	  
	</table>
    <!--******************************************************************************************
	                                   Fin de l'encadrement blanc
    *******************************************************************************************-->	
  </td>	
</tr>
</table>	
</body>
</html>