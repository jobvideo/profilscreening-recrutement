<?php
/* @var $candidat_id type */
if (isset($_GET['cnd']))
    if (is_numeric($_GET['cnd'])) {
        $candidat_id             = $_GET['cnd'];
        $cand                    = $candidat_id;
        $id_entreprise           = $_SESSION['id_entreprise'];
        $_SESSION['candidat_id'] = $candidat_id;

        $sql_entreprise     = "select * from entreprise where 	admin_user_id='$id_entreprise'";
        $res_entreprise     = $connexion->query($sql_entreprise);
        $donnees_entreprise = $res_entreprise->fetch();
        $raison_social      = $donnees_entreprise['entreprise_raison_social'];

        $sql_voir = "SELECT * FROM   candidat,ville,metier  "
                . "WHERE  candidat_id  ='$cand' "
                . "and  candidat.ville_id = ville.ville_id "
                . " and ( (candidat.metier_id = metier.metier_id ) or (candidat.candidat_emploi_rechercher = metier.metier_nom ))";
        $res_voir = $connexion->query($sql_voir);
        $row_voir = $res_voir->fetch();

        $sql_experience = "SELECT * FROM   experience  WHERE  candidat_id  ='$cand' ";
        $res_experience = $connexion->query($sql_experience);

        $sql_competence = "SELECT * FROM   graphic_competence  WHERE  candidat_id  ='$cand' ";
        $res_competence = $connexion->query($sql_competence);



        $candidat_nom             = ($row_voir['candidat_nom']);
        $candidat_prenom          = $row_voir['candidat_prenom'];
        $candidat_date_naissance  = $row_voir['candidat_date_naissance'];
        $candidat_sex             = $row_voir['candidat_sex'];
        $candidat_telephone       = $row_voir['candidat_telephone'];
        $candidat_email           = $row_voir['candidat_email'];
        $candidat_domicilite      = $row_voir['candidat_domicilite'];
        $candidat_codepostale     = $row_voir['candidat_codepostale'];
        $candidat_permis          = $row_voir['candidat_permis'];
        $candidat_mobilite        = $row_voir['candidat_mobilite'];
        $candidat_moyen_transport = $row_voir['candidat_moyen_transport'];
        $candidat_handicap        = $row_voir['candidat_handicap'];

        $candidat_ref               = $row_voir['candidat_ref'];
        $candidat_date_dispo        = $row_voir['candidat_date_dispo'];
        $candidat_profession        = $row_voir['candidat_profession'];
        $candidat_emploi_rechercher = $row_voir['candidat_emploi_rechercher'];
        $candidat_diplome           = $row_voir['candidat_diplome'];
        $candidat_login             = $row_voir['candidat_login'];
        $date_mise_ajour            = $row_voir['date_mise_ajour'];
        $amenagement                = $row_voir['amenagement'];
        $lequel                     = $row_voir['lequel'];
        $S_definit_comme            = $row_voir['S_definit_comme'];
        $candidat_certification     = $row_voir['candidat_certification'];
        $accompagne                 = $row_voir['accompagne'];
        $dossier                    = '../common/Fichiers/dossier_' . $candidat_ref;
        if ($row_voir['candidat_photo'] != 'img_vide.gif') {
            $candidat_photo = "../common/Images/candidat/" . stripslashes($row_voir['candidat_photo']);
        }
        else {
            $candidat_photo = "../common/Images/candidat/img_vide.gif";
        }

        $candidat_lettre_motivation = $dossier . "/" . stripslashes($row_voir['candidat_lettre_motivation']);
        $candidat_synthese          = $dossier . "/" . stripslashes($row_voir['candidat_synthese']);
        $candidat_video             = stripslashes($row_voir['candidat_video']);
        $candidat_cv                = $dossier . "/" . stripslashes($row_voir['candidat_cv']);
        $candidat_attestation       = $dossier . "/" . stripslashes($row_voir['candidat_attestation']);
        $candidat_diplome2          = $dossier . "/" . stripslashes($row_voir['candidat_diplome2']);
        $candidat_certificat        = $dossier . "/" . stripslashes($row_voir['candidat_certificat']);
        $exp                        = "";
        ?>

        <section id="featured-log" class="featured clearfix">
            <section id="profil" role="section" class="clearfix">
                <div id="cv" class="cv clearfix">
                    <div id="cv-content" class="cv-content">
                        <article class="cv-content-inner clearfix">
                            <figure id="portrait" class="portrait">
                                <img src="<?php echo $candidat_photo ?>" alt="image du candidat" />
                            </figure>
                            <div class="cv-text">
                                <h1><strong>  <?php echo $candidat_nom . " " . $candidat_prenom; ?></strong></h1>
                                <h2><?php echo $candidat_profession ?></h2>
                                <h3><?php echo $candidat_domicilite ?></h3>
                                <ul id="ref" class="ref">
                                    <li>Candidat disponible à partir du : <?php echo $candidat_date_dispo ?> </li>
                                    <li>Réf. <?php echo $candidat_ref ?> </li>
                                </ul>
                            </div>
                        </article>

                    </div>
                    <div id="cv-tabs" class="cv-tabs">
                        <div class="cv-tabs-inner">

                            <form id="share-form" class="mfp-hide form">
                                <fieldset>
                                    <ol>

                                        <li>
                                            <label for="identifiant">Email du destinataire |</label>
                                            <input class="zone" name="email" placeholder="ex : mon email@mondomaine.com" required="" type="text">
                                        </li>
                                    </ol>
                                    <p class="submit-button">
                                        <input name="submit" type="submit" id="send-message" class="submit" value="Envoyer" />
                                    </p>
                                </fieldset>
                                <button class="mfp-close" type="button" title="Close (Esc)">×</button>
                            </form><!--#share-form-->
                            <ul id="tabs" class="tabs">
                                <li><a class="tab1" id="firstonglet" href="#tabs-1" title="Présentation du candidat">Présentation</a></li>
                                <?php
                                if ($accompagne == 'OUI') {
                                    ?> 
                                    <li><a class="tab2" href="#tabs-2" title="Voir les compétences du candidat">Compétences</a></li>
                                    <li><a class="tab3" href="#tabs-3" title="Consulter la synthèse RH du candidat">Synthèse</a></li>

                                    <?php
                                }
                                ?>
                                <li><a class="tab4" href="#tabs-4" title="Télécharger les documents professionnels du candidat">Documents</a></li>
                                <li><a class="tab5" id="ssborder" href="#tabs-5" title="Voir l'ensemble des données du candidats">Résumé</a></li>
                            </ul>
                        </div>
                        <div id="content-tab" class="content-tab">
                            <div id="tabs-1" class="clearfix tab pres">
                                <p class="maj">Dernière mise à jour :  <?php echo $date_mise_ajour; ?></p>
                                <div id="left-col" class="left-col">
                                    <p class="tab-head">vidéo</p>
                                    <div class="embed-container">
                                        <iframe width="200" height="113" src="<?php echo $candidat_video; ?>" frameborder="0" allowfullscreen=""></iframe>
                                    </div>
                                </div><!--#left-col-->
                                <div id="right-col" class="right-col">
                                    <div class="head">
                                        <p class="tab-head">expériences</p>
                                        <?php
                                        $sql_exp = "SELECT * FROM   experience WHERE candidat_id='$candidat_id'";
                                        $res_exp = $connexion->query($sql_exp);
                                        $n       = 0;
                                        ?>
                                        <ul class="tab-cell">
                                            <?php
                                            while ($row_exp = $res_exp->fetch()) {
                                                $experience_titre = $row_exp['experience_titre'];
                                                if ($n > 0)
                                                    $exp              = $exp . ' , ' . $experience_titre;
                                                else
                                                    $exp              = $exp . '   ' . $experience_titre;
                                                $n++;
                                                ?>
                                                <li class="exp"> <?php echo $experience_titre; ?></li>

                                                <?php
                                            }
                                            ?>
                                        </ul>

                                    </div><!--.head-->

                                    <!--
                                                                                    <div class="head head-right">
                                                                                            <p class="tab-head">synthèse rh</p>
                                                                                            <p class="tab-cell synt">Cliquez <a href="http://jobvideo.fr/jobvid-composants/uploads/2015/03/modele-synthese-rh.pdf" target="_blank">ici</a> pour télécharger</p>
                                                                                    </div>
                                    --><!--.head-->

                                    <div class="list-caract">
                                        <ul class="tab-cell caract">
                                            <li><strong>diplôme</strong><?php echo $candidat_diplome ?></li>
                                            <li><strong>mobilité</strong> <?php echo $candidat_mobilite ?></li>
                                            <li><strong>permis de conduire</strong><?php echo $candidat_permis ?></li>
                                            <li><strong>recherche</strong><?php echo $candidat_emploi_rechercher ?></li>
                                            <li><strong>moyen de transport</strong><?php echo $candidat_moyen_transport ?></li>															</ul>
                                    </div><!--.list-caract-->
                                </div><!--#right-col-->
                            </div><!--#tabs-1-->
                            <?php
                            if ($accompagne == 'OUI') {
                                ?> 
                                <div id="tabs-2"  class="clearfix tab competences">
                                    <?php include"competence.php"; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <!--#tabs-2--->
                            <?php
                            if ($accompagne == 'OUI') {
                                ?> 
                                <div id="tabs-3"  class="clearfix tab quest">


                                    <div id="left-col" class="left-col">
                                        <p class="tab-head">synthèse rh</p>

                                        <p class="tab-cell synt">Cliquez   <a href="./synthese_tt.php?session=<?php echo $idses ?>" target="_blank">ici</a>pour télécharger</p>

                                        <p>Nos outils vous permettent de mieux connaître vos futurs collaborateurs, de cerner davantage leurs attentes, leur motivation et leurs compétences.</p>
                                        <p class="tab-head tab-assoc">Personnalité au travail</p>
                                        <p>Évaluez le profil professionnel de nos candidats, leur adéquation au(x) poste(s); Une synthèse de ces résultats vous est proposée. Profil Pro R est un test développé par Central Test.</p>
                                        <p class="tab-head tab-supp">Motivations</p>
                                        <p>Découvrez les principales sources de motivations professionnelles.</p>
                                        <p class="tab-head">Aptitudes</p>
                                        <p>Appréciez les qualités professionnelles et le savoir-faire de nos candidats. L'analyses des tests. L'analyse de nos tests est effectuée par des professionnels des ressources humaines diplômés en psychologie du travail. Ces tests ne font pas l´objet de préparations spécifiques. Ils témoignent de tendances observées au moment de l´entretien. Ces données sont dynamiques et peuvent évoluer selon des événements liés à  la vie professionnelle ou personnelle du candidat.</p>
                                        <p class="tab-head othertest">Besoin d´autres tests</p>
                                        <p>Vous avez retenu un candidat mais certains doutes persistent ? D´autres tests peuvent vous être proposés : Test de situation au travail, test d´aptitude, test de connaissances...</p>
                                        <p class="submit-button"><a class="submit" href="/#featured-contact">Contactez-nous</a></p>
                                    </div><!--#left-col-->



                                    <div id="right-col" class="right-col">


                                        <p class="tab-head tab-assoc">Personnalité au travail | graphique</p>

                                        <div id='Personnalite' style="font-size:10px;">
                                            <?php
                                            include"personnalite_graphe.php";
                                            ?>
                                        </div>


                                        <p class="tab-head tab-supp">Motivations | graphique</p>
                                        <div id='Motivations'>
                                            <?php
                                            include"motivation_graphe.php";
                                            ?>
                                        </div>
                                        <p class="tab-head tab-supp">Aptitudes | graphique</p>
                                        <div id='Aptitudes'>
                                            <?php
                                            include"aptitude_graphe.php";
                                            ?>
                                        </div>

                                    </div><!--#right-col-->	
                                </div><!--#tabs-3-->
                                <?php
                            }
                            ?> 
                            <div id="tabs-4"  class="clearfix tab docs">
                                <p class="tab-head">Certification</p>
                                <p>Les documents présentés sont régulièrement mis à jour par les candidats ou les opérateurs chargés de leur accompagnement. Ils sont certifiés comme exacts par leurs déposants dans un cadre contractuel avec notre équipe. Le dépôt de tout document incomplet, erroné, ou faux entraîne des sanctions graduées pouvant aller jusqu'à la suspension de la publication du profil du candidat ou sa radiation de notre base de données.
                                    Merci de nous avertir si vous constatez une inexactitude ou un faux.</p>
                                <div id="left-col" class="left-col">
                                    <p class="tab-head tabdocs">le CV</p>
                                    <p class="tab-cell synt">Cliquez <a href="<?php echo $candidat_cv; ?>" target="_blank">ici</a> pour télécharger</p>
                                    <p class="tab-head tabdocs">la lettre de motivation</p>
                                    <p class="tab-cell synt">Cliquez <a href="<?php echo $candidat_lettre_motivation; ?>" target="_blank">ici</a> pour télécharger</p>
                                    <p class="tab-head tabdocs">attestation(s)</p>
                                    <p class="tab-cell synt">
                                        Cliquez <a href="<?php echo $candidat_attestation; ?>" target="_blank">ici</a> pour télécharger</p>

                                </div><!--#left-col-->

                                <div id="right-col" class="right-col">
                                    <p class="tab-head tabdocs">les diplômes</p>
                                    <p class="tab-cell synt">
                                        Cliquez <a href="<?php echo $candidat_attestation; ?>" target="_blank">ici</a> pour télécharger</p>
                                    <p class="tab-cell synt">
                                        Cliquez <a href="<?php echo $candidat_diplome2; ?>" target="_blank">ici</a> pour télécharger</p>

                                    <p class="tab-head tabdocs">certificat(s)</p>
                                    <p class="tab-cell synt">
                                        Cliquez <a href="<?php echo $candidat_certificat; ?>" target="_blank">ici</a> pour télécharger</p>
                                </div><!--#right-col-->
                            </div><!--#tabs-4-->
                            <div id="tabs-5"  class="clearfix tab coord">
                                <div id="left-col" class="left-col">
                                    <ul class="tab-cell caract">
                                        <li><strong>nom</strong><?php echo $candidat_nom; ?></li>
                                        <li><strong>prénom</strong><?php echo $candidat_prenom; ?></li>
                                        <li><strong>téléphones</strong><?php echo $candidat_telephone; ?></li>
                                        <li><strong>email</strong><?php echo $candidat_email; ?></li>
                                        <li><strong>sexe</strong><?php echo $candidat_sex; ?></li>
                                        <li><strong>Né(e) le</strong><?php echo $candidat_date_naissance; ?></li>
                                        <li><strong>Département</strong><?php echo $candidat_codepostale; ?></li>
                                        <li><strong>Adresse</strong><?php echo $candidat_domicilite; ?> </li>
                                    </ul>
                                </div><!--#left-col-->
                                <div id="right-col" class="right-col">
                                    <ul class="tab-cell caract">
                                        <li><strong>mobilité</strong><?php echo $candidat_mobilite; ?> km</li>
                                        <li><strong>permis de conduire</strong><?php echo $candidat_permis; ?></li>
                                        <li><strong>moyen de transport</strong><?php echo $candidat_moyen_transport; ?></li>
                                        <li><strong>handicap</strong><?php echo $candidat_handicap; ?></li>
                                        <li><strong>aménagement de poste</strong><?php echo $amenagement; ?></li>
                                        <li><strong>lequel ?</strong><?php echo $lequel; ?></li>
                                        <li><strong>Se définit comme</strong><?php echo $S_definit_comme; ?> </li>
                                        <li><strong>Expériences</strong><?php echo $exp; ?> </li>
                                    </ul>
                                </div><!--#right-col-->
                                <p class="tab-head">synthèse rh </p>
                                <p class="tab-cell synt">Cliquez <a href="./synthese_tt.php?session=<?php echo $idses ?>" target="_blank">ici</a> pour télécharger</p>

                            </div>
                        </div><!--#tabs-5-->
                    </div><!--#content-tab-->
                </div>
                </div>
            </section>
        </section>
        <?php
    }
?>