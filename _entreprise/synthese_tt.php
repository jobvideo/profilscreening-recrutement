<?php
session_start();
$role="";
if(isset($_SESSION['role']))
$role = $_SESSION['role'];
include('../include/connexion.php');
include('../include/fonctions.php');
include("../include/parametres.php");
include("../include/pagination.php");
include "../include/session_test.php";  //---> Vérifier la session en cours, Fermer la session
 
 $idses = $_GET['session'];
 
 
$nom = $_SESSION['nom'];
 $candidat_id = $_SESSION['candidat_id'] ;
  ?>
 
<?php

 
$sql = "select * from candidat where candidat_id = '$candidat_id'";
            $res = $connexion->query($sql);
            $row = $res->fetch();
			$nom=$row['candidat_nom'];
			$prenom=$row['candidat_prenom'];
			
?>
<?php include('../include/header.php');?>
<body id="body" class="page cv-candidat">

<link rel="stylesheet" type="text/css" media="all" href= "../css/style_syntehese.css">

	<section id="featured-log" class="featured clearfix">

  
	
	<section id="profil" role="section" class="clearfix">
	<div id="cv" class="cv clearfix">
	 <div id="cv-tabs" class="cv-tabs">
<div id="content-tab" class="content-tab" style="padding:20px; text-align:justify;">
	 <p><img src="../images/logo.png"></p><br><br>
	<strong>Synthèse des résultats de <span style="color:#0000ff"><?php echo $prenom.'  '.$nom; ?></span></strong><br><br>
	<strong>Introduction</strong><br><br>
Nous recherchons la motivation, les compétences, les aptitudes, la personnalité au travail. Nos
résultats sont croisés à partir d'éléments déclaratifs et auto-évalués. Ils constituent de très bons
indicateurs pour la pré-sélection du candidat ou de son premier entretien. Pour cela, nous utilisons
des tests, des questionnaires internes et externes construits à partir de théories relevant de la
psychologie. Lors d'entretiens individuels ou collectifs, nous évaluons les capacités de nos
candidats durant 12 ou 18 heures, selon nos méthodes d'évaluation. Notre objectif est de présenter
le parcours, le projet professionnel, les ambitions. Notre rôle est d'appréhender la personne dans sa
globalité et sa singularité.
L´analyse de nos tests est effectuée par des professionnels des ressources humaines diplômés en
psychologie du travail. Ces tests ne font pas l´objet de préparations spécifiques. Ils témoignent de
tendances observées au moment de l´entretien. Ces données sont dynamiques et peuvent évoluer
selon des événements liés à la vie professionnelle ou personnelle du candidat.
Nos tests sont publiés avec l´accord du candidat et commentés par celui-ci lorsqu´il le souhaitent.
Les commentaires permettent une meilleure appréciation des résultats, du positionnement et de la
subjectivité de celui-ci.<br><br>
<strong>Les résultats</strong><br><br>
<span style="color:#0000ff"><strong>Personnalité au travail</strong></span><br><br>
Nous proposons une synthèse du test de personnalité, extraite de Profil Pro-R, outil de Central Test.
Celui-ci est utilisé par les psychologues, les DRH et les consultants en recrutement. Profil Pro-R
analyse 12 dimensions fondamentales de la personnalité et des motivations. Il met en lumière le
profil professionnel du candidat.

Tableau d'évaluation de la personnalité au travail
Remarque : la case F est l'axe neutre central au tableau. A représente la plus grande valeur
accordée au facteur opposé, K la plus grande valeur pour le facteur principal.<br><br>

<?php 

include"personnalite_graphe.php";
?>
<br><br>
<span style="color:#0000ff"><strong>Motivations</strong></span><br><br>
Découvrez les principales sources de motivations professionnelles établies à partir d'une trentaine
d'affirmations évaluées selon 4 niveaux.<br><br>
 <strong>Schéma des résultats : </strong><br><br>
 <?php 

include"motivation_graphe.php";
?>
<span style="color:#0000ff"><strong> Aptitudes « capacités à réaliser des actions »</strong></span><br><br>
Présentation de 10 aptitudes principales, évaluées à partir d'un questionnaire composé de 69
critères.
Chaque aptitude est évaluée selon le barème suivant :
- 1= je ne possède pas cette aptitude
- 2= je possède peu cette aptitude
- 3= je possède cette aptitude et l’utilise parfois
- 4= je possède cette aptitude et l’utilise souvent
8 est la note maximum cumulant l'aptitude évaluée dans le champ des expériences personnelles et
professionnelles.<br><br>
 <strong>Schéma des résultats : </strong><br><br>
<?php 

include"aptitude_graphe.php";
?>
<span style="color:#0000ff"><strong>Compétences techniques</strong></span><br><br>
<?php 

include"competence.php";
?>

</div>
</div>
</div>
</section>
</section>
<?php
$filename = 'Synthèse RH '.$prenom.'  '.$nom .'.xls';
 //header("Content-type: application/vnd.ms-excel");
 //header("Content-Disposition: attachment; filename=$filename");
?>
</body>
</html>