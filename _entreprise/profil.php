<?php include('include/header-profil-entreprise.php');
?>
<div id="cv-tabs" class="cv-tabs">
    <div class="cv-tabs-inner clearfix">
        <a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>
        <ul id="tabs" class="tabs clearfix">
            <li><a class="tab1 activate" id="firstonglet" href="./?p=profil&session=<?php echo $session ?>#tabs-1" title="Voir votre profil">Votre profil</a></li>
            <li><a class="tab2" id="#tabs-2" href="./?p=profils_consulte&session=<?php echo $session ?>#tabs-2"  title="Historique de vos consultations"><span class="nbre_ex"><?php echo $nb_consult ?></span><span class="txtNbre">Profil(s) consulté(s)</span></a></li>
            <li><a class="tab3" id="#tabs-3" href="./?p=profils_parraine&session=<?php echo $session ?>#tabs-3"  title="Historique de vos achats"><span class="nbre_ex"><?php echo $nb_acht ?></span><span class="txtNbre">Profil(s) parrainé(s)</span></a></li>
            <li><a class="tab5" id="#tabs-5" href="./?p=rechercher&session=<?php echo $session ?>#tabs-5"  title="Faire une recherche cv multimédia">Rechercher un candidat</a></li>
           

        </ul>
    </div>
    <div id="content-tab" class="content-tab">
        <div id="tabs-1" class="clearfix tab pres">
            <article class="profil-content-inner clearfix">
                <figure id="portrait" class="portrait portrait-entreprise">
                    <img src="./common/Images/entreprise/<?php echo $entreprise_photo; ?>" alt="logo-entreprise"/>
                </figure>
                <div class="cv-text">
                    <h2> <?php echo $nom; ?></h2>
                    <h2 class="skills"><?php echo $entreprise_domaine_activite; ?></h2>
                    <h3><?php echo $entreprise_adresse; ?>,<?php echo $entreprise_code_postale; ?> - <?php echo $entreprise_vile; ?></h3>
                    <h3 class="tel">Tél : <?php echo $entreprise_tel; ?></h3><span class="seperate">|</span><h3 class="siret">Siret : <?php echo $sirete; ?></h3>
                    <ul id
                        ="ref" class="ref">
                        <li>Dernière mise à jour :  <?php echo $entreprise_date_modif; ?></li>
                        <li>Réf. <?php echo $entreprise_ref; ?></li>
                    </ul>
                </div>
                <div id="left-col" class="left-col">
                    <p class="tab-head">vidéo</p>
                    <div class="embed-container"><iframe width="200" height="113" src="<?php echo $entreprise_video_url; ?>" frameborder="0" allowfullscreen></iframe></div>
                </div>
                <!--#left-col-->
                <div id="right-col" class="right-col">
                    <div class="head">
                        <p class="title-tab-head tab-head">Les plus de l’entreprise</p>
                        <ul class="tab-cell">
                            <li class="exp"><?php echo $entreprise_description; ?>.</li>
                        </ul>
                    </div>
                </div>
                <!--#right-col-->
            </article>
        </div>
    </div>
</div>
</div>
</section>

