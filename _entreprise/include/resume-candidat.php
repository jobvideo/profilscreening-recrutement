<section class="cv-content cv-resume clearfix">
	<article class="cv-content-inner">
		<div class="cv-file clearfix">
			<div class="cv-contener clearfix">
				<figure id="portrait" class="portrait">
					<img src="http://jobvideo.fr/wp-content/uploads/2015/03/portrait-eric.jpg" alt="" />
				</figure>
				<div class="cv-text">
					<div class="cv-col">
						<h2><strong>Éric</strong></h2>
						<h2>Formateur-consultant</h2>
						<h3>Le Havre</h3>
					</div>
					<div class="cv-col">
						<ul id="ref" class="ref">
							<li>Candidat disponible à partir du : 16/05/2015</li>
							<li>Réf. 76jbv000test</li>
						</ul>
						<p class="titleInterest">votre intérêt pour le candidat</p>
						<div class="btnRadio clearfix">
							<label class="choix" for="peu interessant"><span></label>
							<input type="radio" id="choix1" value="choix1" name="choix"><label for="choix1"><span></span><p>Peu intéressant</p></label>
							<label class="choix" for="interessant"><span></label>
							<input type="radio" id="choix2" value="choix2" name="choix"><label for="choix2"><span></span><p>Intéressant</p></label>
							<label class="choix" for="très interessant"><span></label>
							<input type="radio" id="choix3" value="choix3" name="choix"><label for="choix3"><span></span><p>Très intéressant</p></label>
						</div>
					</div>
				</div>
			</div>
		<div class="buttonResume clearfix">
				<a class="button delete-profil" title="Cliquer pour supprimer le profil" href="">supprimer le profil</a>
				<a class="button voir-profil" title="Cliquer pour voir le profil" href="">voir le profil</a>
				<a class="button add-profil" title="Cliquer pour ajouter le profil à votre panier" href="">ajouter le profil au panier</a>
		</div>
	</div>
	</article>
</section>