<form   name="recherche" action="./index.php?session=<?php echo $idses ?>&p=rechercher#tabs-5" method="POST">
    <section class="cv-search-contener">
        <h3>Votre recherche :</h3>
        <p class="titleInterest">accéder directement au profil candidat</p>
        <section class="filter clearfix">
            <input type="text" class="reference" for="reference" name="ref" id="ref" value="" placeholder="Numéro de Référence JOBVIDÉO"/>

        </section>
        <p class="titleInterest">effectuer une recherche à l'aide de critères</p>
        <section class="filter clearfix">
            <div class="work">
                <label>par métier</label>
                <select class="select dispo" name="metier">
                    <optgroup>
                        <option value="">Sélectionnez un métier </option>
                        <?php
                        $sql      = "select * from metier";
                        $res_rech = $connexion->query($sql);

                        while ($row_rech = $res_rech->fetch()) {
                            $metier_nom = $row_rech['metier_nom'];
                            $metier_id  = $row_rech['metier_id'];
                            ?>
                            <option value="<?php echo $metier_id; ?>"> <?php echo $metier_nom; ?> </option>

    <?php
}
?>			</optgroup>
                </select>
            </div>
            <div class="places">
                <label>par ville</label>
                <select class="select interest" name="ville" >
                    <optgroup>
                        <option value=""> Sélectionnez une ville</option>
<?php
$sql      = "select * from ville";
$res_rech = $connexion->query($sql);

while ($row_rech = $res_rech->fetch()) {
    $ville_nom = $row_rech['ville_nom'];
    $ville_id  = $row_rech['ville_id'];
    ?>

                            <option value="<?php echo $ville_id; ?>"> <?php echo $ville_nom; ?> </option>

                            <?php
                        }
                        ?>
                    </optgroup>
                </select>
            </div>
            <input type="submit" class="submit valid" for="valider" name="submit_rech" value="valider"/>
        </section>
    </section> 
</form>

<section class="content-search">
    <form   name="filtre" action="./index.php?session=<?php echo $idses ?>#tabs-5" method="POST">

        <section class="filter clearfix">
            <label>filtrer par</label>
            <select class="select dispo" name='dispo' onchange="filtre.submit();">
                <optgroup>

                    <option value="">Disponibilité</option>
                    <option  value="<?php echo date("Y/m/d"); ?>" >immédiatement</option>
                    <option  value="" >le 16/05/2016</option>
                </optgroup>
            </select>
            <select class="select interest"  name='vote'  onchange="filtre.submit();">
                <optgroup>
                    <option value="">Evaluation</option>
                    <option  value="1">Peu intéressant</option>
                    <option  value="2">intéressant</option>
                    <option  value="3">très intéressant</option>
                </optgroup>
            </select>
        </section>
    </form>

 <?php  include('./include/resultat_recherche.php');?>
 
</section>
