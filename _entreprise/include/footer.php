			<footer id="footer">
				<nav class="menu-footer-container">
					<ul id="nav-footer" class="nav-footer wrapper clearfix">
						<li id="menu-item-216" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-216">
							<a href="/">Accueil</a>
						</li>
						<li id="menu-item-212" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-212">
							<a href="http://jobvideo.fr/historique/">Historique</a>
						</li>
						<li id="menu-item-213" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-213">
							<a href="http://jobvideo.fr/deontologie/">Déontologie</a></li>
						<li id="menu-item-222" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222">
							<a href="http://jobvideo.fr/partenaires/">Partenaires</a>
						</li>
						<li id="menu-item-223" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-223">
							<a href="/#featured-contact">Contact</a>
						</li>
						<li id="menu-item-221" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221">
							<a href="http://jobvideo.fr/mentions-legales/">Mentions légales</a>
						</li>
					</ul>
				</nav>
				<div class="nav-footer-second wrapper clearfix">
					<p class="bdd">Base de données coopérative RH</p>
					<a class="scop" href="http://www.les-scop.coop/sites/fr/">Société à statut Scop</a>
				</div>
			</footer>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
			<script type="text/javascript" src="../scripts/functions.js"></script>
			<script type="text/javascript" src="../scripts/tabs.js"></script>
			<script type="text/javascript" src="../scripts/jquery.easydropdown.min.js"></script>
			<script type="text/javascript" src="../scripts/velocity.min.js"></script>
			<script type="text/javascript" src="../scripts/main.js"></script>
			<script type="text/javascript" src="../scripts/modernizr.js"></script>
			<script type="text/javascript" src="../scripts/respond.js"></script>
		</main>
	</body>
</html>