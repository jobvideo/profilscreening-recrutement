  <?php
    $idses = $_GET['session'];
	?>
	<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="fr-FR"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="fr-FR"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!--><!--<![endif]-->
<html lang="fr-FR">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<title>jobvideo.fr &#124; Le CV vidéo &#124; abonnements</title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="./images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" media="all" href="../css/styles.css" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Nunito:700' rel='stylesheet' type='text/css'>
	<link rel="pingback" href="http://jobvideo.fr/xmlrpc.php" />
	<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
	<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
	<!--[if IE]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen"><![endif]-->
	<!--[if lt IE 9]><style>.content{height: auto; margin: 0;}	.content div {position: relative;}</style><![endif]-->
</head>
<body id="body" class="page page-id-430 page-child parent-pageid-5 page-template page-template-PageTemplate page-template-page-tarif page-template-PageTemplatepage-tarif-php logged-in page-abonnements">
	<div class="overlay" id="overlay"></div>
	<main id="main" role="main">
		<div class="btn_up"></div>
		<header id="header" class="header clearfix">
			<nav id="nav-widget" class="nav-widget clearfix">
				<div class="wrapper">
					<ul class="breadcrumbs">
						<li><a class="home-icon" href="../site_wordpress">Accueil</a></li>
						<li class="separator"> > </li>
						<li>Vous êtes ici : entreprises</li><li> 
								 <a href="./?p=panier&session=<?php  echo $idses?>">
												 <img  src="./images/btn_fp_panier.gif" with="100"> &nbsp;     
								  Consulter le panier </a>  </li>
					</ul>
					<div id="content-nav-widget" class="clearfix">
					</div>							
				</div>
			</nav>
			<nav class="wrapper clearfix">
				<div class="navInner">
					<a href="#" id="nav-Responsive" class="navResponsive" title="ouvrir la navigation"><span class="menuBurger">ouvrir la navigation</span></a>
					<a id="logo" class="logo" href="../site_wordpress" title="retour à l'accueil">jobvideo.fr</a>
				</div>
				<div id="nav-header" class="menu-entreprises-container">
					<ul id="menu-entreprises" class="menu-header clearfix">
						<li id="menu-item-123" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123">
							<a href="./?p=rechercher&session=<?php  echo $idses?>">Rechercher</a>
						</li>
						<li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124">
							<a href="./?p=grille&session=<?php  echo $idses?>">Grille des prix</a>
						</li>
						<li id="menu-item-124" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124">
							<a href="./?p=profil&session=<?php  echo $idses?>">Modifier Profil</a>
						</li>
						<li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-128">
							<a title="pour nous contacter" href="./?p=historique&session=<?php  echo $idses?>">Parrainé</a>
						</li>
						<li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-128">
							<a title="pour nous contacter" href="./?p=consul&session=<?php  echo $idses?>">Consulté</a>
						</li>
					</ul> 
						   
				</div>
			</nav>
		</header>
	