<?php include('include/header-profil-entreprise.php'); ?>
<div id="cv-tabs" class="cv-tabs">
    <div class="cv-tabs-inner clearfix">
        <a href="./?p=editer_profil&session=<?php echo $session ?>#tabs-4" class="btn edit" title="Modifier son profil">Editer son profil</a>
        <ul id="tabs" class="tabs clearfix">
            <li><a class="tab1" id="firstonglet" href="./?p=profil&session=<?php echo $session ?>#tabs-1" title="Voir votre profil">Votre profil</a></li>
            <li><a class="tab2" id="#tabs-2" href="./?p=profils_consulte&session=<?php echo $session ?>#tabs-2"  title="Historique de vos consultations"><span class="nbre_ex"><?php echo $nb_consult ?></span><span class="txtNbre">Profil(s) consulté(s)</span></a></li>
            <li><a class="tab3 activate" id="#tabs-3" href="./?p=profils_parraine&session=<?php echo $session ?>#tabs-3"  title="Historique de vos achats"><span class="nbre_ex"><?php echo $nb_acht ?></span><span class="txtNbre">Profil(s) parrainé(s)</span></a></li>
            <li><a class="tab5" id="#tabs-5" href="./?p=rechercher&session=<?php echo $session ?>#tabs-5"  title="Faire une recherche cv multimédia">Rechercher un candidat</a></li>
            

        </ul>
    </div>
    <div id="content-tab" class="content-tab">

        <div id="tabs-3" class="clearfix tab sold">
            <form   name="filtre_ach" action="./index.php?session=<?php echo $idses ?>#tabs-5" method="POST">

                <section class="filter clearfix">
                    <label>filtrer par</label>
                    <select class="select dispo" name='dispo_ach' onchange="filtre_ach.submit();">
                        <optgroup>

                            <option value="">Disponibilité</option>
                            <option  value="<?php echo date("Y/m/d"); ?>" >immédiatement</option>
                            <option  value="" >le 16/05/2016</option>
                        </optgroup>
                    </select>
                    <select class="select interest"  name='vote_ach'  onchange="filtre_ach.submit();">
                        <optgroup>
                            <option value="">Evaluation</option>
                            <option  value="1">Peu intéressant</option>
                            <option  value="2">intéressant</option>
                            <option  value="3">très intéressant</option>
                        </optgroup>
                    </select>
                </section>
            </form>
            <?php include('include/candidat_parrainer.php'); ?>
        </div>


    </div>
</div>
</div>
</section>

