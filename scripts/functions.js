$(function() {
    //SUPPRIMER <div class="textwidget">
    $('.textwidget').replaceWith($('.textwidget').contents());

    //BUTTON UP
    $('.btn_up').click(function(e) {
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
        e.preventDefault();
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() < 300) {
            $('.btn_up').fadeOut();
        } else {
            $('.btn_up').fadeIn();
        }
    });

    //ONGLETS
    var fade = function(id, s, e) {
        s.tabs.removeClass(s.activate);
        s.tab(id).addClass(s.activate);
        s.items.fadeOut();
        s.item(id).fadeIn();
        return false;
        e.preventDefault();
    };
    $.fn.fadeTabs = $.idTabs.extend(fade);
    $(".tabs").fadeTabs();

    //NAVIGATION RESPONSIVE
    $('#nav-Responsive').click(function(e){
	$('#nav-Responsive').toggleClass('closeIcon');
	$('.menu-header').toggleClass('navOn');
	$('#overlay').toggleClass('overlayOn');
	e.preventDefault();
	});
	//AJOUT HEADER NAVIGATION EN RESPONSIVE
	if(window.matchMedia("(max-width:2560px)").matches){
		$(window).scroll(function() {
			$header = $('.header');
			$div = $('#nav-header > div');
	        if ($(window).scrollTop() < 100) {
		        $div.removeClass('navThin');
		        $header.removeClass('navThin');
		    }else{
		        $div.addClass('navThin');
		        $header.addClass('navThin');
	        }
		});
	}
	//MODIFICATION PROFIL CANDIDAT (POP-UP)
	if($('#btn-share-1').length){
		$('#btn-share-1').magnificPopup({
			type: 'inline',
			preloader: false,
		});
	}
	//PARTAGE URL PROFIL CANDIDAT (POP-UP)
	if($('#btn-share').length){

		$('#btn-share').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#name',

			// When elemened is focused, some mobile browsers in some cases zoom in
			// It looks not nice, so we disable it:
			callbacks: {
				beforeOpen: function() {
					if($(window).width() < 700) {
						this.st.focus = false;
					} else {
						this.st.focus = '#name';
					}
				}
			}
		});
	}

	//EFFET SUR NOMBRE TAB
	if($('.nbre').length){
		(function(){
		    counter = {
		        $element : null,
		        count : 0,
		        maxCount : max_nb,
		        interval : null,
		        //Initialize
		        init : function(compteur){
		            this.$element = compteur;
		            this.run();
		            this.interval = window.setInterval("counter.run();", 50);
		        },
		        // Run
		        run : function(){
		            if (this.count === this.maxCount){
		                window.clearInterval(this.interval);
		            }

		            this.$element.html(this.count);
		            this.count++;
		        }
		    };

		    $.fn.counter = function(){
		        counter.init(this);
		    }

		    $(".nbre").counter();
		})();
	}
	//FILTRE
	if($('.filter').length){
		$(function(){
			$('.select').easyDropDown();
		});
	}
	//DROPDOWN
	if($('.formulaire').length){
		$(function(){
			$('select').easyDropDown();
		});
	}
	//CHANGE DE VUE LIST VS THUMBNAILS
	if($('.view-choice').length){
		$('.view-list').click(function(e){
			$('.cv-content').addClass('list');
			$('.view-list').addClass('activ');
			$('.view-thumbnail').removeClass('activ');
			e.preventDefault();
		});
		$('.view-thumbnail').click(function(e){
			$('.cv-content').removeClass('list');
			$('.view-list').removeClass('activ');
			$('.view-thumbnail').addClass('activ');
			e.preventDefault();
		});
	}
});