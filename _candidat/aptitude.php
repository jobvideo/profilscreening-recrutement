<?php
$candidat_id  = $_SESSION['candidat_id'];
/* * *******************************************************************************************************
  Gestion de la pagination
 * ******************************************************************************************************** */
//---> Créer un objet de pagination sans condition SQL sur la table
$sql_question = "select *  from question where partie_synthese_id = '2' order by question_position";
$res          = $connexion->query($sql_question);
?>







<fieldset>
    <legend> Questionnaire d’aptitudes : </legend><br>
    <p>Ce questionnaire est une auto-évaluation. Il a pour objectif de vous faire réfléchir aux aptitudes que vous possédez. Une aptitude est la capacité à réaliser une action. Au sein d’un travail, plusieurs peuvent ainsi être nécessaires. 
        <br>
        Ce questionnaire peut être rempli en 30 minutes environ. Il n’est cependant pas limité dans le temps. 
        <br>
        Vous allez donc devoir, dans un premier temps, évaluer la présence ou non de 69 aptitudes. Celles-ci ont été répertoriées de manière à noter l’ensemble des aptitudes présentes dans tous les métiers. De ce fait, certaines vont vous paraître éloignées de votre projet professionnel. Il vous faudra tout de même répondre. 
        <br>
        Vous devrez utiliser une échelle de valeur variant de 1 à 4, sachant que :
        <br><br>
    <dl class="faq">
        <dt>  = je ne possède pas cette aptitude ; </dt>
        <dt>  = je possède peu cette aptitude ; </dt>
        <dt>  = je possède cette aptitude et l’utilise parfois; </dt>
        <dt>  = je possède cette aptitude et l’utilise souvent .  </dt>
    </dl><br>
    Evaluez chaque aptitude mise en pratique lors de vos expériences personnelles et professionnelles. Notez la fréquence de son utilisation. 
    <br>
    Décrivez quelques situations où certaines de vos aptitudes ont été particulièrement utilisées dans le domaine personnel ou professionnel. Précisez s’il s’agit d’une mission de travail ou d’un loisir. Précisez cette démarche. 
    <br>
    Pour l’ensemble de ce questionnaire, soyez le plus précis possible. Ces données compléteront votre profil, il importe qu’elles vous correspondent au mieux. Il n’y a pas de bonnes ou mauvaises réponses. Répondez le plus honnêtement possible pour ne pas fausser votre profil<br>
    <br>	 
    <div style="padding-bottom:20px;" >
        <table border="1" width="90%">
            <tr>
                <td></td>
                <td width="10">Q</td>
                <td width="10">P</td>
                <td  > </td>			  
                <td width="30">Quotidienne</td>
                <td width="30">Professionnelle</td>

            </tr>
<?php
$val     = array();
$nom_val = array();
$n       = 0;

while ($row = @$res->fetch()) {
    $session      = $_GET["session"];
    $question_id  = $row['question_id'];
    $question_nom = ( ($row['question_nom']) );

    $question_visible = ($row['question_visible'] == 'Y') ? "CHECKED" : "";

    //-->les catégories de chaque question



    $sql2 = " SELECT * FROM reponse_question WHERE  question_id = $question_id ";
    $res2 = $connexion->query($sql2);
    ?> 

                <?php
                $nb   = 0;
                while ($row2 = @$res2->fetch()) {
                    $nb++;
                    $sql_existe = "select * from reponse_candidat where question_id  = '$question_id'  and
								reponse_id  = '" . $row2['reponse_question_id'] . "' and candidat_id = '$candidat_id'";
                    $res_existe = $connexion->query($sql_existe);
                    $nbre       = @$res_existe->rowCount();
                    if ($nbre == 0) {
                        $q = "";
                        $p = "";
                    }
                    else {
                        $row_existe = @$res_existe->fetch();
                        $q          = $row_existe['Valeur_Quotidienne'];
                        $p          = $row_existe['Valeur_Professionnelle'];
                        if ($nb == 1) {
                            $Valeur_Quotidienne     = $row_existe['Moyen_apptitude_Q'];
                            $Valeur_Professionnelle = $row_existe['Moyen_apptitude_P'];
                            $val[$n]                = ($Valeur_Quotidienne + $Valeur_Professionnelle);
                            $nom_val[$n]            = ($question_nom);

                            $n++;
                        }
                        else {
                            $Valeur_Quotidienne     = "";
                            $Valeur_Professionnelle = "";
                        }
                    }
                    ?>
                    <tr>
                        <td><b><?php echo affichage($question_nom, " ") ?></b></td>
                        <td width="10"><?php echo $Valeur_Quotidienne; ?> </td>
                        <td width="10"><?php echo $Valeur_Professionnelle; ?></td>						 
                        <td  > <?php echo affichage(($row2['reponse_question_nom']), "---"); ?></td>
                    <input type="hidden" name="quest_<?php echo $question_id; ?>">	
                    <input type="hidden" name="rep_<?php echo $row2['reponse_question_id']; ?>">									
                    <td width="30"><input type="text" name="Quotidienne_<?php echo $row2['reponse_question_id']; ?>" value="<?php echo $q; ?>" disabled></td>
                    <td width="30"> <input type="text" name="Professionnelle_<?php echo $row2['reponse_question_id']; ?>" value="<?php echo $p; ?>" disabled></td>
                    </tr>
        <?php
        $question_nom           = "";
        $Valeur_Quotidienne     = "";
        $Valeur_Professionnelle = "";
    }
    ?> 
                <?php
            }
            ?> 
        </table>
        <div  > &nbsp;

        </div>				
    </div>

    <script type="text/javascript" src="../js/ajax.jquery.js"></script>
    <script src="../js/highcharts2.js" ></script>



    <script type="text/javascript">
        $(function () {
            var chart;
            $(document).ready(function () {
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'apptitud',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Schéma des résultats '
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
                        }

                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function () {
                                    return '<b>' + this.point.name + '</b>: ' + this.y;
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Apptitude',
                            data: [
<?php
for ($i = 0; $i < count($val); $i++)
    echo "
['" . ($nom_val[$i]) . "',   " . round($val[$i], 2) . "],
                    ";
?>
                            ]
                        }]
                });
            });

        });
    </script>
    <div id="apptitud" style="min-width: 400px; height: 400px; margin: 0 auto"></div>




</fieldset>	 



