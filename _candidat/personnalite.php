<?php
$candidat_id  = $_SESSION['candidat_id'];
$sql_question = "select *  from question where partie_synthese_id = '3' order by question_position";
?>




<fieldset>
    <legend> Questionnaire Personnalité : </legend><br>
    <p>
        Nous recherchons la motivation, les compétences, les aptitudes, la personnalité au travail. Nos
        résultats sont croisés à partir d'éléments déclaratifs et auto-évalués. Ils constituent de très bons
        indicateurs pour la pré-sélection du candidat ou de son premier entretien. Pour cela, nous utilisons
        des tests, des questionnaires internes et externes construits à partir de théories relevant de la
        psychologie. Lors d'entretiens individuels ou collectifs, nous évaluons les capacités de nos
        candidats durant 12 ou 18 heures, selon nos méthodes d'évaluation. Notre objectif est de présenter
        le parcours, le projet professionnel, les ambitions. Notre rôle est d'appréhender la personne dans sa
        globalité et sa singularité.
        L´analyse de nos tests est effectuée par des professionnels des ressources humaines diplômés en
        psychologie du travail. Ces tests ne font pas l´objet de préparations spécifiques. Ils témoignent de
        tendances observées au moment de l´entretien. Ces données sont dynamiques et peuvent évoluer
        selon des événements liés à la vie professionnelle ou personnelle du candidat.
        Nos tests sont publiés avec l´accord du candidat et commentés par celui-ci lorsqu´il le souhaitent.
        Les commentaires permettent une meilleure appréciation des résultats, du positionnement et de la
        subjectivité de celui-ci. 
    </p>					

    <p>
        <!-- Début de l'enête de pagination -->
        <?php
        $res  = $connexion->query($sql_question);   //---> Afficher les bouttons
        ?>
        <!-- Fin de l'enête de pagination -->
    </p>
    <div style="padding-bottom:20px;" >
        <div  > &nbsp;

        </div>	
        <table border="1" width="90%">
            <tr>

                <td width="10">FACTEUR OPPOSE</td>

                <td  > </td>			  

                <td width="30">FACTEUR PRINCIPAL</td>

            </tr>
            <?php
            $i    = 0;
            $nbre = @$res->rowCount();
            $n    = 0;

            while ($row = @$res->fetch()) {
                $i++;
                $n++;
                $disabled         = ($mod != 'Y') ? "disabled" : "";
                $color            = ($i % 2 != 0) ? "#EFEFEF" : "#E9E9E9";
                $session          = $_GET["session"];
                $question_id      = $row['question_id'];
                $question_nom     = affichage(($row['question_nom']), "---");
                $question_visible = ($row['question_visible'] == 'Y') ? "CHECKED" : "";

                //-->les catégories de chaque question
                $sql2 = " SELECT * FROM reponse_question WHERE  question_id = $question_id ";
                $res2 = $connexion->query($sql2);
                ?>

                <tr>
                    <td> <?php echo $question_nom; ?>  </td>
                    <td></td>
                    <td>   <?php echo affichage(($row['oppose']), "---"); ?> </td>
                </tr>


    <?php
    while ($row2 = @$res2->fetch()) {
        $sql3         = " SELECT * FROM reponse_candidat WHERE  question_id = $question_id and
							  reponse_id = '" . $row2['reponse_question_id'] . "' and candidat_id = '$candidat_id' ";
        $res3         = $connexion->query($sql3);
        $row3         = @$res3->fetch();
        if ($row3['personnalite'])
            $personnalite = $row3['personnalite'] * 100 / 11;
        else
            $personnalite = 0;
        ?>
                    <tr>

                        <td  ><b> <?php echo affichage(($row2['reponse_question_nom']), "---"); ?></b></td>
                        <td width="210" align="center" >
                            <input type="hidden" name="quest_<?php echo $row2['question_id']; ?>">	
                            <input type="hidden" name="rep_<?php echo $row2['reponse_question_id']; ?>">	
                            <input type="range" name='personnalite_<?php echo $row2['reponse_question_id']; ?>' value="<?php echo $personnalite; ?>" max="+" min="+" step="1" disabled>
                        </td>							
                        <td>  <b><?php echo affichage(($row2['oppose']), "---"); ?></b></td>
                    </tr>
        <?php
        $question_nom = "";
        $oppose       = "";
    }
    ?>   


                <?php
            }
            ?> 
        </table> 
        <div  > &nbsp;

        </div>				
    </div>
</fieldset>	 

