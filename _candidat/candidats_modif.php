<?php include('include/header-modif-profil.php'); ?>
<!-- <link rel="stylesheet" type="text/css" media="all" href= "../css/bootstrap-old.css"> -->

<?php include("candidat-modifs-core.php");?>

<section class="container-steps">

<!--                 <script src="../js/jquery.js"></script> -->
<!--
                <link media="all" type="text/css" rel="stylesheet" href="../css/styleCand.css">
                <link media="all" type="text/css" rel="stylesheet" href="../css/bootstrap.css">
-->

                <form  class="" action="" method="post" name="form1" id="form1" enctype="multipart/form-data">

                    <div class="container-fluid">
                        <ul class="setup-panel row">
                            <li class="col-4 active">
	                            <a class="d-flex justify-content-center align-items-center" href="#step-1">
                                   Étape 1<span>Mes informations</span>
                                </a></li>
                            <li class="col-4 col-step-2 disabled">
	                            <a class="d-flex justify-content-center align-items-center" href="#step-2">
	                                   Étape 2<span>Mes documents</span>
	                            </a>
                            </li>
                            <li class="col-4 disabled">
                            	<a class="d-flex justify-content-center align-items-center" href="#step-3">
                                	Étape 3<span>Mes expériences</span>
                                </a>
                           </li>
                        </ul>
                    </div>

<?php include("step1.php"); ?>
<?php include("step2.php"); ?>
<?php include("step3.php"); ?>
                    <p class="checkbox-container d-flex align-items-center">
                        <input name="Acceptation" id="Acceptation" type="checkbox" class='zone' checked="">
                        J'accepte la politique d'utilisation des données de Profilsreening©<span class="obligatory">*</span>
                    </p>


                    <button><input type="submit" name="Submit" value="<?php echo $op ?>" onclick="verif();"><i class="ti-save"></i></button>

                </form>

                <script type="text/javascript">
                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-34731274-1']);
                    _gaq.push(['_trackPageview']);
                    _gaq.push(['_trackEvent', 'sharing', 'viewed full-screen', 'snippet W7gNz', 0, true]);
                    (function () {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                </script>
                <script type="text/javascript">
                    (function ($) {
                        $('#theme_chooser').change(function () {
                            whichCSS = $(this).val();
                            document.getElementById('snippet-preview').contentWindow.changeCSS(whichCSS);
                        });
                    })(jQuery);


                    // Activate Next Step

                    $(document).ready(function () {

                        var navListItems = $('ul.setup-panel li a'),
                                allWells = $('.setup-content');

                        allWells.hide();

                        navListItems.click(function (e)
                        {
                            e.preventDefault();
                            var $target = $($(this).attr('href')),
                                    $item = $(this).closest('li');

                            if (!$item.hasClass('disabled')) {
                                navListItems.closest('li').removeClass('active');
                                $item.addClass('active');
                                allWells.hide();
                                $target.show();
                            }
                        });

                        $('ul.setup-panel li.active a').trigger('click');

                        // DEMO ONLY //
                        $('#activate-step-2').on('click', function (e) {
                            $('ul.setup-panel li:eq(1)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-2"]').trigger('click');
                            $(this).remove();
                        })

                        $('#activate-step-3').on('click', function (e) {
                            $('ul.setup-panel li:eq(2)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-3"]').trigger('click');
                            $(this).remove();
                        })

                        $('#activate-step-4').on('click', function (e) {
                            $('ul.setup-panel li:eq(3)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-4"]').trigger('click');
                            $(this).remove();
                        })

                        $('#activate-step-3').on('click', function (e) {
                            $('ul.setup-panel li:eq(2)').removeClass('disabled');
                            $('ul.setup-panel li a[href="#step-3"]').trigger('click');
                            $(this).remove();
                        })
                    });




                </script>

            </section>


</div>
</section>
