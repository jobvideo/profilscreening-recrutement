 
<?php
//Compétence de base
$sql_comp_bs	 = "SELECT distinct(question.question_id),question_nom,valeur_competence FROM   reponse_candidat,question,candidat WHERE
	partie_synthese_id = 4 and metier=candidat.metier_id and candidat.candidat_id='$candidat_id'and 
	reponse_candidat.candidat_id='$candidat_id' and reponse_candidat.question_id=question.question_id and type= 'base'  
	 group by question.question_id order by question_position";
$res_comp_bs	 = $connexion->prepare($sql_comp_bs);
$res_comp_bs->execute();

$nb_comp_bs	 = $res_comp_bs->rowCount();
//compétence spécifiques
$sql_comp_sp	 = "SELECT distinct(question.question_id),question_nom,valeur_competence FROM   reponse_candidat,question,candidat WHERE 
	 partie_synthese_id = 4 and metier=candidat.metier_id and candidat.candidat_id='$candidat_id'and reponse_candidat.candidat_id='$candidat_id' 
	 and reponse_candidat.question_id=question.question_id and type= 'specifique'  
	 group by question.question_id order by question_position";
$res_comp_sp	 = $connexion->prepare($sql_comp_sp);
$res_comp_sp->execute();
$nb_comp_sp	 = $res_comp_sp->rowCount();
$comp_Tch	 = array();
$comp_ass	 = array();


$comp_num	 = array();
?>

<div id="tabs-2"  class="clearfix tab competences">
    <div id="left-col" class="left-col">
        <p class="tab-head tab-tech">Compétences de base</p>
        <ul class="tab-cell comptech">
	    <?php
	    $i		 = 0;
	    if ($nb_comp_bs > 0)
		while ($row_comp_Tch	 = @$res_comp_bs->fetch()) {
		    $i++;
		    $graphic_competence_descriptif	 = ($row_comp_Tch['question_nom']);
		    $comp				 = $i;
		    $comp_val			 = $row_comp_Tch['valeur_competence'];
		    array_push($comp_num, 'Comp' . $comp);
		    array_push($comp_Tch, $comp_val);
		    ?>					

		    <li><strong>comp <?php echo $comp; ?>:</strong><?php echo $graphic_competence_descriptif; ?></li><br>


		    <?php
		}
	    else {
		echo "Pas de compétences  ";
	    }
	    ?>


        </ul>
        <p class="tab-head tab-assoc">Compétence spécifiques</p>
	<?php ?>
        <ul class="tab-cell compasso">

	    <?php
	    if ($nb_comp_sp > 0) {
		while ($row_comp_ass = @$res_comp_sp->fetch()) {
		    $i++;
		    $graphic_competence_descriptif	 = ($row_comp_ass['question_nom']);
		    $comp				 = $i;
		    $comp_val			 = $row_comp_ass['valeur_competence'];
		    array_push($comp_num, 'Comp' . $comp);
		    array_push($comp_ass, $comp_val);
		    ?>					

		    <li><strong>comp <?php echo $comp; ?></strong><?php echo $graphic_competence_descriptif ?></li>


		    <?php
		}
	    }
	    else {
		echo "Pas de compétences  ";
	    }
	    ?>

        </ul>



    </div><!--#left-col-->
    <script type="text/javascript" src="../js/ajax.jquery.js"></script>
    <script src="../js/highcharts2.js" ></script>
    <script type="text/javascript">
        $(function () {
            var chart;
            $(document).ready(function () {
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'Compet',
                        type: 'column',
                    },
                    title: {
                        text: ' '
                    },
                    xAxis: {
                        categories: [
<?php
for ($i = 0; $i < count($comp_num); $i++)
    echo " '" . ($comp_num[$i]) . "', ";
?>
                        ],
                        labels: {
                            rotation: -45,
                            align: 'right',
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Niveau'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        formatter: function () {
                            return 'Niveau de la <b>' + this.x + '</b><br/>' +
                                    ':  ' + Highcharts.numberFormat(this.y, 1) +
                                    '  ';
                        }
                    },
                    series: [{
                            name: 'Compétence de base',
                            data: [<?php
for ($i = 0; $i < count($comp_Tch); $i++)
    echo " " . ($comp_Tch[$i]) . " , ";
?>],
                            dataLabels: {
                                enabled: false,
                                rotation: -90,
                                color: '#FFFFFF',
                                align: 'right',
                                x: 0,
                                y: 5,
                                formatter: function () {
                                    return this.y;
                                },
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        },
                        {
                            name: 'compétence spécifiques',
                            data: [<?php
for ($i = 0; $i < count($comp_Tch); $i++)
    echo "0, ";
?> <?php
for ($i = 0; $i < count($comp_ass); $i++)
    echo " " . ($comp_ass[$i]) . " , ";
?>],
                            dataLabels: {
                                enabled: false,
                                rotation: -90,
                                color: '#FF0000',
                                align: 'right',
                                x: 0,
                                y: 5,
                                formatter: function () {
                                    return this.y;
                                },
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        },
                    ]
                });
            });

        });
    </script>


    <div id="right-col" class="right-col">
        <p class="tab-head othertest">Compétences | graphique</p>
        <div id="Compet" style="min-width: 400px; height: 400px; margin: 0 auto"></div><br><br><br>

        <legend><strong>1</strong>Non acquis</legend>
	<legend><strong>2</strong>En cours d'acquisition</legend>
	<legend><strong>3</strong> Bonne connaissance</legend>
	<legend><strong>4</strong>Très bonne connaissance</legend>
    </div><!--#right-col-->	

</div>					