<!--
<link rel="stylesheet" type="text/css" media="all" href= "../css/style_syntehese.css">
<link rel="stylesheet" href="../popup/bootstrap.min.css">
<script src="../popup/jquery.min.js"></script>
<script src="../popup/bootstrap.min.js"></script>
-->
<?php
$candidat_id			 = $_SESSION['candidat_id'];
$sql				 = "SELECT *
                                FROM   candidat
                                 WHERE  candidat_id = " . $candidat_id;
$res				 = $connexion->query($sql); //---> Exécuter la requête
$row				 = $res->fetch();
if ($row == FALSE)
    die("Impossible de trouver le candidat");
$candidat_nom			 = decode_text($row['candidat_nom']);
$candidat_prenom		 = $row['candidat_prenom'];
$candidat_date_naissance	 = $row['candidat_date_naissance'];
$candidat_sex			 = $row['candidat_sex'];
$candidat_telephone		 = $row['candidat_telephone'];
$candidat_email			 = $row['candidat_email'];
$candidat_domicilite		 = $row['candidat_domicilite'];
$candidat_codepostale		 = $row['candidat_codepostale'];
$candidat_permis		 = ($row['candidat_permis'] == 'Y') ? 'OUI' : 'NON';;
$candidat_mobilite		 = ($row['candidat_mobilite']== 'Y') ? 'OUI' : 'NON';
$candidat_moyen_transport	 = $row['candidat_moyen_transport'];
$candidat_handicap		 = ($row['candidat_handicap']== 'Y') ? 'OUI' : 'NON';;
$candidat_ref			 = $row['candidat_ref'];
$candidat_date_dispo		 = $row['candidat_date_dispo'];
$candidat_profession		 = $row['candidat_profession'];
$candidat_emploi_rechercher	 = $row['candidat_emploi_rechercher'];
$candidat_diplome		 = $row['candidat_diplome'];
$candidat_login			 = $row['candidat_login'];
$date_mise_ajour		 = $row['date_mise_ajour'];
$amenagement			 = $row['amenagement'];
$lequel				 = $row['lequel'];
$S_definit_comme		 = $row['S_definit_comme'];
$candidat_certification		 = $row['candidat_certification'];
$admin_user_id			 = $row['admin_user_id'];
$accompagne			 = $row['accompagne'];

$dossier = '../common/Fichiers/dossier_' . $candidat_ref;
if ($row['candidat_photo'] != 'img_vide.gif') {
    $candidat_photo = "../common/Images/candidat/" . stripslashes($row['candidat_photo']);
}
else {
    $candidat_photo = "../common/Images/img_vide.gif";
}
$candidat_lettre_motivation	 = $dossier . "/" . stripslashes($row['candidat_lettre_motivation']);
$candidat_synthese		 = $dossier . "/" . stripslashes($row['candidat_synthese']);
$candidat_video			 = stripslashes($row['candidat_video']);
$candidat_cv			 = $dossier . "/" . stripslashes($row['candidat_cv']);
$candidat_attestation		 = $dossier . "/" . stripslashes($row['candidat_attestation']);
$candidat_diplome2		 = $dossier . "/" . stripslashes($row['candidat_diplome2']);
$candidat_certificat		 = $dossier . "/" . stripslashes($row['candidat_certificat']);
$exp				 = "";
?>
<section id="featured" class="featured featured-inscription clearfix">
    <article id="intro-profil" role="section" class="intro-profil d-flex justify-content-end">
        <div id="image-featured" class="image-featured">
            <h3>bonjour, <?php echo $candidat_prenom . ' ' . $candidat_nom; ?></h3>
            <h1>Bienvenue dans votre espace</h1>
            <p>Ici vous pouvez consulter et modifier votre profil, envoyer votre cv à des entreprises.</p>
        </div>
    </article>
</section>
<section id="profil" role="section" class="featured-profil-candidat clearfix">
    <div id="cv" class="cv clearfix">
	<?php
	//mail
	if (isset($_GET['share_form'])) {
	    $share		 = md5($candidat_id);
	    $lien		 = "http://cv-web.gribdev.eu/share/?share=" . $share;
	    $share_form	 = $_GET['share_form'];
	    // envoyer mail page
	    $to		 = $share_form;
	    $subject	 = "Profil  " . $candidat_prenom;
	    //$to = "somebody@example.com, somebodyelse@example.com";
	    //$subject = "HTML email";
	    $message	 = '
						<html>
						<head>
						<title>  ' . $candidat_prenom . ' </title>
						</head>
						<body>
						 <!-- Modal content-->
						  <div class="modal-content">
								<div class="modal-header">
								  <h4 class="modal-title">
									 <p><img src="http://cv-web.gribdev.eu/images/logo.gif" width="100"></p><br>
								  </h4>
								</div>
								<div class="modal-body">
								 <p> Bonjour  </p>
								  <p>
								   ' . $candidat_prenom . ' vous a invité à consulter son pour y accéder,
								   <a href="' . $lien . '"> Cliquez ici</a>
								  </p>
								  </p>
								  Cordialement
								  </p>
						  </div>
						</body>
						</html>
						';
	    // Always set content-type when sending HTML email
	    $headers	 = "MIME-Version: 1.0" . "\r\n";
	    $headers	 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	    // More headers
	    $headers	 .= 'From: <webmaster@jobvideo.fr>' . "\r\n";
	    //$headers .= 'Cc: myboss@example.com' . "\r\n";
	    if (@mail($to, $subject, $message, $headers)) {
		echo '<div class="container">
											  <div class="alert alert-success">
												<strong>message envoyé avec succès!</strong>
											  </div>
											</div>';
		?>
		<script>
	            setTimeout("document.location='./index.php?session=<?php echo $idses ?>';", 2000);          //---> rafraichir la page en cours
		</script>
		<?php
	    }
	    else {
		echo '<div class="container">
										  <div class="alert alert-success">
											<strong>Erreur!</strong>
										  </div>
										</div>';
		?>
		<script>
	            setTimeout("document.location='./index.php?session=<?php echo $idses ?>';", 2000);          //---> rafraichir la page en cours
		</script>
		<?php
	    }
	}
	?>
        <div id="cv-tabs" class="cv-tabs">
            <div class="cv-tabs-inner clearfix">
                <a href="./?p=profil&session=<?php echo $idses ?>" class="btn edit" title="Modifier votre profil">Je modifie mon profil</a>
                <a href="#share-form" id="btn-share" class="btn share" title="Partager l'url de votre profil par email" >Partager mon profil
                </a>
                <form id="share-form" class="mfp-hide form">
                    <ol>
                        <h3>Partagez votre cv à une entreprise</h3>
                        <li>
                            <label for="identifiant">Email du destinataire</label>
                            <input class="zone" name="email" placeholder="ex : mon email@mondomaine.com" required="" type="text">
                        </li>
                    </ol>
                    <button class="d-flex align-items-center">
                        <input name="submit" type="submit" id="send-message" class="submit" value="Envoyer" />
                    </button>
                    <button class="mfp-close" type="button" title="Close (Esc)" data-dismiss="share">×</button>
                </form><!--#share-form-->
                <ul id="tabs" class="tabs">
                    <li><a class="tab1 activate" id="firstonglet" href="#tabs-1" title="Présentation du candidat">Présentation</a></li>
                    <li><a class="tab2" href="#tabs-2" title="Voir les compétences du candidat">Compétences</a></li>
                    <li><a class="tab3" href="#tabs-3" title="Consulter la synthèse RH du candidat">Synthèse</a></li>
                    <li><a class="tab4" href="#tabs-4" title="Télécharger les documents professionnels du candidat">Documents</a></li>
                    <li><a class="tab5" id="ssborder" href="#tabs-5" title="Voir l'ensemble des données du candidats">Résumé</a></li>
<!--                     <li><a class="tab6" id="ssborder" href="#tabs-6" title="Générer password">Mot de passe</a></li> -->

                </ul>
            </div><!--.cv-tabs-inner -->
            <div id="content-tab" class="content-tab">
                <div id="tabs-1" class="clearfix tab pres">
                    <article class="profil-content-inner clearfix">
                        <figure id="portrait" class="portrait portrait-candidat">
                            <img src="<?php echo $candidat_photo ?>" alt="image du candidat" />
                        </figure>
                        <div class="cv-text">
                            <h1><strong><?php echo $candidat_nom . " " . $candidat_prenom; ?></strong></h1>
                            <h2><?php echo $candidat_profession ?></h2>
                            <h3><?php echo $candidat_domicilite ?></h3>
                            <ul id="ref" class="ref">
                                <li>Candidat disponible à partir du : <?php echo $candidat_date_dispo ?> </li>
                                <li>Identifiant candidat <?php echo $candidat_ref ?> </li>
                            </ul>
                            <p class="maj">Dernière mise à jour :  <?php echo $date_mise_ajour; ?></p>
                        </div>
                    </article>
                    <div id="left-col" class="left-col">
                        <p class="tab-head">vidéo</p>
                        <div class="embed-container">
                            <embed src="<?php echo $candidat_video; ?>" frameborder="0" allowfullscreen="">
                        </div>
                    </div><!--#left-col-->
                    <div id="right-col" class="right-col">
                        <div class="head">
                            <p class="tab-head">expériences</p>
			    <?php
			    $sql_exp = "SELECT * FROM   experience WHERE candidat_id='$candidat_id'";
			    $res_exp = $connexion->query($sql_exp);
			    $n	 = 0;
			    ?>
                            <ul class="tab-cell">
				<?php
				while ($row_exp = $res_exp->fetch()) {
				    $experience_titre	 = $row_exp['experience_titre'];
				    if ($n > 0)
					$exp			 = $exp . ' , ' . $experience_titre;
				    else
					$exp			 = $exp . '   ' . $experience_titre;
				    $n++;
				    ?>
    				<li class="exp"> <?php echo $experience_titre; ?></li>
				    <?php
				}
				?>
                            </ul>
                        </div><!--.head-->
                        <div class="list-caract">
                            <ul class="tab-cell caract">
                                <li><strong>diplôme</strong><?php echo $candidat_diplome ?></li>
                                <li><strong>mobilité</strong> <?php echo $candidat_mobilite ?></li>
                                <li><strong>permis de conduire</strong><?php echo $candidat_permis ?></li>
                                <li><strong>recherche</strong><?php echo $candidat_emploi_rechercher ?></li>
                                <li><strong>moyen de transport</strong><?php echo $candidat_moyen_transport ?></li>															</ul>
                        </div><!--.list-caract-->
                    </div><!--#right-col-->
                </div><!--#tabs-1-->
		<?php
		if ($accompagne == 'OUI') {
		    ?>
    		<div id="tabs-2"  class="clearfix tab competences">
			<?php include"competence.php"; ?>
    		</div>
		<!--#tabs-2--->
		    <?php
		}
		else {
		    include"accompagne.php";
		}

		if ($accompagne == 'OUI') {
		    ?>
    		<div id="tabs-3"  class="clearfix tab quest">
    		    <div id="left-col" class="left-col">
    			<p class="tab-head">synthèse rh</p>
    			<p class="tab-cell synt">Cliquez   <a href="./synthese_tt.php?session=<?php echo $idses ?>" target="_blank">ici</a> pour télécharger</p>
    			<p>Nos outils vous permettent de mieux connaître vos futurs collaborateurs, de cerner davantage leurs attentes, leur motivation et leurs compétences.</p>
    			<p class="tab-head tab-assoc">Personnalité au travail</p>
    			<p>Évaluez le profil professionnel de nos candidats, leur adéquation au(x) poste(s); Une synthèse de ces résultats vous est proposée. Profil Pro R est un test développé par Central Test.</p>
    			<p class="tab-head tab-supp">Motivations</p>
    			<p>Découvrez les principales sources de motivations professionnelles.</p>
    			<p class="tab-head">Aptitudes</p>
    			<p>Appréciez les qualités professionnelles et le savoir-faire de nos candidats. L'analyses des tests. L'analyse de nos tests est effectuée par des professionnels des ressources humaines diplômés en psychologie du travail. Ces tests ne font pas l´objet de préparations spécifiques. Ils témoignent de tendances observées au moment de l´entretien. Ces données sont dynamiques et peuvent évoluer selon des événements liés à  la vie professionnelle ou personnelle du candidat.</p>
    			<p class="tab-head othertest">Besoin d´autres tests</p>
    			<p>Vous avez retenu un candidat mais certains doutes persistent ? D´autres tests peuvent vous être proposés : Test de situation au travail, test d´aptitude, test de connaissances...</p>
    			<p class="submit-button"><a class="submit" href="/#featured-contact">Contactez-nous</a></p>
    		    </div><!--#left-col-->
    		    <div id="right-col" class="right-col">
    			<p class="tab-head tab-assoc">Personnalité au travail | graphique</p>
    			<div id="Personnalite" class="personnalite">
				<?php
				include"personnalite_graphe.php";
				?>
    			</div>
    			<p class="tab-head tab-supp">Motivations | graphique</p>
    			<div id="Motivations" class="motivations">
				<?php
				include"motivation_graphe.php";
				?>
    			</div>
    			<p class="tab-head tab-supp">Aptitudes | graphique</p>
    			<div id="Aptitudes" class="aptitudes">
				<?php
				include"aptitude_graphe.php";
				?>
    			</div>
    		    </div><!--#right-col-->
    		</div><!--#tabs-3-->

		    <?php
		}
		else {
		    include"accompagne_syn.php";
		}
		?>
                <div id="tabs-4"  class="clearfix tab docs">
                    <p class="tab-head">Certification</p>
                    <p>Les documents présentés sont régulièrement mis à jour par les candidats ou les opérateurs chargés de leur accompagnement. Ils sont certifiés comme exacts par leurs déposants dans un cadre contractuel avec notre équipe. Le dépôt de tout document incomplet, erroné, ou faux entraîne des sanctions graduées pouvant aller jusqu'à la suspension de la publication du profil du candidat ou sa radiation de notre base de données.
                        Merci de nous avertir si vous constatez une inexactitude ou un faux.</p>
                    <div id="left-col" class="left-col">
                        <p class="tab-head tabdocs">le CV</p>
                        <p class="tab-cell synt">Cliquez <a href="<?php echo $candidat_cv; ?>" target="_blank">ici</a> pour télécharger</p>
                        <p class="tab-head tabdocs">la lettre de motivation</p>
                        <p class="tab-cell synt">Cliquez <a href="<?php echo $candidat_lettre_motivation; ?>" target="_blank">ici</a> pour télécharger</p>
                        <p class="tab-head tabdocs">attestation(s)</p>
                        <p class="tab-cell synt">
                            Cliquez <a href="<?php echo $candidat_attestation; ?>" target="_blank">ici</a> pour télécharger</p>
                    </div><!--#left-col-->
                    <div id="right-col" class="right-col">
                        <p class="tab-head tabdocs">les diplômes</p>
                        <p class="tab-cell synt">
                            Cliquez <a href="<?php echo $candidat_attestation; ?>" target="_blank">ici</a> pour télécharger</p>
                        <p class="tab-cell synt">
                            Cliquez <a href="<?php echo $candidat_diplome2; ?>" target="_blank">ici</a> pour télécharger</p>
                        <p class="tab-head tabdocs">certificat(s)</p>
                        <p class="tab-cell synt">
                            Cliquez <a href="<?php echo $candidat_certificat; ?>" target="_blank">ici</a> pour télécharger</p>
                    </div><!--#right-col-->
                </div><!--#tabs-4-->
                <div id="tabs-5"  class="clearfix tab coord">
                    <div id="left-col" class="left-col">
                        <ul class="tab-cell caract">
                            <li><strong>nom</strong><?php echo $candidat_nom; ?></li>
                            <li><strong>prénom</strong><?php echo $candidat_prenom; ?></li>
                            <li><strong>téléphones</strong><?php echo $candidat_telephone; ?></li>
                            <li><strong>email</strong><?php echo $candidat_email; ?></li>
                            <li><strong>sexe</strong><?php echo $candidat_sex; ?></li>
                            <li><strong>Né(e) le</strong><?php echo $candidat_date_naissance; ?></li>
                            <li><strong>Département</strong><?php echo $candidat_codepostale; ?></li>
                            <li><strong>Adresse</strong><?php echo $candidat_domicilite; ?> </li>
                        </ul>
                    </div><!--#left-col-->
                    <div id="right-col" class="right-col">
                        <ul class="tab-cell caract"> 
                            <li><strong>mobilité</strong><?php echo $candidat_mobilite; ?>  </li>
                            <li><strong>permis de conduire</strong><?php echo $candidat_permis; ?></li>
                            <li><strong>moyen de transport</strong><?php echo $candidat_moyen_transport; ?></li>
                            <li><strong>handicap</strong><?php echo $candidat_handicap; ?></li>
                            <li><strong>aménagement de poste</strong><?php echo $amenagement; ?></li>
                            <li><strong>lequel ?</strong><?php echo $lequel; ?></li>
                            <li><strong>Se définit comme</strong><?php echo $S_definit_comme; ?> </li>
                            <li><strong>Expériences</strong><?php echo $exp; ?> </li>
                        </ul>
                    </div><!--#right-col-->
                    <p class="tab-head">synthèse rh </p>
                    <p class="tab-cell synt">Cliquez <a href="./synthese_tt.php?session=<?php echo $idses ?>" target="_blank">ici</a> pour télécharger</p>
                </div><!--#tabs-5-->
<!--
                <div id="tabs-6"  class="clearfix tab coord">

		    <?php include"password.php"; ?>

                </div>
-->

            </div><!--#content-tab -->
        </div><!--#cv-tabs-->
    </div><!--#cv-->
</section>
<!-- Modal -->
<form method="get" name="share"  enctype="multipart/form-data" class="modal fade" id="myModal" class="mfp-hide form">
    <div id="popup-content" class="popup-content">
        <ol>
            <h3>Partagez votre cv à une entreprise</h3>
            <li>
                <p>
                    <label for="identifiant">Email du destinataire</label>
                    <input class="zone" name="email" placeholder="ex : mon email@mondomaine.com" required="" type="text">
                </p>
            </li>
        </ol>
        <p class="submit-button">
            <input name="submit" type="submit" id="send-message" class="submit"  data-dismiss="modal" value="Envoyer" onclick="document.location = './?session=<?php echo $idses ?>&share_form=' + document.share.email.value;" />
        </p>
    </div>
    <button type="button" class="btn btn-close" data-dismiss="modal">Fermer</button>
</form>