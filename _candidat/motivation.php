<?php
$candidat_id = $_SESSION['candidat_id'];

/* * *******************************************************************************************************
  Gestion de la pagination
 * ******************************************************************************************************** */
//---> Créer un objet de pagination sans condition SQL sur la table
$sql_question = "select *  from question where partie_synthese_id = '1' order by question_id";

$sql_graph = "SELECT * FROM  graphe_motivation  where candidat_id = '$candidat_id'  ";
$res_graph = $connexion->query($sql_graph);
$row_graph = @$res_graph->fetch();

$sql_graph_name = "SELECT * FROM  titre_graphe     ";
$res_graph_name = $connexion->query($sql_graph_name);
$tab_name       = array();
$o              = 0;

while ($row_graph_name = $res_graph_name->fetch()) {

    $tab_name[$o] = ($row_graph_name['titre']);
    $o++;
}
$mots = implode(",", $tab_name);

$tab = array();
for ($o = 0; $o < 10; $o++) {
    $tab[$o] = $row_graph['x' . ($o + 1)];
}
?>



<fieldset>
    <legend> Questionnaire de motivation  : </legend><br>
    <p>Ce questionnaire a pour objectif d’analyser vos principales sources de motivation au travail. Il se compose de 30 affirmations pour lesquelles vous devez évaluer votre degré 
        d'accord par une note comprise entre 1 à 4, sachant que: 
        <br><br>
    <dl class="faq">
        <dt>  = je suis en total désaccord avec l'affirmation; </dt>
        <dt>  = je suis peu d'accord avec l'affirmation; </dt>
        <dt>  = je suis en accord avec l'affirmation; </dt>
        <dt>  = je suis totalement en accord avec l'affirmation. </dt>
    </dl><br>
    Ce questionnaire peut être rempli en une dizaine de minutes. Il est important de répondre à la totalité des questions. 
    Il est important que vous répondiez le plus sincèrement possible. Ce questionnaire a pour objectif de compléter votre profil, il n’y a pas de mauvaises ou de bonnes réponses. 
    <br>
    Le choix de le publier ou non sur votre profil est libre. Cette question sera discutée avec vous lors d’un entretien individuel et votre accord devra être donné pour qu’il soit publié : </p>
    <br>


    <p>
        <!-- Début de l'enête de pagination -->
<?php
$res  = $connexion->query($sql_question);   //---> Afficher les bouttons
?>
        <!-- Fin de l'enête de pagination -->
    </p>
    <div style="padding-bottom:20px;" >
        <dl class="faq">
<?php
$i    = 0;
$nbre = @$res->rowCount();
$n    = 0;
/* Les occasions de développer des relations	4
  Le sentiment d'estime 4
  L'indépendance de pensée et d'action 4
  L'établissement de buts et d'objectifs 2
  Etablissement de méthodes 2
  Sécurité de l’emploi 3
  La rétribution 3
  Le sentiment de prestige 2
  L'autorité attachée à la position 3
  Le développement et le progrès personnel 2
 */
while ($row  = @$res->fetch()) {
    $i++;
    $n++;
    $disabled         = ($mod != 'Y') ? "disabled" : "";
    //$color               = ($i%2!=0)? "#EFEFEF" : "#E9E9E9";
    $session          = $_GET["session"];
    $question_id      = $row['question_id'];
    $question_nom     = affichage($row['question_nom'], "---");
    $question_visible = ($row['question_visible'] == 'Y') ? "CHECKED" : "";
    //$Valeur_Quotidienne     = $row['Valeur_Quotidienne'];
    // $Valeur_Professionnelle     = $row['Valeur_Professionnelle'];
    //-->les catégories de chaque question
    $sql2             = " SELECT * FROM reponse_question WHERE  question_id = $question_id ";
    $res2             = $connexion->query($sql2);

    $sql_existe = "select * from reponse_candidat where question_id  = '$question_id' and candidat_id = '$candidat_id' ";
    $res_existe = $connexion->query($sql_existe);
    $reponse    = "";
    $nbre       = @$res_existe->rowCount();
    if ($nbre >= 0) {
        $row_existe = @$res_existe->fetch();
        $res_existe = $row_existe['valeur_motivation'];
    }


    $rep = "";
    switch ($res_existe) {
        case "1" : $rep = "je suis en total desaccord avec l'affirmation";
            break;
        case "2" : $rep = "je suis peu d'accord avec l'affirmation";
            break;
        case "3" : $rep = "je suis en accord avec l'affirmation";
            break;
        case "4" : $rep = "je suis totalement en accord avec l'affirmation";
            break;
    }
    ?>
                <label for="utilise"><dt> - <?php echo ($row['question_nom']); ?> :  <?php echo '<span style="color:#ff0000">' . $rep . '</span>'; ?>
                    </dt>
                </label>
                <?php
            }
            ?> 
        </dl>
        <div style="    "> &nbsp;

        </div>

    </div>

    <script src="../js/highcharts2.js"></script>
    <script src="../js/highcharts-more2.js"></script>



    <div id="Motiv" style="min-width: 550px; max-width: 400px;   margin: 0 auto; margin-top:20px; "></div>

    <script type="text/javascript">
        Highcharts.chart('Motiv', {

            chart: {
                polar: true,
                type: 'area'
            },

            title: {
                text: 'Schéma des résultats ',
                x: -80
            },

            pane: {
                size: '80%'
            },

            xAxis: {
                categories: [
<?php
for ($i = 0; $i < count($tab_name); $i++)
    echo ' "' . $tab_name[$i] . '" ,';
?>

                ],
                tickmarkPlacement: 'on',
                lineWidth: 0
            },

            yAxis: {
                gridLineInterpolation: 'polygon',
                lineWidth: 0,
                min: 0
            },

            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
            },

            legend: {
                enabled: false,
                align: 'right',
                verticalAlign: 'top',
                y: 7,
                layout: 'vertical'
            },

            series: [{
                    name: 'Motivation',
                    data: [<?php
for ($i = 0; $i < count($tab); $i++)
    echo ' ' . $tab[$i] . ' ,';
?>

                    ],
                    pointPlacement: 'on'
                }, ]

        });



    </script>


</fieldset>	 




