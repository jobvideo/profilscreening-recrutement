<section id="featured" class="featured featured-inscription clearfix">
    <article id="intro-profil" role="section" class="intro-profil clearfix">
        <div id="image-featured" class="image-featured">
            <h3>bonjour, <?php echo (isset($nom)) ? $nom : ''; ?></h3>
            <h1>Bienvenue dans votre espace</h1>
            <p>Vous pouvez modifier les informations de votre profil. Au besoin vous pouvez aussi contacter un conseiller <a href="">ici</a>.</p>
        </div>
    </article>
</section>
<section id="profil" role="section" class="featured-profil-entreprise clearfix">
    <div id="cv" class="cv clearfix">