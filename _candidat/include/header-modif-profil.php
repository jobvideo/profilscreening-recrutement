<section id="featured" class="featured container-intro-modif-profil clearfix">
    <article id="intro-profil" role="section" class="intro-profil clearfix">
        <div id="image-featured" class="image-featured">
            <h1>Mise à jour de mon profil</h1>
            <p>Ici je peux modifier mes informations personnelles, mes documents ainsi que mes expériences.</p>
            <a class="email-operateur" href=""><button>Contacter un conseiller par mail<i class="ti-email"></i></button></a>
        </div>
    </article>
</section>
<section id="profil" role="section" class="featured-profil-modif clearfix">
    <div id="cv" class="cv clearfix">