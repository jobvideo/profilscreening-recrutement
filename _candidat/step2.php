<script type="text/javascript" language="javascript">

    var candidat_photo_choisi = false;


    function supprimer_image()
    {
        document.delete_file.file.value = "image";
        document.delete_file.submit();
    } //Fin supprimer_image

    function supprimer_candidat_certificat()
    {
        document.delete_file.file.value = "candidat_certificat";
        document.delete_file.submit();
    } //Fin supprimer_candidat_certificat

    function supprimer_candidat_diplome2()
    {
        document.delete_file.file.value = "candidat_diplome2";
        document.delete_file.submit();
    } //Fin supprimer_candidat_diplome2


    function supprimer_candidat_attestation()
    {
        document.delete_file.file.value = "candidat_attestation";
        document.delete_file.submit();
    } //Fin supprimer_candidat_attestation


    function supprimer_candidat_cv()
    {
        document.delete_file.file.value = "candidat_cv";
        document.delete_file.submit();
    } //Fin supprimer_candidat_cv

    /* function supprimer_candidat_video()
     {
     document.delete_file.file.value = "candidat_video";
     document.delete_file.submit();
     } //Fin supprimer_candidat_video
     */
    function supprimer_candidat_lettre_motivation()
    {
        document.delete_file.file.value = "candidat_lettre_motivation";
        document.delete_file.submit();
    } //Fin supprimer_candidat_lettre_motivation

    function supprimer_candidat_synthese()
    {
        document.delete_file.file.value = "candidat_synthese";
        document.delete_file.submit();
    } //Fin supprimer_candidat_synthese



    function switch_overview(file_name, parm)
    {
        file_name = file_name.value;
        var img = document.getElementById("overview" + parm);
        if (img != null)
            img.setAttribute("src", file_name);
        //img.src = filename;
    } //Fin switch_overview

    function DoPreview(file_name)
    {
        var filename = document.form1.file_name.value;
        var Img = new Image();

        Img.src = filename;
        document.document.getElementById("overview").src = Img.src;

    }


</script>

<section id="step-2" class="container-fluid setup-content">
 	<div id="content-insription" class="formulaire row">
      	<div class="container-fluid">
      		<div class="row">
      			<div class="input-block col-file col-12">
	      			<label>Télécharger une nouvelle photo</label>
	      			<p>Attention ! La dimension de l'image doit être au maximum de 400 x 400px</p>
		  			<div class="row">
			  			<div class="col-10">
			  				<div class="d-flex flex-column justify-content-center align-items-center">
			                   <figure class="content-circle">
			                    <img id="overview1" name="overview1" src="<?php echo $candidat_photo ?>" border="0" alt="Ma photo">
			                    </figure>
			                    <div class="container-button d-flex align-items-center">
				                    <a class="delete-img" href="#" onClick="javascript: supprimer_image()">
					                  <button><i class="ti-trash"></i></button>
					                </a>
			                    </div>
							</div>
			  			</div>
		                <div class="col-2 input-picture imput-button">
							<input name="candidat_photo" id="candidat_photo" type="file" value="<?php echo $candidat_photo ?>" alt="<?php echo $candidat_photo ?>" onChange="javascript: verif_upload_filename(this); switch_overview(this, 1);  candidat_photo_choisi = true;">
							<i class="ti-plus"></i>
						</div>
		  			</div>
                </div>
				<div class="input-block col-file col-12"> <label for=" "> Télécharger une lettre motivation </label>
                  <input name="candidat_lettre_motivation" type="file" value="<?php echo $candidat_lettre_motivation ?>" class="zone" onchange="verif_upload_filename(this);" />
                  <div class="row">

                  	 <?php if ($candidat_lettre_motivation != NULL && $candidat_lettre_motivation != "" && $candidat_lettre_motivation != $dossier . "/") { ?>

                        <a href="<?php echo $candidat_lettre_motivation ?>"> <img src="../images/gif/ph.gif" width="24" height="24" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" /> </a>
                        <a href="#" onclick="javascript: supprimer_candidat_lettre_motivation()"> <img alt="supprimer_candidat_lettre_motivation" border="0" src="../images/gif/croix.gif" /> </a>
                        <?php
                    } //Fsi
                    ?>
                  </div><!-- .row -->

                </div>
                <div class="input-block col-file col-12">
	                        <label for=" "> T&eacute;l&eacute;charger une synthese RH :</label>
	                        <input name="candidat_synthese" type="file" value="<?php echo $candidat_synthese ?>" class="zone" style="width:275px;"
	                               onchange="verif_upload_filename(this);" />
	                               <?php
	                               if ($candidat_synthese != NULL && $candidat_synthese != "" && $candidat_synthese != $dossier . "/") {  //---> Faut il afficher l'c&ocirc;ne du liens
	                                   ?>
	                            <a href="<?php echo $candidat_synthese ?>"> <img src="../images/ph.gif" width="24" height="24" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" /> </a>
	                            <a href="#" onclick="javascript: supprimer_candidat_synthese()"> <img alt="supprimer_candidat_synthese" border="0" src="../images/croix.gif" /> </a>
	                            <?php
	                        } //Fsi
	                        ?>
	                    </div>

				<div class="input-block col-file col-12"> <label for=" "> T&eacute;l&eacute;charger un cv: </label>
		                            <input name="candidat_cv" type="file" value="<?php echo $candidat_cv ?>" class="zone" style="width:275px;"
		                                   onchange="verif_upload_filename(this);
		                                   " />
		                                   <?php
		                                   if ($candidat_cv != NULL && $candidat_cv != "" && $candidat_cv != $dossier . "/") {  //---> Faut il afficher l'c&ocirc;ne du liens
		                                       ?>
		                                <a href="<?php echo $candidat_cv ?>"> <img src="../images/ph.gif" width="24" height="24" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" /> </a>
		                                <a href="#" onclick="javascript: supprimer_candidat_cv()">
		                                    <img alt="supprimer_candidat_cv" border="0" src="../images/croix.gif" /> </a>
		                                <?php
		                            } //Fsi
		                            ?>
		                        </div>
				<div class="input-block col-file col-12"> <label for=" "> T&eacute;l&eacute;charger une attestation: </label>
		                            <input name="candidat_attestation" type="file" value="<?php echo $candidat_attestation ?>" class="zone" style="width:275px;"
		                                   onchange="verif_upload_filename(this);" />
		                                   <?php
		                                   if ($candidat_attestation != NULL && $candidat_attestation != "" && $candidat_attestation != $dossier . "/") {  //---> Faut il afficher l'c&ocirc;ne du liens
		                                       ?>
		                                <a href="<?php echo $candidat_attestation ?>"> <img src="../images/ph.gif" width="24" height="24" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" /> </a>
		                                <a href="#" onclick="javascript: supprimer_candidat_attestation()"> <img alt="supprimer_candidat_attestation" border="0" src="../images/croix.gif" /> </a>
		                                <?php
		                            } //Fsi
		                            ?>
		                        </div>
	       		<div class="input-block col-file col-12"> <label for=" "> T&eacute;l&eacute;charger un diplome: </label>
                            <input name="candidat_diplome2" type="file" value="<?php echo $candidat_diplome2 ?>" class="zone" style="width:275px;"
                                   onchange="verif_upload_filename(this);" />
                                   <?php
                                   if ($candidat_diplome2 != NULL && $candidat_diplome2 != "" && $candidat_diplome2 != $dossier . "/") {  //---> Faut il afficher l'c&ocirc;ne du liens
                                       ?>
                                <a href="<?php echo $candidat_diplome2 ?>"> <img src="../images/ph.gif" width="24" height="24" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" /> </a>
                                <a href="#" onclick="javascript: supprimer_candidat_diplome2()"> <img alt="supprimer candidat_diplome2" border="0" src="../images/croix.gif" /> </a>
                                <?php
                            } //Fsi
                            ?>
                        </div>
		   		<div class="input-block col-file col-12"> <label for=" "> T&eacute;l&eacute;charger un certificat: </label>
                            <input name="candidat_certificat" type="file" value="<?php echo $candidat_certificat ?>" class="zone" style="width:275px;"
                                   onchange="verif_upload_filename(this);" />
                                   <?php
                                   if ($candidat_certificat != NULL && $candidat_certificat != "" && $candidat_certificat != $dossier . "/") {  //---> Faut il afficher l'c&ocirc;ne du liens
                                       ?>
                                <a href="<?php echo $candidat_certificat ?>"> <img src="../images/ph.gif" width="24" height="24" border="0" align="absmiddle" alt="t&eacute;l&eacute;chargement" /> </a>
                                <a href="#" onclick="javascript: supprimer_candidat_certificat"> <img alt="supprimer_candidat_certificat" border="0" src="../images/croix.gif" /> </a>
                                <?php
                            } //Fsi
                            ?>

                        </div>
	       </div>
       </div>

        <p class="btnCenterAlign">
			<button id="activate-step-3" class="activate-step" role="button" >
				Aller à l'étape 3<i class="ti-arrow-right"></i>
				</button>
	    </p>
    </div>
</section>