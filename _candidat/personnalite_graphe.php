<?php
$candidat_id  = $_SESSION['candidat_id'];
$sql_question = "select *  from question where partie_synthese_id = '3' order by question_position";
?>



<!-- Début de l'enête de pagination -->
<?php
$res          = $connexion->query($sql_question);   //---> Afficher les bouttons
?>
<!-- Fin de l'enête de pagination -->
<div class="graph-personnality clearfix">
    <!--  partie non dynamique -->
    <div class="header-graph-personnality col100">
        <div class="col33 title-fact" aria-label="facteur principal">
            facteur principal
        </div>
        <div class="col33 personnality-results">
            <span class="col-result" aria-label="facteur principal5">5</span>
            <span class="col-result" aria-label="facteur principal 4">4</span>
            <span class="col-result" aria-label="facteur principal 3">3</span>
            <span class="col-result" aria-label="facteur principal 2">2</span>
            <span class="col-result" aria-label="facteur principal 1">1</span>
            <span class="col-result" aria-label="facteur principal 0">0</span>
            <span class="col-result" aria-label="facteur  1">1</span>
            <span class="col-result" aria-label="facteur opposé 2">2</span>
            <span class="col-result" aria-label="facteur opposé 3">3</span>
            <span class="col-result" aria-label="facteur opposé 4">4</span>
            <span class="col-result" aria-label="facteur opposé 5">5</span>
        </div>
        <div class="col33 title-fact" arial-label="facteur opposé">
            facteur opposé
        </div>
    </div>
    <!--  partie dynamique -->
    <?php
    $i            = 0;
    $nbre         = @$res->rowCount();
    $n            = 0;
    while ($row          = @$res->fetch()) {
        $i++;
        $n++;
        $color            = ($i % 2 != 0) ? "#EFEFEF" : "#E9E9E9";
        $session          = $_GET["session"];
        $question_id      = $row['question_id'];
        $question_nom     = affichage(($row['question_nom']), "---");
        $question_visible = ($row['question_visible'] == 'Y') ? "CHECKED" : "";

        //-->les catégories de chaque question
        $sql2 = " SELECT * FROM reponse_question WHERE  question_id = $question_id ";
        $res2 = $connexion->query($sql2);
        ?>


        <div class="content-graph-personnality content-graph-personnality-results">
            <div class="col100 sub-title-fact"><?php echo $question_nom; ?>  </div>
            <?php
            while ($row2 = @$res2->fetch()) {
                $sql3    = " SELECT * FROM reponse_candidat WHERE  question_id = $question_id and
							  reponse_id = '" . $row2['reponse_question_id'] . "' "
                        . "and candidat_id = '$candidat_id' ";
                 
                $res3    = $connexion->prepare($sql3);
                $res3->execute();
                $nb_pers = $res3->rowCount();
                if ($nb_pers > 0) {

                    $row3         = @$res3->fetch();
                    if ($row3['personnalite'])
                    //$personnalite= $row3['personnalite']*100/11;
                        $personnalite = $row3['personnalite'];
                    else
                        $personnalite = 0; //=1->=11
                    ?>
                    <div class="content-results-fact">
                        <div class="col33 text-result">
                            <?php echo affichage(($row2['reponse_question_nom']), "---"); ?>
                        </div>
                        <div class="col33 personnality-results">
                            <?php
                            $nb           = 5;
                            $facteur      = "principal";
                            for ($i = 1; $i <= 11; $i++) {

                                if ($personnalite == $i) {
                                    echo '<span class="col-result  col-result-ok" aria-label="facteur ' . $facteur . '  ' . $nb . '"></span>';
                                }
                                else {
                                    echo '<span class="col-result  " aria-label="facteur ' . $facteur . '  ' . $nb . '"></span>';
                                }
                                if ($nb == 0) {
                                    $facteur = "opposé";
                                    $nb++;
                                }
                                if ($facteur == "principal") {
                                    $nb--;
                                }
                                else {
                                    $nb++;
                                }
                            }
                            ?>


                        </div>
                        <div class="col33 text-result" arial-label="facteur principal">
                            <?php echo affichage(($row2['oppose']), "---"); ?>
                        </div>
                        <?php
                        $question_nom = "";
                        $oppose       = "";
                        ?>
                    </div>
                    <?php
                }else
                    echo "Synthèse non encore éfféctuée.";
            }
            ?>

        </div>
        <?php
    }
    ?>

</div>

