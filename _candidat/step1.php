<script type="text/javascript" language="javascript">

    var candidat_photo_choisi = false;
    function verif()
    {

        if (document.form1.candidat_sex.value === "") {
            alert("Veuillez introduire le sexe  ");
            document.form1.candidat_sex.focus();
            return;
        } //Fsi

        if (document.form1.candidat_nom.value === "") {
            alert("Veuillez introduire le nom  ");
            document.form1.candidat_nom.focus();
            return;
        } //Fsi
        if (document.form1.candidat_prenom.value === "") {
            alert("Veuillez introduire le prenom  ");
            document.form1.candidat_prenom.focus();
            return;
        } //Fsi
         if (document.form1.candidat_date_naissance.value === "") {
            alert("Veuillez introduire la  date de naissance  ");
            document.form1.candidat_date_naissance.focus();
            return;
        } //Fsi
        if (document.form1.candidat_email.value === "") {
            alert("Veuillez introduire l'email  ");
            document.form1.candidat_email.focus();
            return;
        } //Fsi
        if (document.form1.candidat_telephone.value === "") {
            alert("Veuillez introduire le tél  ");
            document.form1.candidat_telephone.focus();
            return;
        } //Fsi
        if (document.form1.candidat_domicilite.value === "") {
            alert("Veuillez introduire l'adresse  ");
            document.form1.candidat_domicilite.focus();
            return;
        } //Fsi
        if (document.form1.candidat_codepostale.value === "") {
            alert("Veuillez introduire le code postal  ");
            document.form1.candidat_codepostale.focus();
            return;
        } //Fsi
          if (document.form1.candidat_date_dispo.value === "") {
            alert("Veuillez introduire la date de disponibilité  ");
            document.form1.candidat_date_dispo.focus();
            return;
        } //Fsi
        if (document.form1.Acceptation.checked === false) {
            alert("Veuillez Accepter les conditions générales d'utilisation des données ");
            document.form1.Acceptation.focus();
            return;
        }

        document.form1.submit();
    }

    function mobilite() {

        mob = document.form1.candidat_mobilite.value;

        if (mob == "Y")
            document.getElementById('Plequel').style.display = '';
        else
            document.getElementById('Plequel').style.display = 'none';
    }
    function permis() {

        mob = document.form1.candidat_permis.value;

        if (mob == "Y")
            document.getElementById('divpermis').style.display = '';
        else
            document.getElementById('divpermis').style.display = 'none';
    }
</script>
<?php
$sql_metier = "SELECT * FROM   metier  ";

$res_metier = $connexion->query($sql_metier);
$nb_metier  = $res_metier->rowCount();
?>

<section id="step-1" class="container-fluid setup-content">
    <div id="content-insription" class="formulaire row">

	    <div class="container-fluid">
		    <div class="row">
			    <p class="input-block col-md-4"><label for="text">Genre<span class="obligatory">*</span></label>
			        <select type="zone" name="candidat_sex" class="f"style="width:325px;">
			            <option  value="Homme" <?php echo ($candidat_sex == 'Homme' ) ? 'selected' : '' ?>  >Homme</option>
			            <option  value="Femme" <?php echo ($candidat_sex == 'Femme' ) ? 'selected' : '' ?> >Femme</option>
			        </select>
			    </p>

			    <p class="input-block col-md-4">
				    <label for="text">Nom<span class="obligatory">*</span></label>
			        <input type="text" name="candidat_nom" id="candidat_nom" value="<?php echo $candidat_nom ?>" placeholder="Inserer votre nom ici">
			    </p>

			    <p class="input-block col-md-4">
				    <label for="text">Prénom<span class="obligatory">*</span></label>
			        <input type="text" name="candidat_prenom" id="candidat_prenom" placeholder="Inserer votre prénom ici" value="<?php echo $candidat_prenom ?>">
			    </p>
		    </div>
	    </div>

		<div class="container-fluid">
			<div class="row">
				<p class="input-block col-md-4">
					<label for="text">Date de naissance<span class="obligatory">*</span></label>
					<input type="date" name="candidat_date_naissance" id="candidat_date_naissance" value="<?php echo $candidat_date_naissance ?>" >
				</p>
				<p class="input-block col-md-4">
					<label for="email">Email<span class="obligatory">*</span></label>
					<input type="email" id="candidat_email" name="candidat_email" value="<?php echo $candidat_email ?>"  placeholder="ex : votreadresse@mondomaine.fr" required>
					</p>
					<p class="input-block col-md-4">
						<label for="tel">Tél.<span class="obligatory">*</span></label>
						<input type="number_.format" id="candidat_telephone" name="candidat_telephone"  value="<?php echo $candidat_telephone ?>"  placeholder="ex : 03364775236" required>
					</p>
			</div>
		</div>

	    <div class="container-fluid">
	    	<div class="row">
	    		 <p class="input-block col-md-4">
		    		 <label for="domicilite">Adresse<span class="obligatory">*</span></label>
			        <input name="candidat_domicilite" type="text" class='zone' value="<?php echo $candidat_domicilite ?>">
			    </p>
			    <p class="input-block col-md-4">
				    <label for="codepostale">Code postale<span class="obligatory">*</span></label>
			        <input name="candidat_codepostale" type="text" class='zone' value="<?php echo $candidat_codepostale ?>">
			    </p>
			    <p class="input-block col-md-4">
				    <label for="date_dispo">Disponible à partir du <span class="obligatory">*</span></label>
			        <input name="candidat_date_dispo" type="date" class='zone' value="<?php echo $candidat_date_dispo ?>">
			    </p>
	    	</div>
	    </div>
	    <div class="container-fluid">
	    	<div class="row">
	    		<p class="input-block col-md-6">
				    <label for="permis">Avez-vous le permis ?<span class="obligatory"> </span></label>

			        <select type="zone" name="candidat_permis" class="f" onchange="permis();">
			            <option  value="N" <?php echo ($candidat_permis == 'N' ) ? 'selected' : '' ?> >Non</option>
			            <option  value="Y" <?php echo ($candidat_permis == 'Y' ) ? 'selected' : '' ?>  >Oui</option>
			        </select>
	    		</p>
				
			    <?php if ($candidat_permis == 'Y') {
			        ?>
			        <p class="input-block col-md-6" id="divpermis">
				        <label for="permis">Permis<span class="obligatory"> </span></label>
			            <select type="zone" name="candidat_permisT[]" id="candidat_permisT" class="f" multiple="">
			                <option  value="A" <?php echo (stristr($candidat_permisT, 'A') == TRUE ) ? 'selected' : '' ?>  >A</option>
			                <option  value="B" <?php echo (stristr($candidat_permisT, 'B') == TRUE ) ? 'selected' : '' ?> >B</option>
			                <option  value="C" <?php echo (stristr($candidat_permisT, 'C') == TRUE ) ? 'selected' : '' ?>  >C</option>
			                <option  value="D" <?php echo (stristr($candidat_permisT, 'D') == TRUE ) ? 'selected' : '' ?> >D</option>
			                <option  value="E" <?php echo (stristr($candidat_permisT, 'E') == TRUE ) ? 'selected' : '' ?> >E</option>

			            </select>
			        </p>
			        <?php
			    }
			     
			    ?>
	    	</div>
	    </div>
		<div class="container-fluid">
			<div class="row">
				<p class="input-block col-md-6"><label for="candidat_handicap">Connaissance d'un handicap (RQTH)</label>
			        <select type="zone" name="candidat_handicap" id="candidat_handicap" class="f">
			            <option  value="N" <?php echo ($candidat_handicap == 'N' ) ? 'selected' : '' ?> >Non</option>
			            <option  value="Y" <?php echo ($candidat_handicap == 'Y' ) ? 'selected' : '' ?>  >Oui</option>
			        </select>
				</p>
			    <p class="input-block col-md-6"><label for="candidat_mobilite">Mobilité<span class="obligatory"> </span></label>
			        <select type="zone" name="candidat_mobilite" id="candidat_mobilite" class="f" onchange="mobilite();" >
			            <option  value="N" <?php echo ($candidat_mobilite == 'N' ) ? 'selected' : '' ?> >Non</option>
			            <option  value="Y" <?php echo ($candidat_mobilite == 'Y' ) ? 'selected' : '' ?>  >Oui</option>
			        </select>

				    <?php if (strtoupper($candidat_mobilite) == 'Y') { ?>
				        <span class="lequel" style="display: block;" id="Plequel">
				            <label for=" ">lequel? |<span class="obligatory"> </span></label>
				            <input name="lequel" type="text" class='zone'  >
				        </span>
				        <?php
				    }
				    else {
				        ?>
				        <span class="lequel" style="display:none;" id="Plequel"><label for=" ">lequel ?<span class="obligatory"> </span></label>
				            <input name="lequel"  id ="lequel" type="text" class='zone'  >
				        </span>
				        <?php
				    }
				    ?>
			    </p>
			</div>
		</div>

	    <div class="container-fluid">
	    	<div class="row">
			    <p class="input-block col-md-4"><label for="Se définit comme :">Je me définis comme<span class="obligatory"> </span></label>
			        <input name="S_definit_comme" type="text" class='zone' value="<?php echo $S_definit_comme ?>">
			    </p>
			    <p class="input-block col-md-4"><label for="profession">Profession |<span class="obligatory"> </span></label>
			        <input name="candidat_profession" type="text" class='zone' value="<?php echo $candidat_profession ?>">
			    </p>
			    <p class="input-block col-md-4"><label for="emploi_rechercher">Emploi recherché |<span class="obligatory"> </span></label>
			        <select type = "zone" name = "candidat_emploi_rechercher" id = "candidat_emploi_rechercher" class = "f">
			            <option  value="" >Sélectionnez un emploi</option>
			            <?php
			            while ($row_metier = $res_metier->fetch()) {
			                ?>

			                <option  value="<?php echo $row_metier['metier_nom']; ?>" <?php echo ($candidat_emploi_rechercher == $row_metier['metier_nom'] ) ? 'selected' : '' ?>  ><?php echo $row_metier['metier_nom']; ?></option>

			                <?php
			            }
			            ?>
			        </select>
			    </p>
	    	</div>
	    </div>

	    <div class="container-fluid">
	    	<div class="row">
	    		 <p class="input-block col-md-4"><label for="diplome">Diplôme principal<span class="obligatory"> </span></label>
	        <input name="candidat_diplome" type="text" class='zone' value="<?php echo $candidat_diplome ?>">
	    </p>
	    <p class="input-block col-md-4"><label for="certification">Certification<span class="obligatory"> </span></label>
	        <input name="candidat_certification" type="text" class='zone' value="<?php echo $candidat_certification ?>">
	    </p>
	    <p class="input-block col-md-4"><label for="accompagne">Je souhaite être accompagné ?<span class="obligatory"> </span></label>
	        <select type="zone" name="accompagne" class="f">
	            <option  value="OUI" <?php echo ($accompagne == 'OUI' ) ? 'selected' : '' ?>  >Oui</option>
	            <option  value="NON" <?php echo ($accompagne == 'NON' ) ? 'selected' : '' ?> >Non</option>
	        </select>
	    </p>
	    	</div>
	    </div>

	    <p class="btnCenterAlign">
			<button id="activate-step-2" class="activate-step" role="button" >Aller à l'étape 2<i class="ti-arrow-right"></i></button>
	    </p>
    </div>
</section>


